﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPDDL.AMI.Adaptor.Models
{
    public class CategoryCodeMaster
    {
        public int ID { get; set; }
        public string Category_Name { get; set; }
    }
}