﻿using Newtonsoft.Json;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class HesResullt
    {
        public string ReconnectResult { get; set; }
    }
    public class Xml
    {
        [JsonProperty("@version")]
        public string Version { get; set; }

        [JsonProperty("@encoding")]
        public string Encoding { get; set; }
    }

    public class DisconnectResult
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public int Code { get; set; }
    }
    public class DisconnectResponse
    {
        [JsonProperty("@xmlns")]
        public string Xmlns { get; set; }
        public DisconnectResult DisconnectResult { get; set; }
    }

    public class ConnectResult
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public int Code { get; set; }
    }
    public class ConnectResponse
    {
        [JsonProperty("@xmlns")]
        public string Xmlns { get; set; }
        public ConnectResult ConnectResult { get; set; }
    }
    
    public class DemandResetResult
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public int Code { get; set; }
    }
    public class DemandResetResponse
    {
        [JsonProperty("@xmlns")]
        public string Xmlns { get; set; }
        public DemandResetResult DemandResetResult { get; set; }
        public int Code { get; set; }
    }
   
    public class ODRResult
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public int Code { get; set; }
    }
    public class ODRResponse
    {
        [JsonProperty("@xmlns")]
        public string Xmlns { get; set; }
        public ODRResult ODRResult { get; set; }
    }

    public class RelayStatusResult
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public int Code { get; set; }
    }
    public class RelayStatusResponse
    {
        [JsonProperty("@xmlns")]
        public string Xmlns { get; set; }
        public RelayStatusResult RelayStatusResult { get; set; }
    } 

    public class PingResult
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public int Code { get; set; }
    }
    public class PingResponse
    {
        [JsonProperty("@xmlns")]
        public string Xmlns { get; set; }
        public PingResult PingResult { get; set; }
    }

    public class BalanceUpdateResult
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public int Code { get; set; }
    }
    public class BalanceUpdateResponse
    {
        [JsonProperty("@xmlns")]
        public string Xmlns { get; set; }
        public BalanceUpdateResult BalanceUpdateResult { get; set; }
    }


    public class SoapBody
    {
        public DisconnectResponse DisconnectResponse { get; set; }
        public ConnectResponse ConnectResponse { get; set; }
        public DemandResetResponse DemandResetResponse { get; set; }
        public ODRResponse ODRResponse { get; set; }
        public RelayStatusResponse RelayStatusResponse { get; set; }
        public PingResponse PingResponse { get; set; }
        public BalanceUpdateResponse BalanceUpdateResponse { get; set; }
    }

    public class SoapEnvelope
    {
        [JsonProperty("@xmlns:soap")]
        public string XmlnsSoap { get; set; }

        [JsonProperty("@xmlns:xsi")]
        public string XmlnsXsi { get; set; }

        [JsonProperty("@xmlns:xsd")]
        public string XmlnsXsd { get; set; }

        [JsonProperty("soap:Body")]
        public SoapBody SoapBody { get; set; }
    }

    public class Root
    {
        [JsonProperty("?xml")]
        public Xml Xml { get; set; }

        [JsonProperty("soap:Envelope")]
        public SoapEnvelope SoapEnvelope { get; set; }
    }
}