﻿namespace TPDDL.AMI.Adaptor.Models
{

    public class BalanceUpdateInput
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string DeviceID { get; set; }
        public DisplayAttributes DisplayAttributes { get; set; }
    }

    public class Attrib
    {
        public string AttribName { get; set; }
        public string AttribValue { get; set; }
    }

    public class DisplayAttributes
    {
        public Attrib[] Attributes { get; set; }
    }

}