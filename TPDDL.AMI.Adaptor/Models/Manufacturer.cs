﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class Manufacturer
    {
        public int ID { get; set; }
        public string ManufacturerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DataSource { get; set; }
        public string CompCD { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ChangedBy { get; set; }
        public DateTime ChangedOn { get; set; }
    }
}