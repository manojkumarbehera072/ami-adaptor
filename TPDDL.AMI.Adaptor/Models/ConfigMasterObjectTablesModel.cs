﻿namespace TPDDL.AMI.Adaptor.Models
{
    public class ConfigMasterObjectTablesModel
    {
        public string Master_Header_Table { get; set; }
        public string Master_Attrib_Table { get; set; }
    }
}