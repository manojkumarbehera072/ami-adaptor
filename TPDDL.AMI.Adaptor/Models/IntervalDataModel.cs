﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class IntervalDataModel
    {
        public int EquipmentId { get; set; }
        public string DeviceCode { get; set; }
        public int ParamId { get; set; }
        public DateTime IntervalEndTime { get; set; }
        public decimal ParamValue { get; set; }
        public string InsertedBy { get; set; }
        public DateTime InsertedDate { get; set; }
        public string Status { get; set; }
        public int Flag { get; set; }
        public int DataSource { get; set; }
        public string ChangedBy { get; set; }
        public DateTime ChangedDate { get; set; }
    }
}