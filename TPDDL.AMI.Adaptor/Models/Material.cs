﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class Material
    {
        public int ID { get; set; }
        public string UtilityId { get; set; }
        public string Description { get; set; }
        public string DeviceType { get; set; }
        public string MaterClass { get; set; }
        public string DataSource { get; set; }
        public int CompId { get; set; }
        public string InsertedBy { get; set; }
        public DateTime InsertedDate { get; set; }
        public string ChangedBy { get; set; }
        public DateTime ChangedDate { get; set; }
    }
}