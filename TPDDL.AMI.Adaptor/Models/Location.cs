﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class Location
    {
        public int EquipmentId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}