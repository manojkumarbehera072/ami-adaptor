﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class AMIDeviceControlModel
    {
        public int ID { get; set; }
        public string UUID { get; set; }
        public string CategoryCode { get; set; }
        public DateTime ProcessingDateTime { get; set; }
        public string UtilitiesDeviceID { get; set; }
    }
}