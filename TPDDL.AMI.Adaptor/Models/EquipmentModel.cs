﻿namespace TPDDL.AMI.Adaptor.Models
{
    public class EquipmentModel
    {
        public int ID { get; set; }
        public string MeterID { get; set; }
        public string UtilityID { get; set; }
        public string AltUtilityID { get; set; }
    }
}