﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPDDL.AMI.Adaptor.Models
{
    public class ConsumerDevLocLinkModel
    {
        public int ID { get; set; }
        public string ConsumerID { get; set; }
        public string DeviceLocationId { get; set; }
    }
}