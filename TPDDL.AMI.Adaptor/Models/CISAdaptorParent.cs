﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class CISAdaptorParent
    {
        public string UUID { get; set; }
        public DateTime LogDateTime { get; set; }
        public string Request_Type { get; set; }
        public int Child_Count { get; set; }
    }
}