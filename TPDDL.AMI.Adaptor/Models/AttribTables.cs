﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class AttribTables
    {
        public int DataSource { get; set; }
        public DateTime Valid_From { get; set; }
        public int Attrib_ID { get; set; }
    }
}