﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class ExceptionCISSync
    {
        public int ID { get; set; }
        public string UUID { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ServiceName { get; set; }
    }
}