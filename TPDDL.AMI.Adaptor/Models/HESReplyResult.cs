﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPDDL.AMI.Adaptor.Models
{
    public class HESReplyResult
    {
        public string RefID { get; set; }
        public string DeviceID { get; set; }
        public string Action { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseText { get; set; }
        public string ResponseStatus { get; set; }
        public DateTime AMIResponseDateTime { get; set; }
        public DateTime HESResponseDateTime { get; set; }
    }
}