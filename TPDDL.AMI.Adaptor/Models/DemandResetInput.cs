﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPDDL.AMI.Adaptor.Models
{
    public class DemandResetInput
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string DeviceID { get; set; }
        public string ReplyURL { get; set; }
        public string DemandResetFlag { get; set; }
    }
}