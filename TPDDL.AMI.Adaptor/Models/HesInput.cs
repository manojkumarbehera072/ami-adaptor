﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class HesInput
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string DeviceID { get; set; }
        public string ReplyURL { get; set; }
        public string Source { get; set; }
        public string SysdateTime { get; set; }
        public string MRStartDate { get; set; }
        public string MREndDate { get; set; }
        public string[] ParamName { get; set; }
        public string DemandResetFlag { get; set; }
        public DisplayAttributes DisplayAttribute { get; set; }
    }
}