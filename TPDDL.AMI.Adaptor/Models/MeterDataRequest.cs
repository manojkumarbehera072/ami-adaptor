﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class MeterDataRequest
    {
        public string Action { get; set; }
        public string DataType { get; set; }
        public string Source { get; set; }
        public string ObjectIdentifier { get; set; }
        public string ObjectIdentifierType { get; set; }
        public string ObjectId { get; set; }
        public DateTime StarDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string UoMCategory { get; set; }
        public string UoMSubCategory { get; set; }
        public string ParamType { get; set; }
        public string ParamName { get; set; }
        public string ProfileName { get; set; }
        public string EstValues { get; set; }
        public string ReplyURL { get; set; }

    }
}