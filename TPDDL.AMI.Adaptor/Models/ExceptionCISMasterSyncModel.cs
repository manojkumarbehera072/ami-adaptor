﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class ExceptionCISMasterSyncModel
    {
        public string PayloadRefID { get; set; }
        public string MasterObject { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }
        public string ErrorDescription { get; set; }
        public DateTime Created_on { get; set; }
        public string Created_by { get; set; }        
        public string Changed_by { get; set; }
        public DateTime Changed_on { get; set; }
    }
}