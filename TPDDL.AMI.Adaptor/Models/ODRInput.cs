﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPDDL.AMI.Adaptor.Models
{
    public class ODRInput
    {
        public string RefID { get; set; }
        public string Action { get; set; }
        public string DeviceID { get; set; }
        public string ReplyURL { get; set; }
        public string MRStartDate { get; set; }
        public string MREndDate { get; set; }
        public string[] ParamName { get; set; }
    }
}