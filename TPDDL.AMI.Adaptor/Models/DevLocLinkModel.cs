﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class DevLocLinkModel
    {
        public int ID { get; set; }
        public int EquipmentId { get; set; }
        public int DevicelocationId { get; set; }
        public DateTime ValidFromDate { get; set; }
        public DateTime ValidToDate { get; set; }
    }
}