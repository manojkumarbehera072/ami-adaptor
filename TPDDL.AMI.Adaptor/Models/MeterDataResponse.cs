﻿using System;
using System.Collections.Generic;

namespace TPDDL.AMI.Adaptor.Models
{
    public class MeterRead
    {
        public object ParamName { get; set; }
        public object ParamValue { get; set; }
        public object DateTime { get; set; }
        public object Status { get; set; }
        public object Flag { get; set; }
        public object DataSource { get; set; }
    }

    public class EventHeader
    {
        public object EventID { get; set; }
        public object EventDesc { get; set; }
        public object EventDateTime { get; set; }
        public object ReportingDateTime { get; set; }
        public object Source { get; set; }
    }

    public class EventAttrib
    {
        public object EventID { get; set; }
        public object EventDateTime { get; set; }
        public object EventParamName { get; set; }
        public object EventParamValue { get; set; }
    }

    public class EventData
    {
        public EventHeader EventHeader { get; set; }
        public List<EventAttrib> EventAttrib { get; set; }
    }

    public class Equipment
    {
        public object EquipmentID { get; set; }
        public List<MeterRead> MeterRead { get; set; }
        public List<EventData> EventData { get; set; }
    }
    public class MeterDataResponse
    {
        public object ObjectIdentifier { get; set; }
        public object ObjectId { get; set; }
        public List<Equipment> Equipment { get; set; }
        public object ResponseCode { get; set; }
        public object ResponseText { get; set; }
    }

}