﻿namespace TPDDL.AMI.Adaptor.Models
{
    public class EquipmentRegLink
    {
        public string LogRegNo { get; set; }
        public int EquipmentId { get; set; }
        public string UOBIS { get; set; }
    }
}