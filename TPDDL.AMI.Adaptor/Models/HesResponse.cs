﻿namespace TPDDL.AMI.Adaptor.Helper
{
    public class HesResponse
    {
        public int Code { get; set; }
        public string Status { get; set; }
    }
}