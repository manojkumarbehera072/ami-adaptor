﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class HesWorkflowModel
    {
        public string WorkflowType { get; set; }
        public string RefrenceObjectType { get; set; }
        public string ReferenceObjId { get; set; }
        public string WorkflowStatus { get; set; }
        public string WorkflowResponseText { get; set; }
        public string RequestSentTime { get; set; }
        public string ResponseReceivedTime { get; set; }
        public int HESId { get; set; }
        public string DataSource { get; set; }
        public int RetryCount { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ChangedOn { get; set; }
        public string ChangedBy { get; set; }
    }
}