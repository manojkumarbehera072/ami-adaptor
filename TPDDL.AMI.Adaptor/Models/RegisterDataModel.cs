﻿using System;

namespace TPDDL.AMI.Adaptor.Models
{
    public class RegisterDataModel
    {
        public int EquipmentId { get; set; }
        public string DeviceCode { get; set; }
        public int ParamId { get; set; }
        public DateTime EndTime { get; set; }
        public decimal ParamValue { get; set; }
        public DateTime DemandPeakTiime { get; set; }
        public string InsertedBy { get; set; }
        public DateTime InsertedDate { get; set; }
        public string Status { get; set; }
        public int Flag { get; set; }
        public int DataSource { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}