﻿using log4net;
using log4net.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using TPDDL.AMI.Adaptor.Helper;
using TPDDL.AMI.Adaptor.Models;
using Attribute = TPDDL.AMI.Adaptor.Models.Attrib;
using Equipment = TPDDL.AMI.Adaptor.Helper.Equipment;
using Equipments = TPDDL.AMI.Adaptor.Models.Equipment;

namespace TPDDL.AMI.Adaptor
{

    [WebService(Namespace = "http://tpddl.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class TPDDLAMIAdaptorService : System.Web.Services.WebService
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];
        HES hes = new HES();
        DateTimeValidator dateTimeValidator = new DateTimeValidator();
        Equipment equipment = new Equipment();
        HesWorkflow hesWorkflow = new HesWorkflow();
        HesWorkflowModel hesWorkflowModel = new HesWorkflowModel();
        string compId = ConfigurationManager.AppSettings["compId"];
        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void EquipmentCreate_Single(DeviceCreate.UtilsDvceERPSmrtMtrCrteReqMsg utilitiesDeviceERPSmartMeterCreateRequest)
        {
            string query = "";
            string uuid = "";
            string CreatedBy = "";
            bool isRejected = false;
            string errorMessage = "";
            string deviceType = "";
            string configValue = "";
            int hesCode = 0;
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            void Confirmation_In()
            {
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UtilitiesDeviceERPSmartMeterCreateConfirmation_InBinding cls = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UtilitiesDeviceERPSmartMeterCreateConfirmation_InBinding();
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfMsg req = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfMsg();
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.BusinessDocumentMessageHeader head = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.BusinessDocumentMessageHeader();
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UUID uid = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UUID REFuid = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfUtilsDvce devie = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfUtilsDvce();
                uid.Value = uuid;
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UtilitiesDeviceID deviceid = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.UtilitiesDeviceID();
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.Log log = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.Log();
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.LogItem[] logitem = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.LogItem[1];
                UtilitiesDeviceERPSmartMeterCreateConfirmation_In.LogItem logitemr = new UtilitiesDeviceERPSmartMeterCreateConfirmation_In.LogItem();
                System.Guid GUID;
                GUID = System.Guid.NewGuid();
                head.ReferenceUUID = uid;
                REFuid.Value = GUID.ToString();
                head.UUID = REFuid;
                head.CreationDateTime = DateTime.Now;
                head.SenderBusinessSystemID = "TPDDL";
                DataTable dturl = new DataTable();
                dturl.Clear();
                dturl = hes.GetSAPAMIUrl("EquipmentCreate_Single_Confirmation");

                cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
                cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();

                req.MessageHeader = head;
                deviceid.Value = utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.ID.Value;
                devie.ID = deviceid;
                req.UtilitiesDevice = devie;
                log.BusinessDocumentProcessingResultCode = "3";
                log.MaximumLogItemSeverityCode = "1";
                logitemr.TypeID = "61204";
                if (isRejected)
                {
                    configValue = hes.GetConifigValueByParam(Constants.EquipmentCreateSingle_MethodName, Constants.FailureSeverityCode);
                    logitemr.SeverityCode = configValue;
                    logitemr.Note = errorMessage;
                }
                else
                {
                    configValue = hes.GetConifigValueByParam(Constants.EquipmentCreateSingle_MethodName, Constants.SuccessSeverityCode);
                    logitemr.SeverityCode = configValue;
                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        logitemr.Note = "Request Processed Successfully";
                    }
                }
                logitem[0] = logitemr;
                log.Item = logitem;
                req.Log = log;

                cls.UtilitiesDeviceERPSmartMeterCreateConfirmation_In(req);
            }
            try
            {
                #region CIS Log entry
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader != null)
                {
                    if (utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.UUID.Value;
                        uuid = utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    if (utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.SenderBusinessSystemID != null)
                    {
                        CreatedBy = utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.SenderBusinessSystemID;
                    }
                    if (utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.CreationDateTime != null &&
                        utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.CreationDateTime != DateTime.MinValue)
                    {
                        DateTime creationDateTime = dateTimeValidator.CheckDate(utilitiesDeviceERPSmartMeterCreateRequest.MessageHeader.CreationDateTime);
                        cISAdaptorParent.LogDateTime = creationDateTime;
                    }
                }
                else
                {
                    uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Request_Type = "EquipmentCreate_Single";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);
                #endregion
                string fields = "";
                string values = "";
                string equipmentUtilityId = "";
                string updateEquipmentFields = "";
                int dataOriginID = 0;
                DataSourceMaster dataSourceMaster = new DataSourceMaster();

                ExceptionCISSync exceptionSync = new ExceptionCISSync();
                Material materialMaster;
                Manufacturer manufacturerMaster;

                //Validations
                if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice != null)
                {
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.ID != null
                        && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.ID.Value != null)
                    {
                        fields += "Utility_ID,";
                        values += "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.ID.Value + "',";
                        equipmentUtilityId = utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.ID.Value;
                    }
                    else
                    {
                        isRejected = true;
                        errorMessage += "Utility_ID Missing;";
                    }

                    if (!String.IsNullOrEmpty(utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SerialID))
                    {
                        fields += "Meter_ID,";
                        values += "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SerialID + "',";
                        updateEquipmentFields += "Meter_ID =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SerialID + "',";
                    }
                    else
                    {
                        isRejected = true;
                        errorMessage += "Serial_ID Missing;";
                    }

                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.StartDate != DateTime.MinValue)
                    {
                        fields += "StartDate,";
                        values += "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.StartDate.ToString("yyyy-MM-dd") + "',";
                        updateEquipmentFields += "StartDate =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.StartDate.ToString("yyyy-MM-dd") + "',";
                    }
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.EndDate != DateTime.MinValue)
                    {
                        fields += "EndDate,";
                        values += "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.EndDate.ToString("yyyy-MM-dd") + "',";
                        updateEquipmentFields += "EndDate =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.EndDate.ToString("yyyy-MM-dd") + "',";
                    }

                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.MaterialID != null
                        && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.MaterialID.Value != null)
                    {
                        string materialId = utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.MaterialID.Value;

                        fields += "Material_ID,";
                        values += "'" + materialId + "',";
                        updateEquipmentFields += "Material_ID =" + materialId + ",";

                        Material_Master material_Master = new Material_Master();
                        materialMaster = material_Master.GetMaterialByMaterialUtilityId(materialId);

                        if (materialMaster.ID > 0)
                        {
                            deviceType = materialMaster.DeviceType;
                        }
                        else
                        {
                            isRejected = true;
                            errorMessage += "Material_ID Not Found in Material_Master;";
                        }
                    }
                    else
                    {
                        isRejected = true;
                        errorMessage += "Material_ID Missing;";
                    }

                    // IndividualMaterialManufacturerInformation node
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation != null)
                    {
                        if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID != null
                            && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value != null)
                        {
                            string manufacturerId = utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value;

                            Manufacturer_Master manufacturer_Master = new Manufacturer_Master();
                            manufacturerMaster = manufacturer_Master.GetManufacturerById(manufacturerId);

                            if (manufacturerMaster.ID > 0)
                            {
                                fields += "Manufacturer_ID,";
                                values += "'" + manufacturerId + "',";
                                updateEquipmentFields += "Manufacturer_ID =" + "'" + manufacturerId + "',";
                            }
                            else
                            {
                                isRejected = true;
                                errorMessage += "Manufacturer_ID Not Found in Manufacturer_Master;";
                            }
                        }
                        if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID != null
                        && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value != null)
                        {
                            fields += "Model_ID,";
                            values += "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value + "',";
                            updateEquipmentFields += "Model_ID =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value + "',";
                        }
                        if (!String.IsNullOrEmpty(utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID))
                        {
                            fields += "Name_Plate_ID,";
                            values += "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID + "',";
                            updateEquipmentFields += "Name_Plate_ID =" + "'" + utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID + "',";
                        }
                    }

                    //SmartMeter node
                    if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SmartMeter != null)
                    {
                        if (utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID != null
                            && utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value != null)
                        {
                            //Get HES Code
                            hesCode = hes.GetHESIdByHESCode(utilitiesDeviceERPSmartMeterCreateRequest.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value);

                            if (hesCode > 0)
                            {
                                fields += "HES_ID,";
                                values += "'" + hesCode + "',";
                                updateEquipmentFields += "HES_ID =" + "'" + hesCode + "',";
                            }
                            else
                            {
                                isRejected = true;
                                errorMessage += "HES_CD Not Found in HES_MASTER";
                            }
                        }
                    }

                    if (CreatedBy != null)
                    {
                        fields += "Created_by,";
                        values += "'" + CreatedBy + "',";
                    }
                    fields += "Created_on,";
                    values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                    fields += "Status,";
                    values += "'InStock',";

                    if (CreatedBy != null)
                    {
                        updateEquipmentFields += "Changed_by = '" + CreatedBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(CreatedBy);
                        if (dataOriginID > 0)
                        {
                            fields += "Data_Origin_Code,";
                            values += "'" + dataOriginID + "',";
                            updateEquipmentFields += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                        }
                    }

                    if (!String.IsNullOrEmpty(deviceType))
                    {
                        fields += "Device_Type,";
                        values += "'" + deviceType + "',";
                        updateEquipmentFields += "Device_Type =" + "'" + deviceType + "',";
                    }

                    fields += "COMP_ID,";
                    values += "'" + compId + "',";
                    updateEquipmentFields += "COMP_ID =" + "'" + compId + "',";

                    //Remove end Comma 
                    fields = fields.TrimEnd(',');
                    values = values.TrimEnd(',');
                    updateEquipmentFields = updateEquipmentFields.TrimEnd(',');

                    if (!isRejected)
                    {
                        int equipId = equipment.GetEquipmentIdByUtilityId(equipmentUtilityId);

                        //UtilityId exists in equipment table
                        if (equipId > 0)
                        {
                            //update equipment
                            using (SqlConnection connection = new SqlConnection(constr))
                            {
                                connection.Open();
                                query = "update equipment set " + updateEquipmentFields + " where Utility_ID='" + equipmentUtilityId + "'";
                                SqlCommand cmd = new SqlCommand(query, connection);
                                cmd.ExecuteNonQuery();
                                connection.Close();
                            }
                        }

                        else
                        {
                            //Equipment Entry
                            using (SqlConnection connection = new SqlConnection(constr))
                            {
                                connection.Open();
                                query = "insert into equipment (" + fields + ") values(" + values + ")";
                                SqlCommand cmd = new SqlCommand(query, connection);
                                cmd.ExecuteNonQuery();
                                connection.Close();
                            }
                        }
                        //Write to File
                        var xmlString = Serialize<DeviceCreate.UtilsDvceERPSmrtMtrCrteReqMsg>(utilitiesDeviceERPSmartMeterCreateRequest);
                        WriteToLog(xmlString, uuid, Constants.CisAdaptorLog_Path);//Constants.CisAdaptorLog_Path
                    }
                    else
                    {
                        var xmlStrings = Serialize<DeviceCreate.UtilsDvceERPSmrtMtrCrteReqMsg>(utilitiesDeviceERPSmartMeterCreateRequest);
                        WriteToLog(xmlStrings, uuid, Constants.Rejected_Path);
                    }
                    Confirmation_In();
                }

                //Exception_CIS_Sync
                exceptionSync.UUID = uuid;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = CreatedBy;
                exceptionSync.ServiceName = Constants.EquipmentCreateSingle_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);

                //HES Work Flow in Equipment Single Create
                if (!isRejected)
                {
                    int refObjId = equipment.GetEquipmentIdByUtilityId(equipmentUtilityId);

                    hesWorkflowModel.WorkflowType = "EquipmentInventory";
                    hesWorkflowModel.RefrenceObjectType = "Equipment";
                    hesWorkflowModel.ReferenceObjId = Convert.ToString(refObjId);
                    hesWorkflowModel.WorkflowStatus = "1";
                    hesWorkflowModel.HESId = hesCode;
                    hesWorkflowModel.DataSource = "EquipmentCreate_Single";
                    hesWorkflowModel.CreatedBy = CreatedBy;
                    hesWorkflowModel.CreatedOn = DateTime.Now;

                    hesWorkflow.HesWorkFlowEntry(hesWorkflowModel);
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :EquipmentCreate_Single --- UUID :" + uuid + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                errorMessage += "Exception :" + ex.Message;
                Confirmation_In();
                var xmlString = Serialize<DeviceCreate.UtilsDvceERPSmrtMtrCrteReqMsg>(utilitiesDeviceERPSmartMeterCreateRequest);
                WriteToLog(xmlString, "EquipmentCreate_Single" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), "~/CISAdapterLog/Exception/");
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void EquipmentCreate_Bulk(DeviceCreateBulk.UtilsDvceERPSmrtMtrBlkCrteReqMsg UtilitiesDeviceERPSmartMeterBulkCreateRequest)
        {
            string query = "";
            string uuid = "";
            string parentUUID = "";
            string CreatedBy = "";
            bool isRejected = false;
            string deviceType = "";
            string configValue = "";
            int dataOriginID = 0;
            string errorMessage = "";
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            Equipment equipment = new Equipment();
            void Confirmation_In()
            {
                #region Confirmation_In
                UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_InBinding cls = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_InBinding();
                UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilsDvceERPSmrtMtrBlkCrteConfMsg req = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilsDvceERPSmrtMtrBlkCrteConfMsg();
                UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.BusinessDocumentMessageHeader head = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.BusinessDocumentMessageHeader();
                UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UUID uid = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UUID REFuid = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UUID();
                uid.Value = parentUUID;
                UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.Log log = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.Log();
                UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.LogItem[] logitem = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.LogItem[1];
                UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.LogItem logitemr = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.LogItem();
                UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfMsg[] MeterList = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfMsg[UtilitiesDeviceERPSmartMeterBulkCreateRequest.UtilitiesDeviceERPSmartMeterCreateRequestMessage.Length];

                //  Meter.MessageHeader = Childhead;

                System.Guid GUID;
                GUID = System.Guid.NewGuid();
                head.ReferenceUUID = uid;
                REFuid.Value = GUID.ToString();
                head.UUID = REFuid;
                head.CreationDateTime = DateTime.Now;
                head.SenderBusinessSystemID = "TPDDL";
                DataTable dturl = new DataTable();
                dturl.Clear();
                dturl = hes.GetSAPAMIUrl("EquipmentCreate_Bulk_Confirmation");

                cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
                cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();//ConfigurationManager.AppSettings.Get("DeviceConfirm");
                                                                 // req.m()
                req.MessageHeader = head;
                int i = 0;

                foreach (var utilsDvceERPSmrtMtrCrteReqMsgs in UtilitiesDeviceERPSmartMeterBulkCreateRequest.UtilitiesDeviceERPSmartMeterCreateRequestMessage)
                {
                    UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfMsg Meter = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfMsg();
                    UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.BusinessDocumentMessageHeader Childhead = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.BusinessDocumentMessageHeader();
                    UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfUtilsDvce devie = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilsDvceERPSmrtMtrCrteConfUtilsDvce();
                    UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilitiesDeviceID deviceid = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UtilitiesDeviceID();
                    UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UUID childUuid = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UUID();
                    UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UUID childREFuid = new UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In.UUID();

                    Guid childGUID = System.Guid.NewGuid();
                    childUuid.Value = utilsDvceERPSmrtMtrCrteReqMsgs.MessageHeader.UUID.Value;
                    deviceid.Value = utilsDvceERPSmrtMtrCrteReqMsgs.UtilitiesDevice.ID.Value;
                    devie.ID = deviceid;
                    Childhead.ReferenceUUID = childUuid;
                    childREFuid.Value = childGUID.ToString();
                    Childhead.UUID = childREFuid;
                    Childhead.CreationDateTime = DateTime.Now;
                    Childhead.SenderBusinessSystemID = "TPDDL";
                    //Childhead. = devie;
                    Meter.MessageHeader = Childhead;
                    Meter.UtilitiesDevice = devie;
                    log.BusinessDocumentProcessingResultCode = "3";
                    log.MaximumLogItemSeverityCode = "1";
                    logitemr.TypeID = "61204";
                    if (isRejected)
                    {
                        configValue = hes.GetConifigValueByParam(Constants.EquipmentCreate_Bulk_MethodName, Constants.FailureSeverityCode);
                        logitemr.SeverityCode = configValue;
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        configValue = hes.GetConifigValueByParam(Constants.EquipmentCreate_Bulk_MethodName, Constants.SuccessSeverityCode);
                        logitemr.SeverityCode = configValue;
                        if (!String.IsNullOrEmpty(errorMessage))
                        {
                            logitemr.Note = errorMessage;
                        }
                        else
                        {
                            logitemr.Note = "Request Processed Successfully";
                        }
                    }
                    logitem[0] = logitemr;
                    log.Item = logitem;
                    Meter.Log = log;
                    MeterList[i] = Meter;

                    i++;
                }

                req.UtilitiesDeviceERPSmartMeterCreateConfirmationMessage = MeterList;
                log.BusinessDocumentProcessingResultCode = "3";
                log.MaximumLogItemSeverityCode = "1";
                logitemr.TypeID = "61204";
                if (isRejected)
                {
                    configValue = hes.GetConifigValueByParam(Constants.EquipmentCreate_Bulk_MethodName, Constants.FailureSeverityCode);
                    logitemr.SeverityCode = configValue;
                    logitemr.Note = errorMessage;
                }
                else
                {
                    configValue = hes.GetConifigValueByParam(Constants.EquipmentCreate_Bulk_MethodName, Constants.SuccessSeverityCode);
                    logitemr.SeverityCode = configValue;
                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        logitemr.Note = "Request Processed Successfully";
                    }
                }
                logitem[0] = logitemr;
                log.Item = logitem;
                req.Log = log;

                cls.UtilitiesDeviceERPSmartMeterBulkCreateConfirmation_In(req);
                #endregion

            }
            try
            {
                DataSourceMaster dataSourceMaster = new DataSourceMaster();
                #region CIS Log entry
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (UtilitiesDeviceERPSmartMeterBulkCreateRequest.MessageHeader != null)
                {
                    if (UtilitiesDeviceERPSmartMeterBulkCreateRequest.MessageHeader.SenderBusinessSystemID != null)
                    {
                        CreatedBy = UtilitiesDeviceERPSmartMeterBulkCreateRequest.MessageHeader.SenderBusinessSystemID;
                    }
                    if (UtilitiesDeviceERPSmartMeterBulkCreateRequest.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = UtilitiesDeviceERPSmartMeterBulkCreateRequest.MessageHeader.UUID.Value;
                        parentUUID = UtilitiesDeviceERPSmartMeterBulkCreateRequest.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    if (UtilitiesDeviceERPSmartMeterBulkCreateRequest.MessageHeader.CreationDateTime != null &&
                        UtilitiesDeviceERPSmartMeterBulkCreateRequest.MessageHeader.CreationDateTime != DateTime.MinValue)
                    {
                        DateTime creationDateTime = dateTimeValidator.CheckDate(UtilitiesDeviceERPSmartMeterBulkCreateRequest.MessageHeader.CreationDateTime);
                        cISAdaptorParent.LogDateTime = creationDateTime;
                    }
                }
                else
                {
                    parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Child_Count = UtilitiesDeviceERPSmartMeterBulkCreateRequest.UtilitiesDeviceERPSmartMeterCreateRequestMessage.Length;
                cISAdaptorParent.Request_Type = "EquipmentCreate_Bulk";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);
                #endregion

                foreach (var utilsDvceERPSmrtMtrCrteReqMsg in UtilitiesDeviceERPSmartMeterBulkCreateRequest.UtilitiesDeviceERPSmartMeterCreateRequestMessage)
                {
                    #region CIS Child Log entry
                    CISAdaptorChild cISAdaptorChild = new CISAdaptorChild();

                    if (utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader != null)
                    {
                        if (utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.UUID != null)
                        {
                            cISAdaptorChild.UUID = utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.UUID.Value;
                            uuid = utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.UUID.Value;
                        }
                        else
                        {
                            uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                        }
                        if (utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.CreationDateTime != null &&
                        utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.CreationDateTime != DateTime.MinValue)
                        {
                            DateTime creationDateTime = dateTimeValidator.CheckDate(utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.CreationDateTime);
                            cISAdaptorChild.LogDateTime = Convert.ToDateTime(creationDateTime);
                        }
                    }
                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    cISAdaptorChild.Parent_UUID = parentUUID;
                    cISAdaptorChild.Request_Type = "EquipmentCreate_Bulk";
                    cisAdaptorLog.CISAdaptorChildLog(cISAdaptorChild);
                    #endregion
                    string fields = "";
                    string values = "";
                    string equipmentUtilityId = "";
                    string updateEquipmentFields = "";
                    int equipId = 0;
                    int hesCode = 0;
                    ExceptionCISSync exceptionSync = new ExceptionCISSync();
                    Material materialMaster;
                    Manufacturer manufacturerMaster;

                    //Validations
                    if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice != null)
                    {
                        if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.ID != null
                            && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.ID.Value != null)
                        {
                            fields += "Utility_ID,";
                            values += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.ID.Value + "',";
                            equipmentUtilityId = utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.ID.Value;
                        }
                        else
                        {
                            isRejected = true;
                            errorMessage += "Utility_ID Missing;";
                        }
                        if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SerialID))
                        {
                            fields += "Meter_ID,";
                            values += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SerialID + "',";
                            updateEquipmentFields += "Meter_ID =" + "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SerialID + "',";
                        }
                        else
                        {
                            isRejected = true;
                            errorMessage += "SerialID Missing;";
                        }
                        if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.StartDate != DateTime.MinValue)
                        {
                            fields += "StartDate,";
                            values += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.StartDate.ToString("yyyy-MM-dd") + "',";
                            updateEquipmentFields += "StartDate =" + "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.StartDate.ToString("yyyy-MM-dd") + "',";
                        }
                        if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.EndDate != DateTime.MinValue)
                        {
                            fields += "EndDate,";
                            values += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.EndDate.ToString("yyyy-MM-dd") + "',";
                            updateEquipmentFields += "EndDate =" + "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.EndDate.ToString("yyyy-MM-dd") + "',";
                        }
                        if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.MaterialID != null
                            && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.MaterialID.Value != null)
                        {
                            string materialId = utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.MaterialID.Value;
                            fields += "Material_ID,";
                            values += "'" + materialId + "',";
                            updateEquipmentFields += "Material_ID ='" + materialId + "',";

                            Material_Master material_Master = new Material_Master();
                            materialMaster = material_Master.GetMaterialByMaterialUtilityId(materialId);

                            if (materialMaster.ID > 0)
                            {
                                deviceType = materialMaster.DeviceType;
                            }
                            else
                            {
                                isRejected = true;
                                errorMessage += "Material_ID Not Found in Material_Master;";
                            }
                        }
                        else
                        {
                            isRejected = true;
                            errorMessage += "Material_ID Missing;";
                        }

                        // IndividualMaterialManufacturerInformation node
                        if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation != null)
                        {
                            if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID != null
                                && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value != null)
                            {
                                string manufacturerId = utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value;
                                Manufacturer_Master manufacturer_Master = new Manufacturer_Master();
                                manufacturerMaster = manufacturer_Master.GetManufacturerById(manufacturerId);

                                if (manufacturerMaster.ID > 0)
                                {
                                    fields += "Manufacturer_ID,";
                                    values += "'" + manufacturerId + "',";
                                    updateEquipmentFields += "Manufacturer_ID =" + "'" + manufacturerId + "',";
                                }
                                else
                                {
                                    isRejected = true;
                                    errorMessage += "Manufacturer_ID Not Found in Manufacturer_Master;";
                                }
                            }
                            if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID != null
                            && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value != null)
                            {
                                fields += "Model_ID,";
                                values += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value + "',";
                                updateEquipmentFields += "Model_ID =" + "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value + "',";
                            }
                            if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID))
                            {
                                fields += "Name_Plate_ID,";
                                values += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID + "',";
                                updateEquipmentFields += "Name_Plate_ID =" + "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID + "',";
                            }
                        }

                        //SmartMeter node
                        if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SmartMeter != null)
                        {
                            if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID != null
                                && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value != null)
                            {
                                //Get HES Code
                                HES hes = new HES();
                                hesCode = hes.GetHESIdByHESCode(utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value);
                                if (hesCode > 0)
                                {
                                    fields += "HES_ID,";
                                    values += "'" + hesCode + "',";
                                    updateEquipmentFields += "HES_ID =" + "'" + hesCode + "',";
                                }
                                else
                                {
                                    isRejected = true;
                                    errorMessage += "HES_CD Not Found in HES_MASTER";
                                }
                            }
                        }
                        if (CreatedBy != null)
                        {
                            fields += "Created_by,";
                            values += "'" + CreatedBy + "',";
                        }
                        fields += "Created_on,";
                        values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        if (CreatedBy != null)
                        {
                            updateEquipmentFields += "Changed_by = '" + CreatedBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                            dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(CreatedBy);
                            if (dataOriginID > 0)
                            {
                                fields += "Data_Origin_Code,";
                                values += "'" + dataOriginID + "',";
                                updateEquipmentFields += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                            }
                        }

                        fields += "Status,";
                        values += "'InStock',";

                        if (!String.IsNullOrEmpty(deviceType))
                        {
                            fields += "Device_Type,";
                            values += "'" + deviceType + "',";
                        }

                        fields += "COMP_ID,";
                        values += "'" + compId + "',";
                        updateEquipmentFields += "COMP_ID =" + "'" + compId + "',";

                        //Remove end Comma 
                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');
                        updateEquipmentFields = updateEquipmentFields.TrimEnd(',');

                        if (!isRejected)
                        {
                            equipId = equipment.GetEquipmentIdByUtilityId(equipmentUtilityId);

                            if (equipId > 0)
                            {
                                //update equipment
                                using (SqlConnection connection = new SqlConnection(constr))
                                {
                                    connection.Open();
                                    query = "update equipment set " + updateEquipmentFields + " where Utility_ID='" + equipmentUtilityId + "'";
                                    SqlCommand cmd = new SqlCommand(query, connection);
                                    cmd.ExecuteNonQuery();
                                    connection.Close();
                                }
                            }
                            else
                            {
                                //Equipment Entry
                                using (SqlConnection connection = new SqlConnection(constr))
                                {
                                    connection.Open();
                                    query = "insert into equipment (" + fields + ") values(" + values + ")";
                                    SqlCommand cmd = new SqlCommand(query, connection);
                                    cmd.ExecuteNonQuery();
                                    connection.Close();
                                }
                            }
                        }
                    }

                    //Exception_CIS_Sync
                    exceptionSync.UUID = uuid;
                    exceptionSync.Status = isRejected ? "Failure" : "Success";
                    exceptionSync.Message = errorMessage;
                    exceptionSync.CreatedBy = CreatedBy;
                    exceptionSync.ServiceName = Constants.EquipmentCreate_Bulk_MethodName;
                    Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                    exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);

                    //HES Work Flow in Equipment Bulk Create
                    if (!isRejected)
                    {
                        int refObjId = equipment.GetEquipmentIdByUtilityId(equipmentUtilityId);

                        hesWorkflowModel.WorkflowType = "EquipmentInventory";
                        hesWorkflowModel.RefrenceObjectType = "Equipment";
                        hesWorkflowModel.ReferenceObjId = Convert.ToString(refObjId);
                        hesWorkflowModel.WorkflowStatus = "1";
                        hesWorkflowModel.HESId = hesCode;
                        hesWorkflowModel.DataSource = "EquipmentCreate_Bulk";
                        hesWorkflowModel.CreatedBy = CreatedBy;
                        hesWorkflowModel.CreatedOn = DateTime.Now;

                        hesWorkflow.HesWorkFlowEntry(hesWorkflowModel);
                    }
                }

                if (!isRejected)
                {
                    //Write to File
                    var xmlString = Serialize<DeviceCreateBulk.UtilsDvceERPSmrtMtrBlkCrteReqMsg>(UtilitiesDeviceERPSmartMeterBulkCreateRequest);
                    string fileName = parentUUID + ".txt";
                    string root = Server.MapPath("~/CISAdapterLog/");
                    if (!Directory.Exists(root))
                    {
                        Directory.CreateDirectory(root);
                    }

                    string subdir = root + DateTime.Today.ToString("yyyy-MM-dd");
                    if (!Directory.Exists(subdir))
                    {
                        Directory.CreateDirectory(subdir);
                    }

                    File.WriteAllText(subdir + "/" + fileName, xmlString);
                }
                else
                {
                    var xmlStrings = Serialize<DeviceCreateBulk.UtilsDvceERPSmrtMtrBlkCrteReqMsg>(UtilitiesDeviceERPSmartMeterBulkCreateRequest);
                    WriteToLog(xmlStrings, uuid, Constants.Rejected_Path);
                }
                Confirmation_In();
            }
            catch (Exception ex)
            {
                string errorLog = "Method :EquipmentCreate_Bulk --- UUID :" + uuid + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                errorMessage += "Exception :" + ex.Message;
                Confirmation_In();
                var xmlString = Serialize<DeviceCreateBulk.UtilsDvceERPSmrtMtrBlkCrteReqMsg>(UtilitiesDeviceERPSmartMeterBulkCreateRequest);
                WriteToLog(xmlString, "EquipmentCreate_Bulk_Confirmation" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), "~/CISAdapterLog/Exception/");
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void EquipmentChange_Single(DeviceChange.UtilsDvceERPSmrtMtrChgReqMsg UtilitiesDeviceERPSmartMeterChangeRequest)
        {
            string query = "";
            string uuid = "";
            string ChangedBy = "";
            string fields = "";
            string utilityID = "";
            bool isRejected = false;
            string errorMessage = "";
            string configValue = "";
            string deviceType = "";
            int dataOriginID = 0;
            int hesCode = 0;

            DataSourceMaster dataSourceMaster = new DataSourceMaster();
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            Equipment equipment = new Equipment();

            void Confirmation_In()
            {
                #region Confirmation_In
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UtilitiesDeviceERPSmartMeterChangeConfirmation_InBinding cls = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UtilitiesDeviceERPSmartMeterChangeConfirmation_InBinding();
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UtilsDvceERPSmrtMtrChgConfMsg req = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UtilsDvceERPSmrtMtrChgConfMsg();
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.BusinessDocumentMessageHeader head = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.BusinessDocumentMessageHeader();
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UUID uid = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UUID REFuid = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UtilsDvceERPSmrtMtrChgConfUtilsDvce devie = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UtilsDvceERPSmrtMtrChgConfUtilsDvce();
                uid.Value = uuid;
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UtilitiesDeviceID deviceid = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.UtilitiesDeviceID();
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.Log log = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.Log();
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.LogItem[] logitem = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.LogItem[1];
                UtilitiesDeviceERPSmartMeterChangeConfirmation_In.LogItem logitemr = new UtilitiesDeviceERPSmartMeterChangeConfirmation_In.LogItem();
                System.Guid GUID;
                GUID = System.Guid.NewGuid();
                head.ReferenceUUID = uid;
                REFuid.Value = GUID.ToString();
                head.UUID = REFuid;
                head.CreationDateTime = DateTime.Now;
                head.SenderBusinessSystemID = "TPDDL";
                DataTable dturl = new DataTable();
                dturl.Clear();
                dturl = hes.GetSAPAMIUrl("EquipmentChange_Single_Confirmation");

                cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
                cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();

                req.MessageHeader = head;
                deviceid.Value = UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.ID.Value;
                devie.ID = deviceid;
                req.UtilitiesDevice = devie;
                log.BusinessDocumentProcessingResultCode = "3";
                log.MaximumLogItemSeverityCode = "1";
                logitemr.TypeID = "61204";
                if (isRejected)
                {
                    configValue = hes.GetConifigValueByParam(Constants.EquipmentChange_Single_MethodName, Constants.FailureSeverityCode);
                    logitemr.SeverityCode = configValue;
                    logitemr.Note = errorMessage;
                }
                else
                {
                    configValue = hes.GetConifigValueByParam(Constants.EquipmentChange_Single_MethodName, Constants.SuccessSeverityCode);
                    logitemr.SeverityCode = configValue;
                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        logitemr.Note = "Request Processed Successfully";
                    }
                }
                logitem[0] = logitemr;
                log.Item = logitem;
                req.Log = log;

                cls.UtilitiesDeviceERPSmartMeterChangeConfirmation_In(req);
                #endregion

            }
            try
            {
                #region CIS Log entry
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (UtilitiesDeviceERPSmartMeterChangeRequest.MessageHeader != null)
                {
                    if (UtilitiesDeviceERPSmartMeterChangeRequest.MessageHeader.SenderBusinessSystemID != null)
                    {
                        ChangedBy = UtilitiesDeviceERPSmartMeterChangeRequest.MessageHeader.SenderBusinessSystemID;
                    }
                    if (UtilitiesDeviceERPSmartMeterChangeRequest.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = UtilitiesDeviceERPSmartMeterChangeRequest.MessageHeader.UUID.Value;
                        uuid = UtilitiesDeviceERPSmartMeterChangeRequest.MessageHeader.UUID.Value;
                    }

                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    if (UtilitiesDeviceERPSmartMeterChangeRequest.MessageHeader.CreationDateTime != null &&
                        UtilitiesDeviceERPSmartMeterChangeRequest.MessageHeader.CreationDateTime != DateTime.MinValue)
                    {
                        DateTime creationDateTime = dateTimeValidator.CheckDate(UtilitiesDeviceERPSmartMeterChangeRequest.MessageHeader.CreationDateTime);
                        cISAdaptorParent.LogDateTime = creationDateTime;
                    }
                }
                else
                {
                    uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                //cISAdaptorParent.Child_Count = 0;
                cISAdaptorParent.Request_Type = "EquipmentChange_Single";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);
                #endregion

                ExceptionCISSync exceptionSync = new ExceptionCISSync();
                Material materialMaster;
                Manufacturer manufacturerMaster;
                //Validations
                if (UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice != null)
                {
                    if (UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.ID != null
                        && UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.ID.Value != null)
                    {
                        utilityID = UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.ID.Value;
                    }
                    else
                    {
                        isRejected = true;
                        errorMessage += "Utility_ID Missing;";
                    }

                    if (!String.IsNullOrEmpty(UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.SerialID))
                    {
                        fields += "Meter_ID = '" + UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.SerialID + "',";
                    }
                    else
                    {
                        isRejected = true;
                        errorMessage += "SerialID Missing;";
                    }

                    if (UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.MaterialID != null
                        && UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.MaterialID.Value != null)
                    {
                        string materialId = UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.MaterialID.Value;
                        fields += "Material_ID ='" + materialId + "',";

                        Material_Master material_Master = new Material_Master();
                        materialMaster = material_Master.GetMaterialByMaterialUtilityId(materialId);

                        if (materialMaster.ID > 0)
                        {
                            deviceType = materialMaster.DeviceType;
                        }
                        else
                        {
                            isRejected = true;
                            errorMessage += "Material_ID Not Found in Material_Master;";
                        }
                    }
                    else
                    {
                        isRejected = true;
                        errorMessage += "Material_ID Missing;";
                    }


                    // IndividualMaterialManufacturerInformation node
                    if (UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation != null)
                    {
                        if (UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID != null
                            && UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value != null)
                        {
                            string manufacturerId = UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value;

                            Manufacturer_Master manufacturer_Master = new Manufacturer_Master();
                            manufacturerMaster = manufacturer_Master.GetManufacturerById(manufacturerId);

                            if (manufacturerMaster.ID > 0)
                            {
                                fields += "Manufacturer_ID = '" + manufacturerId + "',"; ;
                            }
                            else
                            {
                                isRejected = true;
                                errorMessage += "Manufacturer_ID Not Found in Manufacturer_Master;";
                            }
                        }

                        if (UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID != null
                        && UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value != null)
                        {
                            fields += "Model_ID = '" + UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartNumberID.Value + "',";
                        }
                        if (!String.IsNullOrEmpty(UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID))
                        {
                            fields += "Name_Plate_ID = '" + UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.IndividualMaterialManufacturerInformation.SerialID + "',";
                        }
                    }

                    //SmartMeter node
                    if (UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.SmartMeter != null)
                    {
                        if (UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID != null
                            && UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value != null)
                        {
                            //Get HES Code
                            hesCode = hes.GetHESIdByHESCode(UtilitiesDeviceERPSmartMeterChangeRequest.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value);
                            if (hesCode > 0)
                            {
                                fields += "HES_ID " + " = " + hesCode + ",";
                            }
                            else
                            {
                                isRejected = true;
                                errorMessage += "HES_CD Not Found in HES_MASTER;";
                            }
                        }
                    }

                    if (ChangedBy != null)
                    {
                        fields += "Changed_by = '" + ChangedBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                        dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(ChangedBy);
                        if (dataOriginID > 0)
                        {
                            fields += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                        }
                    }

                    int equipId = equipment.GetEquipmentIdByUtilityId(utilityID);

                    if (equipId <= 0)
                    {
                        isRejected = true;
                        errorMessage += "Utility_ID not found in Equipment table;";
                    }

                    fields += "COMP_ID =" + "'" + compId + "',";

                    //Remove end Comma 
                    fields = fields.TrimEnd(',');
                    if (!isRejected)
                    {
                        //Equipment Update
                        using (SqlConnection connection = new SqlConnection(constr))
                        {
                            connection.Open();
                            query = "update Equipment set " + fields + " where Utility_ID = " + utilityID;
                            SqlCommand cmd = new SqlCommand(query, connection);
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }

                        //Write to File
                        var xmlString = Serialize<DeviceChange.UtilsDvceERPSmrtMtrChgReqMsg>(UtilitiesDeviceERPSmartMeterChangeRequest);
                        WriteToLog(xmlString, uuid, Constants.CisAdaptorLog_Path);//Constants.CisAdaptorLog_Path                        
                    }
                    else
                    {
                        var xmlStrings = Serialize<DeviceChange.UtilsDvceERPSmrtMtrChgReqMsg>(UtilitiesDeviceERPSmartMeterChangeRequest);
                        WriteToLog(xmlStrings, uuid, Constants.Rejected_Path);
                    }
                    Confirmation_In();
                }
                //Exception_CIS_Sync
                exceptionSync.UUID = uuid;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = ChangedBy;
                exceptionSync.ServiceName = Constants.EquipmentChange_Single_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);

                //HES Work Flow in Equipment Change Single
                if (!isRejected)
                {
                    int refObjId = equipment.GetEquipmentIdByUtilityId(utilityID);

                    hesWorkflowModel.WorkflowType = "EquipmentInventory";
                    hesWorkflowModel.RefrenceObjectType = "Equipment";
                    hesWorkflowModel.ReferenceObjId = Convert.ToString(refObjId);
                    hesWorkflowModel.WorkflowStatus = "1";
                    hesWorkflowModel.HESId = hesCode;
                    hesWorkflowModel.DataSource = "EquipmentChange_Single";
                    hesWorkflowModel.CreatedBy = ChangedBy;
                    hesWorkflowModel.CreatedOn = DateTime.Now;

                    hesWorkflow.HesWorkFlowEntry(hesWorkflowModel);
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :EquipmentChange_Single --- UUID :" + uuid + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                errorMessage += "Exception :" + ex.Message;
                Confirmation_In();
                var xmlString = Serialize<DeviceChange.UtilsDvceERPSmrtMtrChgReqMsg>(UtilitiesDeviceERPSmartMeterChangeRequest);
                WriteToLog(xmlString, "EquipmentChange_Single" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), "~/CISAdapterLog/Exception/");
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void RegisterCreate(UtilitiesDeviceERPSmartMeterRegisterCreateRequest_Out.UtilsDvceERPSmrtMtrRegCrteReqMsg UtilitiesDeviceERPSmartMeterRegisterCreateRequest)
        {
            string query = "";
            string uuid = "";
            string CreatedBy = "";
            string configValue = "";
            string errorMessage = "";
            bool isRejected = false;
            int dataOriginID = 0;
            DataSourceMaster dataSourceMaster = new DataSourceMaster();
            ExceptionCISSync exceptionSync = new ExceptionCISSync();
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            void Confirmation_in()
            {
                #region Confirmation_In
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_InBinding cls = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_InBinding();
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UtilsDvceERPSmrtMtrRegCrteConfMsg req = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UtilsDvceERPSmrtMtrRegCrteConfMsg();
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.BusinessDocumentMessageHeader head = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.BusinessDocumentMessageHeader();
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UUID uid = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UUID REFuid = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UtilsDvceERPSmrtMtrRegCrteConfUtilsDvce devie = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UtilsDvceERPSmrtMtrRegCrteConfUtilsDvce();
                uid.Value = uuid;
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UtilitiesDeviceID deviceid = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.UtilitiesDeviceID();
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.Log log = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.Log();
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.LogItem[] logitem = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.LogItem[1];
                UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.LogItem logitemr = new UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In.LogItem();

                System.Guid GUID;
                GUID = System.Guid.NewGuid();
                head.ReferenceUUID = uid;
                REFuid.Value = GUID.ToString();
                head.UUID = REFuid;
                head.CreationDateTime = DateTime.Now;
                head.SenderBusinessSystemID = "TPDDL";
                DataTable dturl = new DataTable();
                dturl.Clear();
                dturl = hes.GetSAPAMIUrl("RegisterCreate_Confirmation");

                cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
                cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();

                req.MessageHeader = head;
                deviceid.Value = UtilitiesDeviceERPSmartMeterRegisterCreateRequest.UtilitiesDevice.ID.Value;
                devie.ID = deviceid;
                req.UtilitiesDevice = devie;
                log.BusinessDocumentProcessingResultCode = "3";
                log.MaximumLogItemSeverityCode = "1";
                logitemr.TypeID = "61204";
                if (isRejected)
                {
                    configValue = hes.GetConifigValueByParam(Constants.RegisterCreate_MethodName, Constants.FailureSeverityCode);
                    logitemr.SeverityCode = configValue;
                    logitemr.Note = errorMessage;
                }
                else
                {
                    configValue = hes.GetConifigValueByParam(Constants.RegisterCreate_MethodName, Constants.SuccessSeverityCode);
                    logitemr.SeverityCode = configValue;
                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        logitemr.Note = "Request Processed Successfully";
                    }
                }
                logitem[0] = logitemr;
                log.Item = logitem;
                req.Log = log;

                cls.UtilitiesDeviceERPSmartMeterRegisterCreateConfirmation_In(req);
                #endregion

            }
            try
            {
                //CIS Log entry
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (UtilitiesDeviceERPSmartMeterRegisterCreateRequest.MessageHeader != null)
                {
                    if (UtilitiesDeviceERPSmartMeterRegisterCreateRequest.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = UtilitiesDeviceERPSmartMeterRegisterCreateRequest.MessageHeader.UUID.Value;
                        uuid = UtilitiesDeviceERPSmartMeterRegisterCreateRequest.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    if (UtilitiesDeviceERPSmartMeterRegisterCreateRequest.MessageHeader.SenderBusinessSystemID != null)
                    {
                        CreatedBy = UtilitiesDeviceERPSmartMeterRegisterCreateRequest.MessageHeader.SenderBusinessSystemID;
                    }
                    cISAdaptorParent.LogDateTime = UtilitiesDeviceERPSmartMeterRegisterCreateRequest.MessageHeader.CreationDateTime;
                }
                else
                {
                    uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Request_Type = "RegisterCreate";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);


                int EquipmentId = 0;


                //Validations
                if (UtilitiesDeviceERPSmartMeterRegisterCreateRequest.UtilitiesDevice != null)
                {
                    if (UtilitiesDeviceERPSmartMeterRegisterCreateRequest.UtilitiesDevice.ID != null
                        && UtilitiesDeviceERPSmartMeterRegisterCreateRequest.UtilitiesDevice.ID.Value != null)
                    {
                        var UtilityId = UtilitiesDeviceERPSmartMeterRegisterCreateRequest.UtilitiesDevice.ID.Value;

                        Equipment equipment = new Equipment();
                        EquipmentId = equipment.GetEquipmentIdByUtilityId(UtilityId);

                        if (EquipmentId > 0)
                        {

                            //Equipment_Reg_Link
                            if (UtilitiesDeviceERPSmartMeterRegisterCreateRequest.UtilitiesDevice.Register != null)
                            {
                                foreach (var utilsDvceERPSmrtMtrRegCrteReqMsg in UtilitiesDeviceERPSmartMeterRegisterCreateRequest.UtilitiesDevice.Register)
                                {
                                    string fields = "";
                                    string values = "";

                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.StartDate != DateTime.MinValue)
                                    {
                                        fields += "ValidFrom,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.EndDate != DateTime.MinValue)
                                    {
                                        fields += "ValidTo,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesMeasurementTaskID != null && utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesMeasurementTaskID.Value != null)
                                    {
                                        fields += "Log_Reg_no,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesMeasurementTaskID.Value + "',";
                                    }
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesObjectIdentificationSystemCodeText))
                                    {
                                        fields += "UOBIS,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesObjectIdentificationSystemCodeText + "',";
                                    }
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.UtiltiesMeasurementTaskCategoryCode))
                                    {
                                        fields += "Cat_Code,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtiltiesMeasurementTaskCategoryCode + "',";
                                    }
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesDivisionCategoryCode))
                                    {
                                        fields += "Div_code,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesDivisionCategoryCode + "',";
                                    }
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.TimeZoneCode))
                                    {
                                        fields += "TimeZone,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.TimeZoneCode + "',";
                                    }
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications.MeasureUnitCode))
                                    {
                                        fields += "MeasCode,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications.MeasureUnitCode + "',";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications.DecimalValuePrecision.TotalDigitNumberValue > 0)
                                    {
                                        fields += "No_Digits,";
                                        values += utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications.DecimalValuePrecision.TotalDigitNumberValue + ",";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications.DecimalValuePrecision.FractionDigitNumberValue > 0)
                                    {
                                        fields += " Dec_Digits,";
                                        values += utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications.DecimalValuePrecision.FractionDigitNumberValue + ",";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications.MeterReadingResultAdjustmentFactorValue > 0)
                                    {
                                        fields += "Factor,";
                                        values += utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications.MeterReadingResultAdjustmentFactorValue + ",";
                                    }
                                    if (EquipmentId > 0)
                                    {
                                        fields += "Equipment_Id,";
                                        values += EquipmentId + ",";
                                    }
                                    if (!String.IsNullOrEmpty(CreatedBy))
                                    {
                                        fields += "Created_by,";
                                        values += "'" + CreatedBy + "',";

                                        dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(CreatedBy);
                                        if (dataOriginID > 0)
                                        {
                                            fields += "Data_Origin_Code,";
                                            values += "'" + dataOriginID + "',";
                                        }
                                    }
                                    fields += "Created_on,";
                                    values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                    //Remove end Comma 
                                    fields = fields.TrimEnd(',');
                                    values = values.TrimEnd(',');

                                    //Equipment_Reg_Link Entry
                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        connection.Open();
                                        query = "insert into Equipment_Reg_Link (" + fields + ") values(" + values + ")";
                                        SqlCommand cmd = new SqlCommand(query, connection);
                                        cmd.ExecuteNonQuery();
                                        connection.Close();
                                    }
                                }
                            }
                        }
                        else
                        {
                            var xmlStrings = Serialize<UtilitiesDeviceERPSmartMeterRegisterCreateRequest_Out.UtilsDvceERPSmrtMtrRegCrteReqMsg>(UtilitiesDeviceERPSmartMeterRegisterCreateRequest);
                            WriteToLog(xmlStrings, uuid, Constants.Rejected_Path);

                            isRejected = true;
                        }
                    }

                }

                if (!isRejected)
                {
                    //Write to File
                    var xmlString = Serialize<UtilitiesDeviceERPSmartMeterRegisterCreateRequest_Out.UtilsDvceERPSmrtMtrRegCrteReqMsg>(UtilitiesDeviceERPSmartMeterRegisterCreateRequest);
                    string fileName = uuid + ".txt";
                    string root = Server.MapPath("~/CISAdapterLog/");
                    if (!Directory.Exists(root))
                    {
                        Directory.CreateDirectory(root);
                    }
                    string subdir = root + DateTime.Today.ToString("yyyy-MM-dd");
                    if (!Directory.Exists(subdir))
                    {
                        Directory.CreateDirectory(subdir);
                    }
                    File.WriteAllText(subdir + "/" + fileName, xmlString);
                }
                else
                {

                }
                Confirmation_in();
                //Exception_CIS_Sync
                exceptionSync.UUID = uuid;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = CreatedBy;
                exceptionSync.ServiceName = Constants.RegisterCreate_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :RegisterCreate --- UUID :" + uuid + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                errorMessage += "Exception :" + ex.Message;
                Confirmation_in();
                var xmlString = Serialize<UtilitiesDeviceERPSmartMeterRegisterCreateRequest_Out.UtilsDvceERPSmrtMtrRegCrteReqMsg>(UtilitiesDeviceERPSmartMeterRegisterCreateRequest);
                WriteToLog(xmlString, Constants.MeterReading_Response_Bulk_Confirmation_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void RegisterChange(UtilitiesDeviceERPSmartMeterRegisterChangeRequest_Out.UtilsDvceERPSmrtMtrRegChgReqMsg UtilitiesDeviceERPSmartMeterRegisterChangeRequest)
        {
            string query = "";
            string uuid = "";
            string createdBy = "";
            string configValue = "";
            string errorMessage = "";
            bool isRejected = false;
            int dataOriginID = 0;
            DataSourceMaster dataSourceMaster = new DataSourceMaster();
            ExceptionCISSync exceptionSync = new ExceptionCISSync();
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            void Confirmation_In()
            {
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_InBinding cls = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_InBinding();
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UtilsDvceERPSmrtMtrRegChgConfMsg req = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UtilsDvceERPSmrtMtrRegChgConfMsg();
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.BusinessDocumentMessageHeader head = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.BusinessDocumentMessageHeader();
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UUID uid = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UUID REFuid = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UtilsDvceERPSmrtMtrRegChgConfUtilsDvce devie = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UtilsDvceERPSmrtMtrRegChgConfUtilsDvce();
                uid.Value = uuid;
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UtilitiesDeviceID deviceid = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.UtilitiesDeviceID();
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.Log log = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.Log();
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.LogItem[] logitem = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.LogItem[1];
                UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.LogItem logitemr = new UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In.LogItem();

                System.Guid GUID;
                GUID = System.Guid.NewGuid();
                head.ReferenceUUID = uid;
                REFuid.Value = GUID.ToString();
                head.UUID = REFuid;
                head.CreationDateTime = DateTime.Now;
                head.SenderBusinessSystemID = "TPDDL";
                DataTable dturl = new DataTable();
                dturl.Clear();
                dturl = hes.GetSAPAMIUrl("RegisterChange_Confirmation");

                cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
                cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();

                req.MessageHeader = head;
                deviceid.Value = UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice.ID.Value;
                devie.ID = deviceid;
                req.UtilitiesDevice = devie;
                log.BusinessDocumentProcessingResultCode = "3";
                log.MaximumLogItemSeverityCode = "1";
                logitemr.TypeID = "61204";
                if (isRejected)
                {
                    configValue = hes.GetConifigValueByParam(Constants.RegisterChange_MethodName, Constants.FailureSeverityCode);
                    logitemr.SeverityCode = configValue;
                    logitemr.Note = errorMessage;
                }
                else
                {
                    configValue = hes.GetConifigValueByParam(Constants.RegisterChange_MethodName, Constants.SuccessSeverityCode);
                    logitemr.SeverityCode = configValue;
                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        logitemr.Note = "Request Processed Successfully";
                    }
                }
                logitem[0] = logitemr;
                log.Item = logitem;
                req.Log = log;

                cls.UtilitiesDeviceERPSmartMeterRegisterChangeConfirmation_In(req);
            }
            try
            {
                //CIS Log entry
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (UtilitiesDeviceERPSmartMeterRegisterChangeRequest.MessageHeader != null)
                {
                    if (UtilitiesDeviceERPSmartMeterRegisterChangeRequest.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = UtilitiesDeviceERPSmartMeterRegisterChangeRequest.MessageHeader.UUID.Value;
                        uuid = UtilitiesDeviceERPSmartMeterRegisterChangeRequest.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    if (UtilitiesDeviceERPSmartMeterRegisterChangeRequest.MessageHeader.SenderBusinessSystemID != null)
                    {
                        createdBy = UtilitiesDeviceERPSmartMeterRegisterChangeRequest.MessageHeader.SenderBusinessSystemID;
                    }
                    cISAdaptorParent.LogDateTime = UtilitiesDeviceERPSmartMeterRegisterChangeRequest.MessageHeader.CreationDateTime;
                }
                else
                {
                    uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Request_Type = "RegisterChange";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);


                int EquipmentId = 0;
                string logRegNo = "";

                //Validations
                if (UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice != null)
                {
                    if (UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice.ID != null
                        && UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice.ID.Value != null)
                    {
                        var UtilityId = UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice.ID.Value;

                        Equipment equipment = new Equipment();
                        EquipmentId = equipment.GetEquipmentIdByUtilityId(UtilityId);

                        if (EquipmentId > 0)
                        {

                            //Equipment_Reg_Link
                            if (UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice.Register != null)
                            {
                                foreach (var utilsDvceERPSmrtMtrRegCrteReqMsg in UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice.Register)
                                {
                                    string fields = "";
                                    string values = "";
                                    string updatefields = "";

                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.StartDate != DateTime.MinValue)
                                    {
                                        fields += "ValidFrom,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                        updatefields += "ValidFrom =" + "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.EndDate != DateTime.MinValue)
                                    {
                                        fields += "ValidTo,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                        updatefields += "ValidTo =" + "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesMeasurementTaskID != null && utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesMeasurementTaskID.Value != null)
                                    {
                                        fields += "Log_Reg_no,";
                                        logRegNo = utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesMeasurementTaskID.Value;
                                        values += "'" + logRegNo + "',";
                                        updatefields += "Log_Reg_no =" + "'" + logRegNo + "',";
                                    }
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesObjectIdentificationSystemCodeText))
                                    {
                                        fields += "UOBIS,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesObjectIdentificationSystemCodeText + "',";
                                        updatefields += "UOBIS =" + "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesObjectIdentificationSystemCodeText + "',";
                                    }
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.UtiltiesMeasurementTaskCategoryCode))
                                    {
                                        fields += "Cat_Code,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtiltiesMeasurementTaskCategoryCode + "',";
                                        updatefields += "Cat_Code =" + "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtiltiesMeasurementTaskCategoryCode + "',";
                                    }
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesDivisionCategoryCode))
                                    {
                                        fields += "Div_code,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesDivisionCategoryCode + "',";
                                        updatefields += "Div_code =" + "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.UtilitiesDivisionCategoryCode + "',";
                                    }
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.TimeZoneCode))
                                    {
                                        fields += "TimeZone,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.TimeZoneCode + "',";
                                        updatefields += "TimeZone =" + "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.TimeZoneCode + "',";
                                    }
                                    //Start Specification Array
                                    if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].MeasureUnitCode))
                                    {
                                        fields += "MeasCode,";
                                        values += "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].MeasureUnitCode + "',";
                                        updatefields += "MeasCode =" + "'" + utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].MeasureUnitCode + "',";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].DecimalValuePrecision.TotalDigitNumberValue > 0)
                                    {
                                        fields += "No_Digits,";
                                        values += utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].DecimalValuePrecision.TotalDigitNumberValue + ",";
                                        updatefields += "No_Digits =" + utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].DecimalValuePrecision.TotalDigitNumberValue + ",";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].DecimalValuePrecision.FractionDigitNumberValue > 0)
                                    {
                                        fields += " Dec_Digits,";
                                        values += utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].DecimalValuePrecision.FractionDigitNumberValue + ",";
                                        updatefields += "Dec_Digits =" + utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].DecimalValuePrecision.FractionDigitNumberValue + ",";
                                    }
                                    if (utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].MeterReadingResultAdjustmentFactorValue > 0)
                                    {
                                        fields += "Factor,";
                                        values += utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].MeterReadingResultAdjustmentFactorValue + ",";
                                        updatefields += "Factor =" + utilsDvceERPSmrtMtrRegCrteReqMsg.Specifications[0].MeterReadingResultAdjustmentFactorValue + ",";
                                    }
                                    //End Specification Array
                                    if (EquipmentId > 0)
                                    {
                                        fields += "Equipment_Id,";
                                        values += EquipmentId + ",";
                                        updatefields += "Equipment_Id =" + EquipmentId + ",";
                                    }
                                    if (!String.IsNullOrEmpty(createdBy))
                                    {
                                        fields += "Created_by,";
                                        values += "'" + createdBy + "',";
                                        updatefields += "Changed_by = '" + createdBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                        dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(createdBy);
                                        if (dataOriginID > 0)
                                        {
                                            fields += "Data_Origin_Code,";
                                            values += "'" + dataOriginID + "',";
                                            updatefields += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                                        }
                                    }
                                    fields += "Created_on,";
                                    values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";


                                    //Remove end Comma 
                                    fields = fields.TrimEnd(',');
                                    values = values.TrimEnd(',');
                                    updatefields = updatefields.TrimEnd(',');

                                    //Check Equipment_Reg_Link table with Log_Reg_no and Equipment_Id.
                                    Equipment_Reg_Link equipment_Reg_Link = new Equipment_Reg_Link();
                                    EquipmentRegLink equipmentRegLink = equipment_Reg_Link.GetEquipmentRegLinkByLogRegNoAndEquipmentId(logRegNo, EquipmentId);

                                    if (equipmentRegLink.EquipmentId > 0 && !String.IsNullOrEmpty(equipmentRegLink.LogRegNo))
                                    {
                                        //update the record Equipment_Reg_Link
                                        using (SqlConnection connection = new SqlConnection(constr))
                                        {
                                            connection.Open();
                                            query = "update Equipment_Reg_Link set " + updatefields + " where Log_Reg_no='" + equipmentRegLink.LogRegNo + "' and Equipment_Id=" + equipmentRegLink.EquipmentId;
                                            SqlCommand cmd = new SqlCommand(query, connection);
                                            cmd.ExecuteNonQuery();
                                            connection.Close();
                                        }
                                    }
                                    else
                                    {
                                        //Equipment_Reg_Link Entry
                                        using (SqlConnection connection = new SqlConnection(constr))
                                        {
                                            connection.Open();
                                            query = "insert into Equipment_Reg_Link (" + fields + ") values(" + values + ")";
                                            SqlCommand cmd = new SqlCommand(query, connection);
                                            cmd.ExecuteNonQuery();
                                            connection.Close();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            var xmlStrings = Serialize<UtilitiesDeviceERPSmartMeterRegisterChangeRequest_Out.UtilsDvceERPSmrtMtrRegChgReqMsg>(UtilitiesDeviceERPSmartMeterRegisterChangeRequest);
                            WriteToLog(xmlStrings, uuid, Constants.Rejected_Path);

                            isRejected = true;
                        }
                    }

                }

                if (!isRejected)
                {
                    //Write to File
                    var xmlString = Serialize<UtilitiesDeviceERPSmartMeterRegisterChangeRequest_Out.UtilsDvceERPSmrtMtrRegChgReqMsg>(UtilitiesDeviceERPSmartMeterRegisterChangeRequest);
                    WriteToLog(xmlString, uuid, Constants.CisAdaptorLog_Path);
                }

                Confirmation_In();

                //Exception_CIS_Sync
                exceptionSync.UUID = uuid;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = createdBy;
                exceptionSync.ServiceName = Constants.RegisterChange_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :RegisterChange --- UUID :" + uuid + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                errorMessage += "Exception :" + ex.Message;
                Confirmation_In();
                var xmlString = Serialize<UtilitiesDeviceERPSmartMeterRegisterChangeRequest_Out.UtilsDvceERPSmrtMtrRegChgReqMsg>(UtilitiesDeviceERPSmartMeterRegisterChangeRequest);
                WriteToLog(xmlString, Constants.RegisterChange_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void ReplicationRequest(UtilitiesDeviceERPSmartMeterReplicationBulkRequest_Out.UtilsDvceERPSmrtMtrRplctnBulkReqMsg UtilitiesDeviceERPSmartMeterReplicationBulkRequest)
        {
            string query = "";
            string errorMessage = "";
            string uuid = "";
            string createdBy = "";
            string parentUUID = "";
            string configValue = "";
            bool isRejected = false;
            int dataOriginID = 0;
            DataSourceMaster dataSourceMaster = new DataSourceMaster();
            ExceptionCISSync exceptionSync = new ExceptionCISSync();
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            void Confirmation_In()
            {
                #region Confirmation_In
                UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_InBinding cls = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_InBinding();
                UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilsDvceERPSmrtMtrRplctnBulkConfMsg req = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilsDvceERPSmrtMtrRplctnBulkConfMsg();
                UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.BusinessDocumentMessageHeader head = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.BusinessDocumentMessageHeader();
                UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UUID uid = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UUID();
                UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UUID REFuid = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UUID();
                uid.Value = parentUUID;
                UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.Log log = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.Log();
                UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.LogItem[] logitem = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.LogItem[1];
                UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.LogItem logitemr = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.LogItem();
                UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilsDvceERPSmrtMtrRplctnConfMsg[] MeterList = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilsDvceERPSmrtMtrRplctnConfMsg[UtilitiesDeviceERPSmartMeterReplicationBulkRequest.UtilitiesDeviceERPSmartMeterReplicationRequestMessage.Length];

                System.Guid GUID;
                GUID = System.Guid.NewGuid();
                head.ReferenceUUID = uid;
                REFuid.Value = GUID.ToString();
                head.UUID = REFuid;
                head.CreationDateTime = DateTime.Now;
                head.SenderBusinessSystemID = "TPDDL";
                DataTable dturl = new DataTable();
                dturl.Clear();
                dturl = hes.GetSAPAMIUrl("ReplicationRequest_Confirmation");

                cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
                cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();

                req.MessageHeader = head;
                int j = 0;
                foreach (var utilsDvceERPSmrtMtrCrteReqMsg in UtilitiesDeviceERPSmartMeterReplicationBulkRequest.UtilitiesDeviceERPSmartMeterReplicationRequestMessage)
                {
                    UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilsDvceERPSmrtMtrRplctnConfMsg Meter = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilsDvceERPSmrtMtrRplctnConfMsg();
                    UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.BusinessDocumentMessageHeader Childhead = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.BusinessDocumentMessageHeader();
                    UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilsDvceERPSmrtMtrRplctnConfUtilsDvce devie = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilsDvceERPSmrtMtrRplctnConfUtilsDvce();
                    UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilitiesDeviceID deviceid = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UtilitiesDeviceID();
                    UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UUID childUuid = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UUID();
                    UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UUID childREFuid = new UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In.UUID();

                    Guid childGUID = System.Guid.NewGuid();
                    childUuid.Value = utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.UUID.Value;
                    deviceid.Value = utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.ID.Value;
                    devie.ID = deviceid;
                    Childhead.ReferenceUUID = childUuid;
                    childREFuid.Value = childGUID.ToString();
                    Childhead.UUID = childREFuid;
                    Childhead.CreationDateTime = DateTime.Now;
                    Childhead.SenderBusinessSystemID = "TPDDL";
                    //Childhead. = devie;
                    Meter.MessageHeader = Childhead;
                    Meter.UtilitiesDevice = devie;
                    log.BusinessDocumentProcessingResultCode = "3";
                    log.MaximumLogItemSeverityCode = "1";
                    logitemr.TypeID = "61204";
                    if (isRejected)
                    {
                        configValue = hes.GetConifigValueByParam(Constants.ReplicationRequest_MethodName, Constants.FailureSeverityCode);
                        logitemr.SeverityCode = configValue;
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        configValue = hes.GetConifigValueByParam(Constants.ReplicationRequest_MethodName, Constants.SuccessSeverityCode);
                        logitemr.SeverityCode = configValue;
                        if (!String.IsNullOrEmpty(errorMessage))
                        {
                            logitemr.Note = errorMessage;
                        }
                        else
                        {
                            logitemr.Note = "Request Processed Successfully";
                        }
                    }
                    logitem[0] = logitemr;
                    log.Item = logitem;
                    Meter.Log = log;
                    MeterList[j] = Meter;

                    j++;
                }

                req.UtilitiesDeviceERPSmartMeterReplicationConfirmationMessage = MeterList;
                log.BusinessDocumentProcessingResultCode = "3";
                log.MaximumLogItemSeverityCode = "1";
                logitemr.TypeID = "61204";
                if (isRejected)
                {
                    configValue = hes.GetConifigValueByParam(Constants.ReplicationRequest_MethodName, Constants.FailureSeverityCode);
                    logitemr.SeverityCode = configValue;
                    logitemr.Note = errorMessage;
                }
                else
                {
                    configValue = hes.GetConifigValueByParam(Constants.ReplicationRequest_MethodName, Constants.SuccessSeverityCode);
                    logitemr.SeverityCode = configValue;
                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        logitemr.Note = "Request Processed Successfully";
                    }
                }
                logitem[0] = logitemr;
                log.Item = logitem;
                req.Log = log;

                cls.UtilitiesDeviceERPSmartMeterReplicationBulkConfirmation_In(req);
                #endregion

            }
            try
            {
                #region CIS_ADAPTOR_LOG
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (UtilitiesDeviceERPSmartMeterReplicationBulkRequest.MessageHeader != null)
                {
                    if (UtilitiesDeviceERPSmartMeterReplicationBulkRequest.MessageHeader.SenderBusinessSystemID != null)
                    {
                        createdBy = UtilitiesDeviceERPSmartMeterReplicationBulkRequest.MessageHeader.SenderBusinessSystemID;
                    }
                    if (UtilitiesDeviceERPSmartMeterReplicationBulkRequest.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = UtilitiesDeviceERPSmartMeterReplicationBulkRequest.MessageHeader.UUID.Value;
                        parentUUID = UtilitiesDeviceERPSmartMeterReplicationBulkRequest.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    cISAdaptorParent.LogDateTime = UtilitiesDeviceERPSmartMeterReplicationBulkRequest.MessageHeader.CreationDateTime;
                }
                else
                {
                    parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Child_Count = UtilitiesDeviceERPSmartMeterReplicationBulkRequest.UtilitiesDeviceERPSmartMeterReplicationRequestMessage.Length;
                cISAdaptorParent.Request_Type = "ReplicationRequest";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);
                #endregion

                DeviceLocation deviceLocation = new DeviceLocation();
                DevLocDeviceLink devLocDeviceLink = new DevLocDeviceLink();
                //Validation
                foreach (var utilsDvceERPSmrtMtrCrteReqMsg in UtilitiesDeviceERPSmartMeterReplicationBulkRequest.UtilitiesDeviceERPSmartMeterReplicationRequestMessage)
                {
                    #region CIS_ADAPTOR_CHILD_LOG
                    CISAdaptorChild cISAdaptorChild = new CISAdaptorChild();

                    if (utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader != null)
                    {
                        if (utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.UUID != null)
                        {
                            cISAdaptorChild.UUID = utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.UUID.Value;
                            uuid = utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.UUID.Value;
                        }
                        else
                        {
                            uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                        }
                        cISAdaptorChild.LogDateTime = utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.CreationDateTime;
                    }
                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    cISAdaptorChild.Parent_UUID = parentUUID;
                    cISAdaptorChild.Request_Type = "ReplicationRequest";
                    cisAdaptorLog.CISAdaptorChildLog(cISAdaptorChild);
                    #endregion

                    string equipFields = "";
                    string equipValues = "";
                    string equipUpdateValues = "";
                    string utilityId = "";
                    int equipmentId = 0;
                    string devLocUtilityId = "";
                    int deviceLocId = 0;
                    Equipment equipment = new Equipment();
                    Equipment_Reg_Link equipment_Reg_Link = new Equipment_Reg_Link();

                    if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice != null)
                    {
                        #region Equipment add update
                        if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.ID != null
                            && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.ID.Value != null)
                        {
                            utilityId = utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.ID.Value;
                            equipmentId = equipment.GetEquipmentIdByUtilityId(utilityId);

                            if (!string.IsNullOrEmpty(utilityId))
                            {
                                equipFields += "Utility_ID,";
                                equipValues += utilityId + ",";
                            }
                            if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.StartDate != DateTime.MinValue)
                            {
                                equipFields += "StartDate,";
                                equipValues += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                equipUpdateValues += "StartDate = '" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                            }
                            if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.EndDate != DateTime.MinValue)
                            {
                                equipFields += "EndDate,";
                                equipValues += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                equipUpdateValues += "EndDate = '" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                            }
                            if (!String.IsNullOrEmpty(utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SerialID))
                            {
                                equipFields += "Meter_ID,";
                                equipValues += utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SerialID + ",";
                                equipUpdateValues += "Meter_ID =" + "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SerialID + "',";
                            }
                            if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.MaterialID != null
                                && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.MaterialID.Value != null)
                            {
                                equipFields += "Material_ID,";
                                equipValues += utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.MaterialID.Value + ",";
                                equipUpdateValues += "Material_ID = '" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.MaterialID.Value + "',";
                            }
                            if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation != null)
                            {
                                if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID != null
                                                            && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value != null)
                                {
                                    equipFields += "Manufacturer_ID,";
                                    equipValues += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value + "',";
                                    equipUpdateValues += "Manufacturer_ID = '" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.IndividualMaterialManufacturerInformation.PartyInternalID.Value + "',";
                                }
                            }
                            if (createdBy != null)
                            {
                                equipFields += "Created_by,";
                                equipValues += "'" + createdBy + "',";
                                equipUpdateValues += "changed_by = '" + createdBy + "', changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(createdBy);
                                if (dataOriginID > 0)
                                {
                                    equipFields += "Data_Origin_Code,";
                                    equipValues += "'" + dataOriginID + "',";
                                    equipUpdateValues += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                                }
                            }
                            equipFields += "Created_on,";
                            equipValues += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                            //Remove end Comma 
                            equipFields = equipFields.TrimEnd(',');
                            equipValues = equipValues.TrimEnd(',');
                            equipUpdateValues = equipUpdateValues.TrimEnd(',');

                            if (equipmentId > 0)
                            {
                                using (SqlConnection connection = new SqlConnection(constr))
                                {
                                    connection.Open();
                                    query = "update Equipment set " + equipUpdateValues + "where Utility_ID = '" + utilityId + "'";
                                    SqlCommand cmd = new SqlCommand(query, connection);
                                    cmd.ExecuteNonQuery();
                                    connection.Close();
                                }
                            }
                            else
                            {
                                using (SqlConnection connection = new SqlConnection(constr))
                                {
                                    connection.Open();
                                    query = "insert into Equipment (" + equipFields + ") values(" + equipValues + ")";
                                    SqlCommand cmd = new SqlCommand(query, connection);
                                    cmd.ExecuteNonQuery();
                                    connection.Close();
                                }
                            }
                        }
                        #endregion

                        #region Equipment - Register - Link Starts
                        if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.Register != null)
                        {
                            foreach (var utilsDvceRegister in utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.Register)
                            {
                                string regLinkFields = "";
                                string regLinkValues = "";
                                string regLinkUpdateValues = "";

                                var logRegNo = utilsDvceRegister.UtilitiesMeasurementTaskID.Value;

                                equipmentId = equipment.GetEquipmentIdByUtilityId(utilityId);
                                EquipmentRegLink equipmentRegLink = equipment_Reg_Link.GetEquipmentRegLinkByLogRegNoAndEquipmentId(logRegNo, equipmentId);

                                if (equipmentId > 0)
                                {
                                    regLinkFields += "Equipment_Id,";
                                    regLinkValues += equipmentId + ",";
                                    regLinkUpdateValues += "Equipment_Id ='" + equipmentId + "',";
                                }
                                if (utilsDvceRegister.StartDate != DateTime.MinValue)
                                {
                                    regLinkFields += "ValidFrom,";
                                    regLinkValues += "'" + utilsDvceRegister.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                    regLinkUpdateValues += "ValidFrom ='" + utilsDvceRegister.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                }
                                if (utilsDvceRegister.EndDate != DateTime.MinValue)
                                {
                                    regLinkFields += "ValidTo,";
                                    regLinkValues += "'" + utilsDvceRegister.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                    regLinkUpdateValues += "ValidTo ='" + utilsDvceRegister.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                }
                                if (utilsDvceRegister.UtilitiesMeasurementTaskID != null && utilsDvceRegister.UtilitiesMeasurementTaskID.Value != null)
                                {
                                    regLinkFields += "Log_Reg_no,";
                                    regLinkValues += "'" + utilsDvceRegister.UtilitiesMeasurementTaskID.Value + "',";
                                    regLinkUpdateValues += "Log_Reg_no ='" + utilsDvceRegister.UtilitiesMeasurementTaskID.Value + "',";
                                }
                                if (!String.IsNullOrEmpty(utilsDvceRegister.UtilitiesObjectIdentificationSystemCodeText))
                                {
                                    regLinkFields += "UOBIS,";
                                    regLinkValues += "'" + utilsDvceRegister.UtilitiesObjectIdentificationSystemCodeText + "',";
                                    regLinkUpdateValues += "UOBIS ='" + utilsDvceRegister.UtilitiesObjectIdentificationSystemCodeText + "',";
                                }
                                if (!String.IsNullOrEmpty(utilsDvceRegister.UtilitiesMeasurementTaskCategoryCode))
                                {
                                    regLinkFields += "Cat_Code,";
                                    regLinkValues += "'" + utilsDvceRegister.UtilitiesMeasurementTaskCategoryCode + "',";
                                    regLinkUpdateValues += "Cat_Code ='" + utilsDvceRegister.UtilitiesMeasurementTaskCategoryCode + "',";
                                }
                                if (!String.IsNullOrEmpty(utilsDvceRegister.UtilitiesDivisionCategoryCode))
                                {
                                    regLinkFields += "Div_code,";
                                    regLinkValues += "'" + utilsDvceRegister.UtilitiesDivisionCategoryCode + "',";
                                    regLinkUpdateValues += "Div_code ='" + utilsDvceRegister.UtilitiesDivisionCategoryCode + "',";
                                }
                                if (!String.IsNullOrEmpty(utilsDvceRegister.TimeZoneCode))
                                {
                                    regLinkFields += "TimeZone,";
                                    regLinkValues += "'" + utilsDvceRegister.TimeZoneCode + "',";
                                    regLinkUpdateValues += "TimeZone ='" + utilsDvceRegister.TimeZoneCode + "',";
                                }
                                foreach (var regSpecification in utilsDvceRegister.Specifications)
                                {
                                    if (!String.IsNullOrEmpty(regSpecification.MeasureUnitCode))
                                    {
                                        regLinkFields += "MeasCode,";
                                        regLinkValues += "'" + regSpecification.MeasureUnitCode + "',";
                                        regLinkUpdateValues += "MeasCode ='" + regSpecification.MeasureUnitCode + "',";
                                    }
                                    if (regSpecification.DecimalValuePrecision.TotalDigitNumberValue > 0)
                                    {
                                        regLinkFields += "No_Digits,";
                                        regLinkValues += regSpecification.DecimalValuePrecision.TotalDigitNumberValue + ",";
                                        regLinkUpdateValues += "No_Digits ='" + regSpecification.DecimalValuePrecision.TotalDigitNumberValue + "',";
                                    }
                                    if (regSpecification.DecimalValuePrecision.FractionDigitNumberValue > 0)
                                    {
                                        regLinkFields += " Dec_Digits,";
                                        regLinkValues += regSpecification.DecimalValuePrecision.FractionDigitNumberValue + ",";
                                        regLinkUpdateValues += "Dec_Digits ='" + regSpecification.DecimalValuePrecision.FractionDigitNumberValue + "',";
                                    }
                                    if (regSpecification.MeterReadingResultAdjustmentFactorValue > 0)
                                    {
                                        regLinkFields += "Factor,";
                                        regLinkValues += regSpecification.MeterReadingResultAdjustmentFactorValue + ",";
                                        regLinkUpdateValues += "Factor =" + regSpecification.MeterReadingResultAdjustmentFactorValue + ",";
                                    }
                                }
                                if (!String.IsNullOrEmpty(createdBy))
                                {
                                    regLinkFields += "Created_by,";
                                    regLinkValues += "'" + createdBy + "',";
                                    regLinkUpdateValues += "Changed_by = '" + createdBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                    dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(createdBy);
                                    if (dataOriginID > 0)
                                    {
                                        regLinkFields += "Data_Origin_Code,";
                                        regLinkValues += "'" + dataOriginID + "',";
                                        regLinkUpdateValues += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                                    }
                                }
                                regLinkFields += "Created_on,";
                                regLinkValues += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                regLinkFields = regLinkFields.TrimEnd(',');
                                regLinkValues = regLinkValues.TrimEnd(',');
                                regLinkUpdateValues = regLinkUpdateValues.TrimEnd(',');

                                if (equipmentRegLink.EquipmentId > 0 && !String.IsNullOrEmpty(equipmentRegLink.LogRegNo))
                                {
                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        connection.Open();
                                        query = "update Equipment_Reg_Link set " + regLinkUpdateValues + " where Log_Reg_no='" + equipmentRegLink.LogRegNo + "' and Equipment_Id=" + equipmentRegLink.EquipmentId;
                                        SqlCommand cmd = new SqlCommand(query, connection);
                                        cmd.ExecuteNonQuery();
                                        connection.Close();
                                    }
                                }
                                else
                                {
                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        connection.Open();
                                        query = "insert into Equipment_Reg_Link (" + regLinkFields + ") values(" + regLinkValues + ")";
                                        SqlCommand cmd = new SqlCommand(query, connection);
                                        cmd.ExecuteNonQuery();
                                        connection.Close();
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Device Location add update
                        foreach (var location in utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.Location)
                        {
                            string devLocationFields = "";
                            string devLocationValues = "";
                            string devLocationUpdateValues = "";
                            if (location.InstallationPointID != null && location.InstallationPointID.Value != null)
                            {
                                devLocUtilityId = location.InstallationPointID.Value;
                                devLocationFields += "Utility_ID,";
                                devLocationValues += devLocUtilityId + ",";
                            }
                            if (!String.IsNullOrEmpty(createdBy))
                            {
                                devLocationFields += "Created_by,";
                                devLocationValues += "'" + createdBy + "',";
                                devLocationUpdateValues += "changed_by = '" + createdBy + "', changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(createdBy);
                                if (dataOriginID > 0)
                                {
                                    devLocationFields += "Data_Origin_Code,";
                                    devLocationValues += "'" + dataOriginID + "',";
                                    devLocationUpdateValues += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                                }
                            }
                            devLocationFields += "Created_on,";
                            devLocationValues += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                            devLocationFields = devLocationFields.TrimEnd(',');
                            devLocationValues = devLocationValues.TrimEnd(',');
                            devLocationUpdateValues = devLocationUpdateValues.TrimEnd(',');

                            deviceLocId = deviceLocation.GetDeviceLocIdByUtilityId(devLocUtilityId);
                            if (deviceLocId > 0)
                            {
                                using (SqlConnection connection = new SqlConnection(constr))
                                {
                                    connection.Open();
                                    query = "update Device_Location set " + devLocationUpdateValues + " where Utility_ID = '" + devLocUtilityId + "'";
                                    SqlCommand cmd = new SqlCommand(query, connection);
                                    cmd.ExecuteNonQuery();
                                    connection.Close();
                                }
                            }
                            else
                            {
                                using (SqlConnection connection = new SqlConnection(constr))
                                {
                                    connection.Open();
                                    query = "insert into Device_Location (" + devLocationFields + ") values(" + devLocationValues + ")";
                                    SqlCommand cmd = new SqlCommand(query, connection);
                                    cmd.ExecuteNonQuery();
                                    connection.Close();
                                }
                            }

                            #region DevLoc Device Link
                            string devLocDevLinkFields = "";
                            string devLocDevLinkValues = "";
                            string devLocDevLinkUpdateValues = "";
                            if (equipmentId > 0)
                            {
                                if (location.StartDate != null)
                                {
                                    devLocDevLinkFields += "ValidFromDate,";
                                    devLocDevLinkValues += "'" + location.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                    devLocDevLinkUpdateValues += "ValidFromDate =" + "'" + location.StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                }
                                if (location.EndDate != null)
                                {
                                    devLocDevLinkFields += "ValidToDate,";
                                    devLocDevLinkValues += "'" + location.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                    devLocDevLinkUpdateValues += "ValidToDate =" + "'" + location.EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                }
                                if (location.InstallationPointID != null && location.InstallationPointID.Value != null)
                                {
                                    deviceLocId = deviceLocation.GetDeviceLocIdByUtilityId(devLocUtilityId);
                                    if (deviceLocId > 0)
                                    {
                                        devLocDevLinkFields += "DevicelocationId,";
                                        devLocDevLinkValues += deviceLocId + ",";
                                    }
                                }
                                if (equipmentId > 0)
                                {
                                    devLocDevLinkFields += "EquipmentId,";
                                    devLocDevLinkValues += equipmentId + ",";
                                    devLocDevLinkUpdateValues += "EquipmentId =" + "'" + equipmentId + "',";

                                }
                                if (createdBy != null)
                                {
                                    devLocDevLinkFields += "Created_by,";
                                    devLocDevLinkValues += "'" + createdBy + "',";
                                    devLocDevLinkUpdateValues += "Changed_by = '" + createdBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                    dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(createdBy);
                                    if (dataOriginID > 0)
                                    {
                                        devLocDevLinkFields += "Data_Origin_Code,";
                                        devLocDevLinkValues += "'" + dataOriginID + "',";
                                        devLocDevLinkUpdateValues += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                                    }
                                }
                                devLocDevLinkFields += "Created_on,";
                                devLocDevLinkValues += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                //Remove end Comma 
                                devLocDevLinkFields = devLocDevLinkFields.TrimEnd(',');
                                devLocDevLinkValues = devLocDevLinkValues.TrimEnd(',');
                                devLocDevLinkUpdateValues = devLocDevLinkUpdateValues.TrimEnd(',');

                                int devLocDeviceLinkId = devLocDeviceLink.GetDeviceLocDevLinkIdByParam(equipmentId, deviceLocId);
                                if (deviceLocId > 0 && devLocDeviceLinkId > 0)
                                {
                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        connection.Open();
                                        query = "update DevLoc_Device_Link set " + devLocDevLinkUpdateValues + " where ID = '" + devLocDeviceLinkId + "'";
                                        SqlCommand cmd = new SqlCommand(query, connection);
                                        cmd.ExecuteNonQuery();
                                        connection.Close();
                                    }
                                }
                                else
                                {
                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        connection.Open();
                                        query = "insert into DevLoc_Device_Link (" + devLocDevLinkFields + ") values(" + devLocDevLinkValues + ")";
                                        SqlCommand cmd = new SqlCommand(query, connection);
                                        cmd.ExecuteNonQuery();
                                        connection.Close();
                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion

                        #region Device_Location_Info table Starts 

                        int i = 0;
                        foreach (var location in utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.Location)
                        {
                            string deviceLocInfoFields = "";
                            string deviceLocInfoValues = "";
                            string updateDevInfoValues = "";

                            if (location.InstallationPointAddressInformation != null)
                            {
                                if (!String.IsNullOrEmpty(location.InstallationPointAddressInformation.HouseID))
                                {
                                    deviceLocInfoFields += "HouseID,";
                                    deviceLocInfoValues += "'" + location.InstallationPointAddressInformation.HouseID + "',";
                                    updateDevInfoValues += "HouseID =" + "'" + location.InstallationPointAddressInformation.HouseID + "',";
                                }
                                if (!String.IsNullOrEmpty(location.InstallationPointAddressInformation.StreetPostalCode))
                                {
                                    deviceLocInfoFields += "StreetPostalCode,";
                                    deviceLocInfoValues += "'" + location.InstallationPointAddressInformation.StreetPostalCode + "',";
                                    updateDevInfoValues += "StreetPostalCode =" + "'" + location.InstallationPointAddressInformation.StreetPostalCode + "',";
                                }
                                if (!String.IsNullOrEmpty(location.InstallationPointAddressInformation.CityName))
                                {
                                    deviceLocInfoFields += "CityName,";
                                    deviceLocInfoValues += "'" + location.InstallationPointAddressInformation.CityName + "',";
                                    updateDevInfoValues += "CityName =" + "'" + location.InstallationPointAddressInformation.CityName + "',";
                                }
                                if (!String.IsNullOrEmpty(location.InstallationPointAddressInformation.StreetName))
                                {
                                    deviceLocInfoFields += "StreetName,";
                                    deviceLocInfoValues += "'" + location.InstallationPointAddressInformation.StreetName + "',";
                                    updateDevInfoValues += "StreetName =" + "'" + location.InstallationPointAddressInformation.StreetName + "',";
                                }
                                if (!String.IsNullOrEmpty(location.InstallationPointAddressInformation.CountryCode))
                                {
                                    deviceLocInfoFields += "CountryCode,";
                                    deviceLocInfoValues += "'" + location.InstallationPointAddressInformation.CountryCode + "',";
                                    updateDevInfoValues += "CountryCode =" + "'" + location.InstallationPointAddressInformation.CountryCode + "',";
                                }
                                if (!String.IsNullOrEmpty(location.InstallationPointAddressInformation.TimeZoneCode))
                                {
                                    deviceLocInfoFields += "TimeZoneCode,";
                                    deviceLocInfoValues += "'" + location.InstallationPointAddressInformation.TimeZoneCode + "',";
                                    updateDevInfoValues += "TimeZoneCode =" + "'" + location.InstallationPointAddressInformation.TimeZoneCode + "',";
                                }
                                if (location.ModificationInformation.InstallationDate != null &&
                                location.ModificationInformation.InstallationDate != DateTime.MinValue)
                                {
                                    deviceLocInfoFields += "InstallationDate,";
                                    deviceLocInfoValues += "'" + location.ModificationInformation.InstallationDate.ToString("yyyy-MM-dd") + "',";
                                    updateDevInfoValues += "InstallationDate =" + "'" + location.ModificationInformation.InstallationDate.ToString("yyyy-MM-dd") + "',";
                                }
                                if (location.InstallationPointHierarchyRelationship.ParentInstallationPointID != null
                                    && location.InstallationPointHierarchyRelationship.ParentInstallationPointID.Value != null)
                                {
                                    deviceLocInfoFields += "ParentInstallationPointID,";
                                    deviceLocInfoValues += "'" + location.InstallationPointHierarchyRelationship.ParentInstallationPointID.Value + "',";
                                    updateDevInfoValues += "ParentInstallationPointID =" + "'" + location.InstallationPointHierarchyRelationship.ParentInstallationPointID.Value + "',";
                                }
                                if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation != null)
                                {
                                    if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].StartDate != DateTime.MinValue)
                                    {
                                        deviceLocInfoFields += "ValidFrom,";
                                        deviceLocInfoValues += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                        updateDevInfoValues += "ValidFrom = '" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].StartDate.ToString("yyyy-MM-dd") + "',";
                                    }
                                    if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].EndDate != DateTime.MinValue)
                                    {
                                        deviceLocInfoFields += "ValidTo,";
                                        deviceLocInfoValues += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                        updateDevInfoValues += "ValidTo = '" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].EndDate.ToString("yyyy-MM-dd") + "',";
                                    }
                                    if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].LogicalInstallationPointID != null
                                        && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].LogicalInstallationPointID.Value != null)
                                    {
                                        deviceLocInfoFields += "LogicalInstallationPointID,";
                                        deviceLocInfoValues += "'" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].LogicalInstallationPointID.Value + "',";
                                        updateDevInfoValues += "LogicalInstallationPointID ='" + utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.LogicalLocation[i].LogicalInstallationPointID.Value + "',";
                                    }

                                    i++;
                                }
                                if (deviceLocId > 0)
                                {
                                    deviceLocInfoFields += "Device_Location_ID,";
                                    deviceLocInfoValues += deviceLocId + ",";
                                }
                                if (!String.IsNullOrEmpty(createdBy))
                                {
                                    deviceLocInfoFields += "Created_by,";
                                    deviceLocInfoValues += "'" + createdBy + "',";
                                    updateDevInfoValues += "Changed_by = '" + createdBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                    dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(createdBy);
                                    if (dataOriginID > 0)
                                    {
                                        deviceLocInfoFields += "Data_Origin_Code,";
                                        deviceLocInfoValues += "'" + dataOriginID + "',";
                                        updateDevInfoValues += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                                    }
                                }
                                deviceLocInfoFields += "Created_on,";
                                deviceLocInfoValues += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                deviceLocInfoFields = deviceLocInfoFields.TrimEnd(',');
                                deviceLocInfoValues = deviceLocInfoValues.TrimEnd(',');
                                updateDevInfoValues = updateDevInfoValues.TrimEnd(',');


                                using (SqlConnection connection = new SqlConnection(constr))
                                {
                                    connection.Open();
                                    string deviceLocationInfoQuery = "insert into Device_Location_Info (" + deviceLocInfoFields + ") values(" + deviceLocInfoValues + ")";
                                    SqlCommand cmdDeviceLocationInfo = new SqlCommand(deviceLocationInfoQuery, connection);
                                    cmdDeviceLocationInfo.ExecuteNonQuery();
                                    connection.Close();
                                }

                            }
                        }

                        #endregion

                        #region Smart Meter
                        if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SmartMeter != null)
                        {
                            if (utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID != null
                                    && utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value != null)
                            {
                                //Get HES Code
                                int hesCode = hes.GetHESIdByHESCode(utilsDvceERPSmrtMtrCrteReqMsg.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value);

                                if (hesCode > 0)
                                {
                                    try
                                    {
                                        using (SqlConnection connection = new SqlConnection(constr))
                                        {
                                            connection.Open();
                                            query = "update equipment set HES_ID =" + hesCode + ", changed_by = '" + createdBy + "', changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where ID = " + equipmentId + "";
                                            SqlCommand cmd = new SqlCommand(query, connection);
                                            cmd.ExecuteNonQuery();
                                            connection.Close();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
                if (!isRejected)
                {
                    //Write to File
                    var xmlString = Serialize<UtilitiesDeviceERPSmartMeterReplicationBulkRequest_Out.UtilsDvceERPSmrtMtrRplctnBulkReqMsg>(UtilitiesDeviceERPSmartMeterReplicationBulkRequest);
                    WriteToLog(xmlString, uuid, Constants.CisAdaptorLog_Path);
                }
                else
                {

                }
                Confirmation_In();
                //Exception_CIS_Sync
                exceptionSync.UUID = parentUUID;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = createdBy;
                exceptionSync.ServiceName = Constants.ReplicationRequest_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :ReplicationRequest --- UUID :" + parentUUID + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                errorMessage += "Exception :" + ex.Message;
                Confirmation_In();
                var xmlString = Serialize<UtilitiesDeviceERPSmartMeterReplicationBulkRequest_Out.UtilsDvceERPSmrtMtrRplctnBulkReqMsg>(UtilitiesDeviceERPSmartMeterReplicationBulkRequest);
                WriteToLog(xmlString, Constants.ReplicationRequest_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        //[SoapDocumentMethod(OneWay = true)]
        //[WebMethod]
        //public void UtilitiesDeviceERPSmartMeterRegisterBulkCreateRequest_Out(UtilitiesDeviceERPSmartMeterRegisterBulkCreateRequest_Out.UtilsDvceERPSmrtMtrRegBulkCrteReqMsg UtilitiesDeviceERPSmartMeterRegisterBulkCreateRequest)
        //{



        //}

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void DeviceLocation_Notification(UtilitiesDeviceERPSmartMeterLocationNotification_Out.UtilsDvceERPSmrtMtrLocNotifMsg UtilitiesDeviceERPSmartMeterLocationNotification)
        {
            string query = "";
            string errorMessage = "";
            string uuid = "";
            string CreatedBy = "";
            bool isRejected = false;
            //bool isInserted = false;
            int deviceLocId = 0;
            int equipmentId = 0;
            int dataOriginID = 0;
            DataSourceMaster dataSourceMaster = new DataSourceMaster();
            DevLocDeviceLink devLocDeviceLink = new DevLocDeviceLink();
            Equipment equipment = new Equipment();
            DeviceLocation deviceLocation = new DeviceLocation();
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                //CIS Log entry
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (UtilitiesDeviceERPSmartMeterLocationNotification.MessageHeader != null)
                {
                    if (UtilitiesDeviceERPSmartMeterLocationNotification.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = UtilitiesDeviceERPSmartMeterLocationNotification.MessageHeader.UUID.Value;
                        uuid = UtilitiesDeviceERPSmartMeterLocationNotification.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }

                    if (UtilitiesDeviceERPSmartMeterLocationNotification.MessageHeader.SenderBusinessSystemID != null)
                    {
                        CreatedBy = UtilitiesDeviceERPSmartMeterLocationNotification.MessageHeader.SenderBusinessSystemID;
                    }
                    cISAdaptorParent.LogDateTime = UtilitiesDeviceERPSmartMeterLocationNotification.MessageHeader.CreationDateTime;
                }
                else
                {
                    uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Request_Type = "DeviceLocation_Notification";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);

                ExceptionCISSync exceptionSync = new ExceptionCISSync();

                //Validations
                if (UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice != null)
                {
                    if (UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.ID != null
                        && UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.ID.Value != null)
                    {
                        var UtilityId = UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.ID.Value;
                        equipmentId = equipment.GetEquipmentIdByUtilityId(UtilityId);

                        if (equipmentId > 0)
                        {
                            //DevLoc_Device_Link
                            if (UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.Location != null)
                            {
                                int i = 0;
                                List<Location> locationList = new List<Location>();

                                foreach (var UtilitiesDeviceERPSmartMeterLocation in UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.Location)
                                {
                                    //bool isInserted = false;
                                    string fields = "";
                                    string values = "";
                                    var devLocUtilId = "";
                                    string streetName = "";
                                    if (!String.IsNullOrEmpty(UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.StreetName))
                                    {
                                        streetName = UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.StreetName;
                                    }

                                    string parentInstallationPointID = "";
                                    if (UtilitiesDeviceERPSmartMeterLocation.InstallationPointHierarchyRelationship.ParentInstallationPointID != null &&
                                        UtilitiesDeviceERPSmartMeterLocation.InstallationPointHierarchyRelationship.ParentInstallationPointID.Value != null)
                                    {
                                        parentInstallationPointID = UtilitiesDeviceERPSmartMeterLocation.InstallationPointHierarchyRelationship.ParentInstallationPointID.Value;
                                    }

                                    if (UtilitiesDeviceERPSmartMeterLocation != null)
                                    {
                                        //Insert into Device_Location
                                        if (UtilitiesDeviceERPSmartMeterLocation.InstallationPointID != null && UtilitiesDeviceERPSmartMeterLocation.InstallationPointID.Value != null)
                                        {
                                            devLocUtilId = UtilitiesDeviceERPSmartMeterLocation.InstallationPointID.Value;
                                            deviceLocId = deviceLocation.GetDeviceLocIdByUtilityId(devLocUtilId);
                                            if (CreatedBy != null)
                                            {
                                                dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(CreatedBy);
                                            }

                                            if (deviceLocId > 0)
                                            {
                                                using (SqlConnection connection = new SqlConnection(constr))
                                                {
                                                    connection.Open();
                                                    query = "update Device_Location set changed_by = '" + CreatedBy + "', Data_Origin_Code='" + dataOriginID + "', changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "', STATUS_CD='A', Utility_Office_ID = '" + streetName + "' where Utility_ID = '" + devLocUtilId + "'";
                                                    SqlCommand cmd = new SqlCommand(query, connection);
                                                    cmd.ExecuteNonQuery();
                                                    connection.Close();
                                                }
                                            }
                                            else
                                            {
                                                using (SqlConnection connection = new SqlConnection(constr))
                                                {
                                                    connection.Open();
                                                    query = "insert into Device_Location (Utility_ID, Created_by, Created_on, STATUS_CD, Data_Origin_Code, Utility_office_ID) values('" + devLocUtilId + "', '" + CreatedBy + "', '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "','A','" + dataOriginID + "', '" + streetName + "')";
                                                    SqlCommand cmd = new SqlCommand(query, connection);
                                                    cmd.ExecuteNonQuery();
                                                    connection.Close();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            isRejected = true;
                                            errorMessage += "InstallationPointID Not Found;";
                                        }

                                        string ValidFromDate = "";
                                        string ValidToDate = "";
                                        DateTime validFromDate = DateTime.MinValue;
                                        //DevLoc_Device_Link entry
                                        if (UtilitiesDeviceERPSmartMeterLocation.StartDate != null)
                                        {
                                            ValidFromDate = UtilitiesDeviceERPSmartMeterLocation.StartDate.ToString("MM/dd/yyyy HH:mm:ss");
                                            validFromDate = UtilitiesDeviceERPSmartMeterLocation.StartDate;
                                            fields += "ValidFromDate,";
                                            values += "'" + ValidFromDate + "',";
                                        }
                                        else
                                        {
                                            isRejected = true;
                                            errorMessage += "StartDate Not Found in Payload list: " + (i + 1) + ";";
                                        }
                                        if (UtilitiesDeviceERPSmartMeterLocation.EndDate != null)
                                        {
                                            ValidToDate = UtilitiesDeviceERPSmartMeterLocation.EndDate.ToString("MM/dd/yyyy HH:mm:ss");
                                            fields += "ValidToDate,";
                                            values += "'" + ValidToDate + "',";
                                        }
                                        else
                                        {
                                            isRejected = true;
                                            errorMessage += "EndDate Not Found in Payload list:" + (i + 1) + ";";
                                        }
                                        if (UtilitiesDeviceERPSmartMeterLocation.InstallationPointID != null && UtilitiesDeviceERPSmartMeterLocation.InstallationPointID.Value != null)
                                        {
                                            deviceLocId = deviceLocation.GetDeviceLocIdByUtilityId(devLocUtilId);
                                            if (deviceLocId > 0)
                                            {
                                                fields += "DevicelocationId,";
                                                values += deviceLocId + ",";
                                            }
                                            else
                                            {
                                                isRejected = true;
                                                errorMessage += "InstallationPointID Not Found in Payload";
                                            }
                                        }
                                        if (equipmentId > 0)
                                        {
                                            fields += "EquipmentId,";
                                            values += equipmentId + ",";
                                        }
                                        if (CreatedBy != null)
                                        {
                                            fields += "Created_by,";
                                            values += "'" + CreatedBy + "',";

                                            dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(CreatedBy);
                                            if (dataOriginID > 0)
                                            {
                                                fields += "Data_Origin_Code,";
                                                values += "'" + dataOriginID + "',";
                                            }
                                        }
                                        fields += "Created_on,";
                                        values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                        //Remove end Comma 
                                        fields = fields.TrimEnd(',');
                                        values = values.TrimEnd(',');

                                        //DevLocLinkModel devLocLinkModel = new DevLocLinkModel();
                                        bool updateDevLocLink = false;
                                        var devLocLinkList = devLocDeviceLink.CheckValidFromDate(ValidFromDate, equipmentId);
                                        if (devLocLinkList.Count > 0)
                                        {
                                            if (deviceLocId != devLocLinkList[0].DevicelocationId)
                                            {
                                                isRejected = true;
                                                errorMessage += "DeviceLocationId not consistent row: " + (i + 1) + ",";
                                            }
                                            if (devLocLinkList[0].ValidFromDate.ToString("yyyy-MM-dd") == validFromDate.ToString("yyyy-MM-dd"))
                                            {
                                                updateDevLocLink = true;
                                            }
                                            else
                                            {
                                                isRejected = true;
                                                errorMessage += "Inconsistent values in DevLoc_Device_Link: " + (i + 1) + ",";
                                            }
                                        }

                                        var devLocLinkList2 = devLocDeviceLink.CheckValidToDate(ValidToDate, equipmentId);
                                        if (devLocLinkList2.Count > 0)
                                        {
                                            if (devLocLinkList2[0].ValidFromDate > validFromDate)
                                            {
                                                isRejected = true;
                                                errorMessage += "Inconsistent values in DevLoc_Device_Link: " + (i + 1) + ",";
                                            }
                                        }

                                        //int sameTimeSlice = devLocDeviceLink.CheckEquipmentIdAndDeviceLocId(ValidFromDate, ValidToDate);
                                        //if (sameTimeSlice != 0)
                                        //{
                                        //    isRejected = true;
                                        //    errorMessage += "Same time slice row:" + (i + 1) + ",";
                                        //}

                                        if (!isRejected)
                                        {
                                            if (updateDevLocLink)
                                            {
                                                using (SqlConnection connection = new SqlConnection(constr))
                                                {
                                                    connection.Open();
                                                    query = "Update DevLoc_Device_Link set(" + fields + ") values(" + values + ")";
                                                    query = "Update DevLoc_Device_Link Set EquipmentId=" + equipmentId + ", DeviceLocationId=" + deviceLocId
                                                        + ", ValidFromDate='" + ValidFromDate + "', ValidToDate='" + ValidToDate + "', Changed_by='" + CreatedBy
                                                        + "', Changed_on='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' Where ID=" + devLocLinkList[0].ID;
                                                    SqlCommand cmd = new SqlCommand(query, connection);
                                                    cmd.ExecuteNonQuery();
                                                    connection.Close();
                                                    //isInserted = true;
                                                }
                                            }
                                            else
                                            {
                                                using (SqlConnection connection = new SqlConnection(constr))
                                                {
                                                    try
                                                    {
                                                        connection.Open();
                                                        query = "insert into DevLoc_Device_Link (" + fields + ") values(" + values + ")";
                                                        SqlCommand cmd = new SqlCommand(query, connection);
                                                        cmd.ExecuteNonQuery();
                                                        connection.Close();
                                                        //isInserted = true;
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        //deviceLocation.DeleteDeviceLocationById(deviceLocId);
                                                    }
                                                }
                                            }
                                        }
                                        //if (!isInserted)
                                        //{
                                        //    deviceLocation.DeleteDeviceLocationById(deviceLocId);
                                        //}

                                        //Check Config_value 
                                        ConfigMasterData configMasterData = new ConfigMasterData();
                                        string configValue = configMasterData.GetConfigValueByMDObjectAndConfigName();
                                        string consumerUtilityID = "";
                                        consumerUtilityID = configValue == "Y" ? devLocUtilId : parentInstallationPointID;

                                        //Consumer Entry
                                        Consumer consumer = new Consumer();
                                        var consumerID = consumer.GetConsumerByConsumerID(consumerUtilityID);
                                        if (consumerID > 0)
                                        {
                                            using (SqlConnection connection = new SqlConnection(constr))
                                            {
                                                connection.Open();
                                                query = "Update Consumer Set Changed_by = '" + CreatedBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where Utility_ID = '" + consumerUtilityID + "'";
                                                SqlCommand cmd = new SqlCommand(query, connection);
                                                cmd.ExecuteNonQuery();
                                                connection.Close();
                                            }
                                        }
                                        else
                                        {
                                            using (SqlConnection connection = new SqlConnection(constr))
                                            {
                                                connection.Open();
                                                query = "insert into Consumer (Utility_ID, Created_by, Created_on) values(" + consumerUtilityID + ",'" + CreatedBy + "','" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "')";
                                                SqlCommand cmd = new SqlCommand(query, connection);
                                                cmd.ExecuteNonQuery();
                                                connection.Close();
                                            }
                                        }

                                        //ConsumerDevLocLink Entry
                                        consumerID = consumer.GetConsumerByConsumerID(consumerUtilityID);
                                        ConsumerDevlocLink consumerDevlocLink = new ConsumerDevlocLink();
                                        var consumerDevlocID = consumerDevlocLink.GetConsumerDevLocIDByParam(deviceLocId, consumerID);
                                        if (consumerDevlocID > 0)
                                        {
                                            using (SqlConnection connection = new SqlConnection(constr))
                                            {
                                                connection.Open();
                                                query = "Update ConsumerDevLocLink Set ValidFromDate='" + ValidFromDate + "', ValidToDate='" + ValidToDate
                                                    + "', Changed_by='" + CreatedBy + "', Changed_on='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")
                                                    + "' where ConsumerID='" + consumerID + "' and DeviceLocationId = '" + deviceLocId + "'";
                                                SqlCommand cmd = new SqlCommand(query, connection);
                                                cmd.ExecuteNonQuery();
                                                connection.Close();
                                            }
                                        }
                                        else
                                        {
                                            using (SqlConnection connection = new SqlConnection(constr))
                                            {
                                                connection.Open();
                                                query = "insert into ConsumerDevLocLink (ConsumerID, DeviceLocationId, ValidFromDate, ValidToDate, Created_by, Created_on) values("
                                                    + consumerID + "," + deviceLocId + ",'" + ValidFromDate + "','" + ValidToDate + "','" + CreatedBy + "','" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "')";
                                                SqlCommand cmd = new SqlCommand(query, connection);
                                                cmd.ExecuteNonQuery();
                                                connection.Close();
                                            }
                                        }
                                    }

                                    //Device_Location_Info
                                    var deviceLocInfoFields = "";
                                    var deviceLocInfoValues = "";
                                    var updateDeviceLocationValues = "";
                                    if (UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation != null)
                                    {
                                        if (!String.IsNullOrEmpty(UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.HouseID))
                                        {
                                            deviceLocInfoFields += "HouseID,";
                                            deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.HouseID + "',";
                                            updateDeviceLocationValues += "HouseID =" + "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.HouseID + "',";

                                        }
                                        if (!String.IsNullOrEmpty(UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.StreetPostalCode))
                                        {
                                            deviceLocInfoFields += "StreetPostalCode,";
                                            deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.StreetPostalCode + "',";
                                            updateDeviceLocationValues += "StreetPostalCode =" + "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.StreetPostalCode + "',";
                                        }
                                        if (!String.IsNullOrEmpty(UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.CityName))
                                        {
                                            deviceLocInfoFields += "CityName,";
                                            deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.CityName + "',";
                                            updateDeviceLocationValues += "CityName =" + "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.CityName + "',";
                                        }
                                        if (!String.IsNullOrEmpty(UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.StreetName))
                                        {
                                            deviceLocInfoFields += "StreetName,";
                                            deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.StreetName + "',";
                                            updateDeviceLocationValues += "StreetName =" + "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.StreetName + "',";
                                        }
                                        if (!String.IsNullOrEmpty(UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.CountryCode))
                                        {
                                            deviceLocInfoFields += "CountryCode,";
                                            deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.CountryCode + "',";
                                            updateDeviceLocationValues += "CountryCode =" + "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.CountryCode + "',";
                                        }
                                        if (!String.IsNullOrEmpty(UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.TimeZoneCode))
                                        {
                                            deviceLocInfoFields += "TimeZoneCode,";
                                            deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.TimeZoneCode + "',";
                                            updateDeviceLocationValues += "TimeZoneCode =" + "'" + UtilitiesDeviceERPSmartMeterLocation.InstallationPointAddressInformation.TimeZoneCode + "',";
                                        }
                                        //InstallationPointHierarchyRelationship
                                        if (UtilitiesDeviceERPSmartMeterLocation.InstallationPointHierarchyRelationship.ParentInstallationPointID != null
                                            && UtilitiesDeviceERPSmartMeterLocation.InstallationPointHierarchyRelationship.ParentInstallationPointID.Value != null)
                                        {
                                            parentInstallationPointID = UtilitiesDeviceERPSmartMeterLocation.InstallationPointHierarchyRelationship.ParentInstallationPointID.Value;
                                            deviceLocInfoFields += "ParentInstallationPointID,";
                                            deviceLocInfoValues += "'" + parentInstallationPointID + "',";
                                            updateDeviceLocationValues += "ParentInstallationPointID =" + "'" + parentInstallationPointID + "',";
                                        }
                                        if (UtilitiesDeviceERPSmartMeterLocation.ModificationInformation.InstallationDate != null &&
                                            UtilitiesDeviceERPSmartMeterLocation.ModificationInformation.InstallationDate != DateTime.MinValue)
                                        {
                                            deviceLocInfoFields += "InstallationDate,";
                                            deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocation.ModificationInformation.InstallationDate.ToString("yyyy-MM-dd") + "',";
                                            updateDeviceLocationValues += "InstallationDate =" + "'" + UtilitiesDeviceERPSmartMeterLocation.ModificationInformation.InstallationDate.ToString("yyyy-MM-dd") + "',";
                                        }
                                        else
                                        {
                                            errorMessage += "InstallationDate Not Found;";
                                            isRejected = true;
                                        }
                                        if (UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation != null)
                                        {
                                            if (UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].StartDate != DateTime.MinValue)
                                            {
                                                deviceLocInfoFields += "ValidFrom,";
                                                deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                                updateDeviceLocationValues += "ValidFrom =" + "'" + UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].StartDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                            }
                                            if (UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].EndDate != DateTime.MinValue)
                                            {
                                                deviceLocInfoFields += "ValidTo,";
                                                deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                                updateDeviceLocationValues += "ValidTo =" + "'" + UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].EndDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                            }
                                            if (UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].LogicalInstallationPointID != null
                                                && UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].LogicalInstallationPointID.Value != null)
                                            {
                                                deviceLocInfoFields += "LogicalInstallationPointID,";
                                                deviceLocInfoValues += "'" + UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].LogicalInstallationPointID.Value + "',";
                                                updateDeviceLocationValues += "LogicalInstallationPointID =" + "'" + UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.LogicalLocation[i].LogicalInstallationPointID.Value + "',";
                                            }
                                            i++;
                                        }
                                        if (deviceLocId > 0)
                                        {
                                            deviceLocInfoFields += "Device_Location_ID,";
                                            deviceLocInfoValues += deviceLocId + ",";
                                            updateDeviceLocationValues += "Device_Location_ID =" + "'" + deviceLocId + "',";
                                        }
                                        if (CreatedBy != null)
                                        {
                                            deviceLocInfoFields += "Created_by,";
                                            deviceLocInfoValues += "'" + CreatedBy + "',";
                                            updateDeviceLocationValues += "Changed_by =" + "'" + CreatedBy + "',";

                                            dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(CreatedBy);
                                            if (dataOriginID > 0)
                                            {
                                                fields += "Data_Origin_Code,";
                                                values += "'" + dataOriginID + "',";
                                                updateDeviceLocationValues += "Data_Origin_Code =" + "'" + dataOriginID + "',";
                                            }
                                        }
                                        deviceLocInfoFields += "Created_on,";
                                        deviceLocInfoValues += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                        updateDeviceLocationValues += "Changed_on =" + "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                        //Remove end Comma 
                                        deviceLocInfoFields = deviceLocInfoFields.TrimEnd(',');
                                        deviceLocInfoValues = deviceLocInfoValues.TrimEnd(',');
                                        updateDeviceLocationValues = updateDeviceLocationValues.TrimEnd(',');

                                        DeviceLocationInfo deviceLocationInfo = new DeviceLocationInfo();
                                        int deviceLocInfoId = deviceLocationInfo.GetDevLocInfoIdByDeviceLocationID(deviceLocId);

                                        if (deviceLocInfoId > 0)
                                        {
                                            using (SqlConnection connection = new SqlConnection(constr))
                                            {
                                                connection.Open();
                                                query = "update Device_Location_Info set " + updateDeviceLocationValues + " where ID = '" + deviceLocInfoId + "'";
                                                SqlCommand cmd = new SqlCommand(query, connection);
                                                cmd.ExecuteNonQuery();
                                                connection.Close();
                                            }
                                        }
                                        else
                                        {
                                            using (SqlConnection connection = new SqlConnection(constr))
                                            {
                                                connection.Open();
                                                string deviceLocationInfoQuery = "insert into Device_Location_Info (" + deviceLocInfoFields + ") values(" + deviceLocInfoValues + ")";
                                                SqlCommand cmdDeviceLocationInfo = new SqlCommand(deviceLocationInfoQuery, connection);
                                                cmdDeviceLocationInfo.ExecuteNonQuery();
                                                connection.Close();
                                            }
                                        }
                                    }

                                    Location location = new Location();
                                    location.StartDate = UtilitiesDeviceERPSmartMeterLocation.StartDate;
                                    location.EndDate = UtilitiesDeviceERPSmartMeterLocation.EndDate;
                                    location.EquipmentId = equipmentId;
                                    locationList.Add(location);

                                    //Check Config_value 

                                    //Consumer Entry

                                    //ConsumerDevLocLink Entry
                                    //-----------------------------------------------

                                }



                                List<Location> SortedLists = locationList.OrderBy(o => o.EndDate).ToList();
                                string locationStatus = "";
                                foreach (var SortedList in SortedLists)
                                {
                                    if (SortedList.EndDate.ToString("yyyy-MM-dd") == "9999-12-31")
                                    {
                                        locationStatus = "Installed";
                                    }
                                    else
                                    {
                                        locationStatus = "InStock";
                                    }

                                    string compId = ConfigurationManager.AppSettings["compId"];

                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        connection.Open();
                                        query = "update Equipment set Status ='" + locationStatus + "',COMP_ID =" + compId + ", changed_by = '" + CreatedBy + "', changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where ID = " + SortedList.EquipmentId + "";
                                        SqlCommand cmd = new SqlCommand(query, connection);
                                        cmd.ExecuteNonQuery();
                                        connection.Close();
                                    }
                                }
                            }
                            else
                            {
                                isRejected = true;
                                errorMessage += "Location Missing;";
                            }
                        }
                        else
                        {
                            isRejected = true;
                            errorMessage += "Utility_ID Not Found in Equipment;";
                        }
                    }

                    if (!isRejected)
                    {
                        if (UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.SmartMeter != null)
                        {

                            if (UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID != null
                                    && UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value != null)
                            {
                                //Get HES Code
                                int hesCode = hes.GetHESIdByHESCode(UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value);

                                if (hesCode > 0)
                                {
                                    try
                                    {
                                        using (SqlConnection connection = new SqlConnection(constr))
                                        {
                                            connection.Open();
                                            query = "update equipment set HES_ID =" + hesCode + ",Data_Origin_Code='" + dataOriginID + "', changed_by = '" + CreatedBy + "', changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where ID = " + equipmentId + "";
                                            SqlCommand cmd = new SqlCommand(query, connection);
                                            cmd.ExecuteNonQuery();
                                            connection.Close();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                            }
                        }
                        //Write to File
                        var xmlString = Serialize<UtilitiesDeviceERPSmartMeterLocationNotification_Out.UtilsDvceERPSmrtMtrLocNotifMsg>(UtilitiesDeviceERPSmartMeterLocationNotification);
                        WriteToLog(xmlString, uuid, Constants.CisAdaptorLog_Path);
                    }
                    else
                    {
                        var xmlStrings = Serialize<UtilitiesDeviceERPSmartMeterLocationNotification_Out.UtilsDvceERPSmrtMtrLocNotifMsg>(UtilitiesDeviceERPSmartMeterLocationNotification);
                        WriteToLog(xmlStrings, uuid, Constants.Rejected_Path);
                    }

                }
                errorMessage = errorMessage.TrimEnd(',');
                //Exception_CIS_Sync
                exceptionSync.UUID = uuid;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = CreatedBy;
                exceptionSync.ServiceName = Constants.DeviceLocation_Notification_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);

                //HES Work Flow in Device Location Notification
                if (!isRejected)
                {
                    int hesId = hes.GetHESIdByHESCode(UtilitiesDeviceERPSmartMeterLocationNotification.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value);

                    hesWorkflowModel.WorkflowType = "EquipmentActivity";
                    hesWorkflowModel.RefrenceObjectType = "Equipment";
                    hesWorkflowModel.ReferenceObjId = Convert.ToString(equipmentId);
                    hesWorkflowModel.WorkflowStatus = "1";
                    hesWorkflowModel.HESId = hesId;
                    hesWorkflowModel.DataSource = "DeviceLocation_Notification";
                    hesWorkflowModel.CreatedBy = CreatedBy;
                    hesWorkflowModel.CreatedOn = DateTime.Now;

                    hesWorkflow.HesWorkFlowEntry(hesWorkflowModel);
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :DeviceLocation_Notification --- UUID :" + uuid + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                var xmlStrings = Serialize<UtilitiesDeviceERPSmartMeterLocationNotification_Out.UtilsDvceERPSmrtMtrLocNotifMsg>(UtilitiesDeviceERPSmartMeterLocationNotification);
                WriteToLog(xmlStrings, Constants.DeviceLocation_Notification_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        //[SoapDocumentMethod(OneWay = true)]
        //[WebMethod]
        //public void DeviceLocation_Notification_Bulk(UtilitiesDeviceERPSmartMeterLocationBulkNotification_Out.UtilsDvceERPSmrtMtrLocBulkNotifMsg UtilitiesDeviceERPSmartMeterLocationBulkNotification)
        //{





        //}

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void MeterConnectDisconnect_Request_Single(SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.SmrtMtrUtilsConncnStsChgReqERPCrteReqMsg SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest)
        {
            string query = "";
            string uuid = "";
            string createdBy = "";
            string errorMessage = "";
            bool isRejected = false;
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                //CIS Log entry
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.MessageHeader != null)
                {
                    if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.MessageHeader.UUID.Value;
                        uuid = SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }

                    if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.MessageHeader.SenderBusinessSystemID != null)
                    {
                        createdBy = SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.MessageHeader.SenderBusinessSystemID;
                    }
                    cISAdaptorParent.LogDateTime = SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.MessageHeader.CreationDateTime;
                }
                else
                {
                    uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Request_Type = "MeterConnectDisconnect_Request_Single";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);

                string referenceID = "";
                string categoryCode = "";
                bool immediateStatusChangeIndicator = false;
                string fields = "";
                string values = "";
                string updatefields = "";
                int amiReqResId = 0;
                int dataOriginID = 0;
                AMIDeviceControl aMIDeviceControl = new AMIDeviceControl();
                Equipment equipment = new Equipment();
                CategoryMaster categoryMaster = new CategoryMaster();
                ExceptionCISSync exceptionSync = new ExceptionCISSync();
                CategoryCodeMaster categoryCodeMaster = new CategoryCodeMaster();
                DataSourceMaster dataSourceMaster = new DataSourceMaster();
                EquipmentModel equipmentModel = new EquipmentModel();
                //Validation

                int i = 0;
                if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest != null)
                {
                    //Add UUID to table

                    if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.ID != null &&
                        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.ID.Value != null)
                    {
                        referenceID = Convert.ToInt64(SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.ID.Value).ToString();

                        fields += "Refrence_ID,";
                        values += "'" + referenceID + "',";
                        updatefields += "Refrence_ID =" + "'" + referenceID + "',";
                    }
                    if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.CategoryCode != null &&
                        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.CategoryCode.Value != null)
                    {
                        categoryCode = SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.CategoryCode.Value;

                        categoryCodeMaster = categoryMaster.GetCategoryMasterByCategoryCode(categoryCode);
                        if (categoryCodeMaster.ID > 0)
                        {
                            fields += "Category_Code,";
                            values += "'" + categoryCode + "',";
                            updatefields += "Category_Code =" + "'" + categoryCode + "',";
                        }
                        else
                        {
                            isRejected = true;
                            errorMessage = "Category ID not found in CategoryCodeMaster table ;";
                        }
                    }
                    if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.UtilitiesServiceDisconnectionReasonCode != null)
                    {
                        fields += "Reason_Code,";
                        values += "'" + SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.UtilitiesServiceDisconnectionReasonCode + "',";
                        updatefields += "Reason_Code =" + "'" + SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.UtilitiesServiceDisconnectionReasonCode + "',";
                    }
                    if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.PlannedProcessingDateTime != null
                        && SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.PlannedProcessingDateTime > DateTime.Now)
                    {
                        DateTime Planned_Processing_DateTime = dateTimeValidator.CheckDate(SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.PlannedProcessingDateTime);

                        fields += "Planned_Processing_DateTime,";
                        values += "'" + Planned_Processing_DateTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        updatefields += "Planned_Processing_DateTime =" + "'" + Planned_Processing_DateTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                    }
                    else
                    {
                        isRejected = true;
                        errorMessage += "Invalid value for Planned_Processing_DateTime;";
                    }

                    if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.ImmediateStatusChangeIndicator != false)
                    {
                        immediateStatusChangeIndicator = SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.ImmediateStatusChangeIndicator;
                        fields += "Immediate_Status_Change_Indicator,";
                        values += "'" + immediateStatusChangeIndicator + "',";
                        updatefields += "Immediate_Status_Change_Indicator =" + "'" + immediateStatusChangeIndicator + "',";
                    }

                    if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.DeviceConnectionStatus != null)
                    {
                        if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.DeviceConnectionStatus[i].UtilitiesDeviceID != null &&
                            SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.DeviceConnectionStatus[i].UtilitiesDeviceID.Value != null)
                        {
                            string utilityID = SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.DeviceConnectionStatus[i].UtilitiesDeviceID.Value;
                            equipmentModel = equipment.GetEquipmentByUtilityId(utilityID);

                            if (equipmentModel.ID > 0)
                            {
                                fields += "Equipment_ID,";
                                values += "'" + equipmentModel.ID + "',";
                                updatefields += "Equipment_ID =" + "'" + equipmentModel.ID + "',";
                            }
                            else
                            {
                                isRejected = true;
                                errorMessage += "Equipment ID not found in equipment table;";
                            }
                        }
                        //Add HES_ID to table
                        if (SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.DeviceConnectionStatus[i].SmartMeter.UtilitiesAdvancedMeteringSystemID != null &&
                            SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.DeviceConnectionStatus[i].SmartMeter.UtilitiesAdvancedMeteringSystemID.Value != null)
                        {
                            string hes_cd = SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.UtilitiesConnectionStatusChangeRequest.DeviceConnectionStatus[i].SmartMeter.UtilitiesAdvancedMeteringSystemID.Value;
                            int hesID = hes.GetHESIdByHESCode(hes_cd);
                            fields += "HES_ID,";
                            values += "'" + hesID + "',";
                            updatefields += "HES_ID =" + "'" + hesID + "',";
                        }
                        i++;
                    }

                    if (createdBy != null)
                    {
                        fields += "Created_by,";
                        values += "'" + createdBy + "',";
                        updatefields += "Changed_by = '" + createdBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                        dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(createdBy);
                        if (dataOriginID > 0)
                        {
                            fields += "Data_Origin_Code,";
                            values += "'" + dataOriginID + "',";
                            updatefields += "Data_Origin_Code = '" + dataOriginID + "',";
                        }
                        else
                        {
                            isRejected = true;
                            errorMessage += "SenderBusinessSystemID not found in DataSourceMaster table;";
                        }
                    }
                    if (!String.IsNullOrEmpty(uuid))
                    {
                        fields += "UUID,";
                        values += "'" + uuid + "',";
                        updatefields += "UUID = '" + uuid + "',";
                    }
                    fields += "Created_on,";
                    values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";


                    fields += "Request_Status,";
                    values += "" + 1 + ",";
                    updatefields += "Request_Status = 1,";

                    fields = fields.TrimEnd(',');
                    values = values.TrimEnd(',');
                    updatefields = updatefields.TrimEnd(',');
                }

                if (!isRejected)
                {
                    amiReqResId = aMIDeviceControl.GetAMIDeviceControlIDByReferenceID(referenceID);
                    if (amiReqResId > 0)
                    {
                        using (SqlConnection connection = new SqlConnection(constr))
                        {
                            connection.Open();
                            query = "update AMIDeviceControl set " + updatefields + "where ID = '" + amiReqResId + "'";
                            SqlCommand cmd = new SqlCommand(query, connection);
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }
                    }
                    else
                    {
                        using (SqlConnection connection = new SqlConnection(constr))
                        {
                            connection.Open();
                            query = "insert into AMIDeviceControl (" + fields + ") values(" + values + ")";
                            SqlCommand cmd = new SqlCommand(query, connection);
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }
                    }

                    HesInput hesInput = new HesInput();

                    if (immediateStatusChangeIndicator == true)
                    {
                        hesInput.Action = categoryCodeMaster.Category_Name;
                        hesInput.DeviceID = equipmentModel.MeterID;
                        hesInput.RefID = referenceID;
                        hesInput.ReplyURL = ConfigurationManager.AppSettings["hes_reply_url"];
                        hesInput.DemandResetFlag = "true";
                        hesInput.MRStartDate = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy HH:mm:ss");
                        hesInput.MREndDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                        hesInput.ParamName = new string[] { "KWH", "CKWH" };

                        string responseStatus = "";
                        HesResponse hesResponse = new HesResponse();
                        if (Convert.ToInt32(categoryCode) == categoryCodeMaster.ID)
                        {
                            HesConnector hesConnector = new HesConnector();
                            hesResponse = hesConnector.Hes_Connector(hesInput);
                        }
                        if (hesResponse.Code == 0)
                        {
                            responseStatus = "11";
                        }
                        //Set HES_Ack_Status to 11
                        aMIDeviceControl.AMIDeviceControlRequestStatusUpdate(amiReqResId, 2, responseStatus);
                    }

                    //Write to log
                    var xmlString = Serialize<SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.SmrtMtrUtilsConncnStsChgReqERPCrteReqMsg>(SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest);
                    WriteToLog(xmlString, uuid, Constants.CisAdaptorLog_Path);
                }
                else
                {
                    var xmlStrings = Serialize<SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.SmrtMtrUtilsConncnStsChgReqERPCrteReqMsg>(SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest);
                    WriteToLog(xmlStrings, uuid, Constants.Rejected_Path);
                }

                errorMessage = errorMessage.TrimEnd(',');
                //Exception_CIS_Sync
                exceptionSync.UUID = uuid;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = createdBy;
                exceptionSync.ServiceName = Constants.MeterConnectDisconnect_Request_Single_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :MeterConnectDisconnect_Request_Single --- UUID :" + uuid + " --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                var xmlStrings = Serialize<SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest.SmrtMtrUtilsConncnStsChgReqERPCrteReqMsg>(SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateRequest);
                WriteToLog(xmlStrings, Constants.MeterConnectDisconnect_Request_Single_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        //[SoapDocumentMethod(OneWay = true)]
        //[WebMethod]
        //public void AMIDCTResponse(HESReplyResult hesReplyResult)
        //{
        //    try
        //    {
        //        string uuid = "";
        //        string createdBy = "";
        //        bool isRejected = false;
        //        string errorMessage = "";
        //        string referenceID = "";
        //        string updatefields = "";
        //        string responseStatus = "";
        //        ExceptionCISSync exceptionSync = new ExceptionCISSync();
        //        AMIDeviceControl aMIDeviceControl = new AMIDeviceControl();
        //        AMIDeviceControlModel aMIDeviceControlModel = new AMIDeviceControlModel();

        //        //Validation
        //        if (hesReplyResult != null)
        //        {
        //            if (!String.IsNullOrEmpty(hesReplyResult.RefID))
        //            {
        //                referenceID = hesReplyResult.RefID;
        //            }
        //            //if (!String.IsNullOrEmpty(hESReplyResult.DeviceID))
        //            //{
        //            //    updatefields += "DeviceID =" + "'" + hESReplyResult.DeviceID + "',"; //where to update?
        //            //}
        //            //if (!String.IsNullOrEmpty(hESReplyResult.Action))
        //            //{
        //            //    updatefields += "Action =" + "'" + hESReplyResult.Action + "',";//where to update?
        //            //}
        //            if (!String.IsNullOrEmpty(hesReplyResult.ResponseCode))
        //            {
        //                updatefields += "Response_Code =" + "'" + hesReplyResult.ResponseCode + "',";
        //            }
        //            if (!String.IsNullOrEmpty(hesReplyResult.ResponseText))
        //            {
        //                updatefields += "Response_Text =" + "'" + hesReplyResult.ResponseText + "',";
        //            }
        //            if (!String.IsNullOrEmpty(hesReplyResult.ResponseStatus))
        //            {
        //                responseStatus = hesReplyResult.ResponseStatus;
        //                updatefields += "Response_Status =" + "'" + responseStatus + "',";
        //            }
        //            if (hesReplyResult.AMIResponseDateTime != null)
        //            {
        //                updatefields += "AMI_Response_DateTime =" + "'" + hesReplyResult.AMIResponseDateTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
        //            }
        //            if (hesReplyResult.HESResponseDateTime != null)
        //            {
        //                updatefields += "HES_Response_DateTime =" + "'" + hesReplyResult.HESResponseDateTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
        //            }

        //            updatefields += "Changed_by = 'HES_Connector', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

        //            updatefields += "Request_Status =" + "'3',";

        //            updatefields = updatefields.TrimEnd(',');
        //        }

        //        if (!isRejected)
        //        {
        //            using (SqlConnection connection = new SqlConnection(constr))
        //            {
        //                connection.Open();
        //                string query = "update AMIDeviceControl set " + updatefields + "where Refrence_ID = '" + referenceID + "'";
        //                SqlCommand cmd = new SqlCommand(query, connection);
        //                cmd.ExecuteNonQuery();
        //                connection.Close();
        //            }
        //            //Write to log
        //            var xmlString = Serialize<HESReplyResult>(hesReplyResult);
        //            WriteToLog(xmlString, uuid,Constants.CisAdaptorLog_Path);
        //        }
        //        else
        //        {
        //            var xmlStrings = Serialize<HESReplyResult>(hesReplyResult);
        //            WriteToLog(xmlStrings, uuid, Constants.Rejected_Path);
        //        }
        //        aMIDeviceControlModel = aMIDeviceControl.GetAMIDeviceControlByReferenceID(referenceID);

        //        #region Confirmation_In
        //        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_InBinding cls = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_InBinding();
        //        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.SmrtMtrUtilsConncnStsChgReqERPCrteConfMsg req = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.SmrtMtrUtilsConncnStsChgReqERPCrteConfMsg();
        //        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.BusinessDocumentMessageHeader head = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.BusinessDocumentMessageHeader();
        //        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UUID uid = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UUID();
        //        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UUID REFuid = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UUID();
        //        //SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UtilsDvceERPSmrtMtrRegChgConfUtilsDvce devie = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UtilsDvceERPSmrtMtrRegChgConfUtilsDvce();
        //        uid.Value = uuid;
        //        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UtilitiesDeviceID deviceid = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.UtilitiesDeviceID();
        //        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.Log log = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.Log();
        //        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.LogItem[] logitem = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.LogItem[1];
        //        SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.LogItem logitemr = new SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In.LogItem();

        //        System.Guid GUID;
        //        GUID = System.Guid.NewGuid();
        //        head.ReferenceUUID = uid;
        //        REFuid.Value = GUID.ToString();
        //        head.UUID = REFuid;
        //        head.CreationDateTime = DateTime.Now;
        //        head.SenderBusinessSystemID = "TPDDL";
        //        DataTable dturl = new DataTable();
        //        dturl.Clear();
        //        dturl = hes.GetSAPAMIUrl("SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In");

        //        cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
        //        cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();

        //        req.MessageHeader = head;
        //        req.UtilitiesConnectionStatusChangeRequest.ID.Value = referenceID;
        //        req.UtilitiesConnectionStatusChangeRequest.CategoryCode.Value = aMIDeviceControlModel.CategoryCode;
        //        foreach (var DeviceConnectionStatus in req.UtilitiesConnectionStatusChangeRequest.DeviceConnectionStatus)
        //        {
        //            DeviceConnectionStatus.ProcessingDateTime = aMIDeviceControlModel.ProcessingDateTime;
        //            DeviceConnectionStatus.UtilitiesDeviceID.Value = aMIDeviceControlModel.UtilitiesDeviceID;
        //        }
        //        //deviceid.Value = UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice.ID.Value;
        //        //devie.ID = deviceid;
        //        //req.UtilitiesDevice = devie;
        //        log.BusinessDocumentProcessingResultCode = "3";
        //        log.MaximumLogItemSeverityCode = "1";
        //        logitemr.TypeID = "0000";
        //        logitemr.SeverityCode = "1";
        //        logitemr.Note = "Request Processed Successfully";
        //        logitem[0] = logitemr;
        //        log.Item = logitem;
        //        //req.Log = log;
        //        cls.SmartMeterUtilitiesConnectionStatusChangeRequestERPCreateConfirmation_In(req);
        //        #endregion

        //        //update Request_Status=4 in AMIDeviceControl table
        //        if (aMIDeviceControlModel.ID > 0)
        //        {
        //            aMIDeviceControl.AMIDeviceControlRequestStatusUpdate(aMIDeviceControlModel.ID, 4, responseStatus);
        //        }
        //        //Exception_CIS_Sync
        //        errorMessage = errorMessage.TrimEnd(',');

        //        exceptionSync.UUID = uuid;
        //        exceptionSync.Status = isRejected ? "Failure" : "Success";
        //        exceptionSync.Message = errorMessage;
        //        exceptionSync.CreatedBy = createdBy;
        //        exceptionSync.ServiceName = "AMIDCTResponse";
        //        Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
        //        exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void MeterReading_Request_Bulk(SmartMeterMeterReadingDocumentERPBulkCreateRequest_Out.SmrtMtrMtrRdngDocERPBulkCrteReqMsg SmartMeterMeterReadingDocumentERPBulkCreateRequest)
        {
            string query = "";
            string uuid = "";
            string createdBy = "";
            string parentUUID = "";
            string utilitiesDeviceID = "";
            string configValue = "";
            string errorMessage = "";
            DateTime CreationDatetime = DateTime.MinValue;
            int dataOriginID = 0;
            bool isRejected = false;
            Equipment equipment = new Equipment();
            DataSourceMaster dataSourceMaster = new DataSourceMaster();
            ExceptionCISSync exceptionSync = new ExceptionCISSync();
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            void Confirmation_In()
            {
                #region Confirmation
                SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_InBinding cls = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_InBinding();
                SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmrtMtrMtrRdngDocERPBulkCrteConfMsg req = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmrtMtrMtrRdngDocERPBulkCrteConfMsg();
                SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.BusinessDocumentMessageHeader head = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.BusinessDocumentMessageHeader();
                SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UUID uid = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UUID();
                SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UUID REFuid = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UUID();
                uid.Value = parentUUID;
                SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.Log log = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.Log();
                SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.LogItem[] logitem = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.LogItem[1];
                SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.LogItem logitemr = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.LogItem();
                SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmrtMtrMtrRdngDocERPCrteConfMsg[] MeterList = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmrtMtrMtrRdngDocERPCrteConfMsg[SmartMeterMeterReadingDocumentERPBulkCreateRequest.SmartMeterMeterReadingDocumentERPCreateRequestMessage.Length];

                System.Guid GUID;
                GUID = System.Guid.NewGuid();
                head.ReferenceUUID = uid;
                REFuid.Value = GUID.ToString();
                head.UUID = REFuid;
                head.CreationDateTime = DateTime.Now;
                head.SenderBusinessSystemID = "TPDDL";
                DataTable dturl = new DataTable();
                dturl.Clear();
                dturl = hes.GetSAPAMIUrl("MeterReading_Request_Bulk_Confirmation");

                cls.Credentials = new System.Net.NetworkCredential(dturl.Rows[0]["UserId"].ToString(), dturl.Rows[0]["Password"].ToString());
                cls.Url = dturl.Rows[0]["ServiceUrl"].ToString();

                req.MessageHeader = head;

                int i = 0;
                foreach (var utilsDvceERPSmrtMtrCrteReqMsg in SmartMeterMeterReadingDocumentERPBulkCreateRequest.SmartMeterMeterReadingDocumentERPCreateRequestMessage)
                {
                    SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmrtMtrMtrRdngDocERPCrteConfMsg Meter = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmrtMtrMtrRdngDocERPCrteConfMsg();
                    SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.BusinessDocumentMessageHeader Childhead = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.BusinessDocumentMessageHeader();
                    // SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.dvcesmrtmtr devie = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UtilsDvceERPSmrtMtrRplctnConfUtilsDvce();
                    // SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UtilitiesDeviceID deviceid = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UtilitiesDeviceID();
                    SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UUID childUuid = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UUID();
                    SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UUID childREFuid = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.UUID();
                    SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmrtMtrMtrRdngDocERPCrteConfMtrRdngDoc MRDOC = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.SmrtMtrMtrRdngDocERPCrteConfMtrRdngDoc();
                    SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.MeterReadingDocumentID md = new SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In.MeterReadingDocumentID();
                    Guid childGUID = System.Guid.NewGuid();
                    childUuid.Value = utilsDvceERPSmrtMtrCrteReqMsg.MessageHeader.UUID.Value;
                    //.Value = utilsDvceERPSmrtMtrCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID.Value;
                    //.ID = deviceid;
                    Childhead.ReferenceUUID = childUuid;
                    md.Value = utilsDvceERPSmrtMtrCrteReqMsg.MeterReadingDocument.ID.Value.ToString();
                    MRDOC.ID = md;
                    //  MRDOC.ID = utilsDvceERPSmrtMtrCrteReqMsg.MeterReadingDocument.ID.Value.ToString();

                    Meter.MeterReadingDocument = MRDOC;
                    childREFuid.Value = childGUID.ToString();
                    Childhead.UUID = childREFuid;
                    Childhead.CreationDateTime = DateTime.Now;
                    Childhead.SenderBusinessSystemID = "TPDDL";
                    //Childhead. = devie;
                    Meter.MessageHeader = Childhead;
                    // Meter.UtilitiesDevice = devie;
                    log.BusinessDocumentProcessingResultCode = "3";
                    log.MaximumLogItemSeverityCode = "1";
                    logitemr.TypeID = "61204";
                    if (isRejected)
                    {
                        configValue = hes.GetConifigValueByParam(Constants.MeterReading_Request_Bulk_MethodName, Constants.FailureSeverityCode);
                        logitemr.SeverityCode = configValue;
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        configValue = hes.GetConifigValueByParam(Constants.MeterReading_Request_Bulk_MethodName, Constants.SuccessSeverityCode);
                        logitemr.SeverityCode = configValue;
                        if (!String.IsNullOrEmpty(errorMessage))
                        {
                            logitemr.Note = errorMessage;
                        }
                        else
                        {
                            logitemr.Note = "Request Processed Successfully";
                        }
                    }
                    logitem[0] = logitemr;
                    log.Item = logitem;
                    Meter.Log = log;
                    MeterList[i] = Meter;

                    i++;
                }
                // deviceid.Value = UtilitiesDeviceERPSmartMeterRegisterChangeRequest.UtilitiesDevice.ID.Value;
                // devie.ID = deviceid;
                // req.UtilitiesDevice = devie;
                req.SmartMeterMeterReadingDocumentERPCreateConfirmationMessage = MeterList;
                // req.
                log.BusinessDocumentProcessingResultCode = "3";
                log.MaximumLogItemSeverityCode = "1";
                logitemr.TypeID = "61204";
                if (isRejected)
                {
                    configValue = hes.GetConifigValueByParam(Constants.MeterReading_Request_Bulk_MethodName, Constants.FailureSeverityCode);
                    logitemr.SeverityCode = configValue;
                    logitemr.Note = errorMessage;
                }
                else
                {
                    configValue = hes.GetConifigValueByParam(Constants.MeterReading_Request_Bulk_MethodName, Constants.SuccessSeverityCode);
                    logitemr.SeverityCode = configValue;
                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        logitemr.Note = errorMessage;
                    }
                    else
                    {
                        logitemr.Note = "Request Processed Successfully";
                    }
                }
                logitem[0] = logitemr;
                log.Item = logitem;
                req.Log = log;

                cls.SmartMeterMeterReadingDocumentERPBulkCreateConfirmation_In(req);
                #endregion

            }
            try
            {
                #region CIS_ADAPTOR_LOG
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (SmartMeterMeterReadingDocumentERPBulkCreateRequest.MessageHeader != null)
                {
                    if (SmartMeterMeterReadingDocumentERPBulkCreateRequest.MessageHeader.SenderBusinessSystemID != null)
                    {
                        createdBy = SmartMeterMeterReadingDocumentERPBulkCreateRequest.MessageHeader.SenderBusinessSystemID;
                    }
                    if (SmartMeterMeterReadingDocumentERPBulkCreateRequest.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = SmartMeterMeterReadingDocumentERPBulkCreateRequest.MessageHeader.UUID.Value;
                        parentUUID = SmartMeterMeterReadingDocumentERPBulkCreateRequest.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }

                    cISAdaptorParent.LogDateTime = SmartMeterMeterReadingDocumentERPBulkCreateRequest.MessageHeader.CreationDateTime;
                }
                else
                {
                    parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Child_Count = SmartMeterMeterReadingDocumentERPBulkCreateRequest.SmartMeterMeterReadingDocumentERPCreateRequestMessage.Length;
                cISAdaptorParent.Request_Type = "MeterReading_Request_Bulk";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);
                #endregion

                int nodeCount = 0;
                foreach (var smrtMtrMtrRdngDocERPCrteReqMsg in SmartMeterMeterReadingDocumentERPBulkCreateRequest.SmartMeterMeterReadingDocumentERPCreateRequestMessage)
                {
                    nodeCount++;

                    #region CIS_ADAPTOR_CHILD_LOG
                    CISAdaptorChild cISAdaptorChild = new CISAdaptorChild();
                    if (smrtMtrMtrRdngDocERPCrteReqMsg.MessageHeader != null)
                    {
                        if (smrtMtrMtrRdngDocERPCrteReqMsg.MessageHeader.UUID != null)
                        {
                            cISAdaptorChild.UUID = smrtMtrMtrRdngDocERPCrteReqMsg.MessageHeader.UUID.Value;
                            uuid = smrtMtrMtrRdngDocERPCrteReqMsg.MessageHeader.UUID.Value;
                        }
                        else
                        {
                            uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                        }
                        cISAdaptorChild.LogDateTime = smrtMtrMtrRdngDocERPCrteReqMsg.MessageHeader.CreationDateTime;
                        CreationDatetime = smrtMtrMtrRdngDocERPCrteReqMsg.MessageHeader.CreationDateTime;
                    }
                    else
                    {
                        uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    cISAdaptorChild.Parent_UUID = parentUUID;
                    cISAdaptorChild.Request_Type = "MeterReading_Request_Bulk";
                    cisAdaptorLog.CISAdaptorChildLog(cISAdaptorChild);
                    #endregion

                    string fields = "";
                    string values = "";
                    string reqResUpdateValues = "";

                    if (smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.ID != null
                        && smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.ID.Value != null)
                    {
                        fields += "Doc_ID,";
                        values += "'" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.ID.Value + "',";
                        reqResUpdateValues += "Doc_ID ='" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.ID.Value + "',";
                    }
                    if (!String.IsNullOrEmpty(smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.MeterReadingReasonCode))
                    {
                        string mrReasonCode = "";

                        if (smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.MeterReadingReasonCode.Length == 1)
                        {
                            mrReasonCode = "0" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.MeterReadingReasonCode + "";
                        }
                        else
                        {
                            mrReasonCode = smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.MeterReadingReasonCode;
                        }
                        fields += "MRReasonCode,";
                        values += "'" + mrReasonCode + "',";
                        reqResUpdateValues += "MRReasonCode ='" + mrReasonCode + "',";
                    }
                    if (smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.ScheduledMeterReadingDate != DateTime.MinValue)
                    {
                        fields += "MeterReadEndDate,";
                        values += "'" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.ScheduledMeterReadingDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        reqResUpdateValues += "MeterReadEndDate ='" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.ScheduledMeterReadingDate.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                    }
                    if (!String.IsNullOrEmpty(smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtilitiesAdvancedMeteringDataSourceTypeCode))
                    {
                        fields += "ReadSource,";
                        values += "'" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtilitiesAdvancedMeteringDataSourceTypeCode + "',";
                        reqResUpdateValues += "ReadSource ='" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtilitiesAdvancedMeteringDataSourceTypeCode + "',";
                    }
                    if (smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask != null)
                    {
                        if (smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtilitiesMeasurementTaskID != null
                            && smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtilitiesMeasurementTaskID.Value != null)
                        {
                            fields += "LogRegno,";
                            values += "'" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtilitiesMeasurementTaskID.Value + "',";
                            reqResUpdateValues += "LogRegno ='" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtilitiesMeasurementTaskID.Value + "',";
                        }
                        if (!String.IsNullOrEmpty(smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtilitiesObjectIdentificationSystemCodeText))
                        {
                            fields += "RegCode,";
                            values += "'" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtilitiesObjectIdentificationSystemCodeText + "',";
                            reqResUpdateValues += "RegCode ='" + smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtilitiesObjectIdentificationSystemCodeText + "',";
                        }
                        fields += "Status,";
                        values += "0,";
                        if (smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice != null)
                        {
                            if (smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID != null
                                && smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID.Value != null)
                            {
                                utilitiesDeviceID = smrtMtrMtrRdngDocERPCrteReqMsg.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID.Value;
                                int equipmentId = equipment.GetEquipmentIdByUtilityId(utilitiesDeviceID);
                                if (equipmentId > 0)
                                {
                                    fields += "Equipment_Id,";
                                    values += equipmentId + ",";
                                    reqResUpdateValues += "Equipment_Id =" + equipmentId + ",";
                                }
                                fields += "Utility_ID,";
                                values += "'" + utilitiesDeviceID + "',";
                                reqResUpdateValues += "Utility_ID ='" + utilitiesDeviceID + "',";
                            }
                        }
                        string firstBill = "";
                        string configValues = "";
                        //if (smrtMtrMtrRdngDocERPCrteReqMsg.BillingAttribute != null)
                        //{
                        ConfigReadRequest configReadRequest = new ConfigReadRequest();
                        DevLocDeviceLink devLocDeviceLink = new DevLocDeviceLink();
                        DateTime validFromDate = new DateTime();
                        configValues = configReadRequest.GetConfigValueByType();
                        //if (!String.IsNullOrEmpty(smrtMtrMtrRdngDocERPCrteReqMsg.BillingAttribute.FirstBill))
                        //{
                        if (configValues == "N")
                        {
                            firstBill = smrtMtrMtrRdngDocERPCrteReqMsg.BillingAttribute.FirstBill;
                        }
                        else if (configValues == "Y")
                        {
                            int equipmentID = equipment.GetEquipmentIdByUtilityId(utilitiesDeviceID);
                            validFromDate = devLocDeviceLink.GetValidFromDateByEquipmentID(equipmentID);
                            if (validFromDate >= DateTime.Now.AddDays(-30))
                            {
                                firstBill = "Y";
                            }
                            else
                            {
                                firstBill = "N";
                            }
                        }
                        fields += "FirstBill,";
                        values += "'" + firstBill + "',";
                        reqResUpdateValues += "FirstBill ='" + firstBill + "',";
                        //}

                        if (smrtMtrMtrRdngDocERPCrteReqMsg.BillingAttribute != null)
                        {
                            if (!String.IsNullOrEmpty(smrtMtrMtrRdngDocERPCrteReqMsg.BillingAttribute.ReadType))
                            {
                                fields += "RequestedReadType,";
                                values += "'" + smrtMtrMtrRdngDocERPCrteReqMsg.BillingAttribute.ReadType + "',";
                                reqResUpdateValues += "RequestedReadType ='" + smrtMtrMtrRdngDocERPCrteReqMsg.BillingAttribute.ReadType + "',";
                            }
                        }

                        //}

                        if (!String.IsNullOrEmpty(parentUUID))
                        {
                            fields += "HeaderUUID,";
                            values += "'" + parentUUID + "',";
                            reqResUpdateValues += "HeaderUUID ='" + parentUUID + "',";
                        }
                        if (!String.IsNullOrEmpty(uuid))
                        {
                            fields += "ChildUUID,";
                            values += "'" + uuid + "',";
                            reqResUpdateValues += "ChildUUID ='" + uuid + "',";
                        }
                        if (CreationDatetime != DateTime.MinValue)
                        {
                            fields += "CreationDatetime,";
                            values += "'" + CreationDatetime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                            reqResUpdateValues += "CreationDatetime ='" + CreationDatetime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        }
                        if (!String.IsNullOrEmpty(createdBy))
                        {
                            fields += "Created_by,";
                            values += "'" + createdBy + "',";
                            reqResUpdateValues += "Changed_by = '" + createdBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                            dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(createdBy);
                            if (dataOriginID > 0)
                            {
                                fields += "Data_Origin_Code,";
                                values += "'" + dataOriginID + "',";
                                reqResUpdateValues += "Data_Origin_Code = '" + dataOriginID + "',";
                            }
                        }
                        fields += "Created_on,";
                        values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');
                        reqResUpdateValues = reqResUpdateValues.TrimEnd(',');

                        using (SqlConnection connection = new SqlConnection(constr))
                        {
                            connection.Open();
                            query = "insert into ReadRequestResponse (" + fields + ") values(" + values + ")";
                            SqlCommand cmd = new SqlCommand(query, connection);
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }

                        if (nodeCount == SmartMeterMeterReadingDocumentERPBulkCreateRequest.SmartMeterMeterReadingDocumentERPCreateRequestMessage.Length)
                        {
                            //Update RecordStatus
                            using (SqlConnection connection = new SqlConnection(constr))
                            {
                                connection.Open();
                                query = "update ReadRequestResponse set RecordStatus='C' where HeaderUUID='" + parentUUID + "'";
                                SqlCommand cmd = new SqlCommand(query, connection);
                                cmd.ExecuteNonQuery();
                                connection.Close();
                            }
                        }

                    }

                }

                //Write to file
                var xmlString = Serialize<SmartMeterMeterReadingDocumentERPBulkCreateRequest_Out.SmrtMtrMtrRdngDocERPBulkCrteReqMsg>(SmartMeterMeterReadingDocumentERPBulkCreateRequest);
                WriteToLog(xmlString, uuid, Constants.CisAdaptorLog_Path);

                Confirmation_In();
                //Change Status to 1 based on parentUUID
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    query = "update ReadRequestResponse set Status=1,Data_Origin_Code = '" + dataOriginID + "',Changed_by = '" + createdBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where HeaderUUID='" + parentUUID + "' and Status=0";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
                //Exception_CIS_Sync
                exceptionSync.UUID = uuid;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = createdBy;
                exceptionSync.ServiceName = Constants.MeterReading_Request_Bulk_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :MeterReading_Request_Bulk --- UUID :" + uuid + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                errorMessage += "Exception :" + ex.Message;
                Confirmation_In();
                var xmlString = Serialize<SmartMeterMeterReadingDocumentERPBulkCreateRequest_Out.SmrtMtrMtrRdngDocERPBulkCrteReqMsg>(SmartMeterMeterReadingDocumentERPBulkCreateRequest);
                WriteToLog(xmlString, Constants.MeterReading_Request_Bulk_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void ConsumerMasterData_Sync(SI_AMI_ConsumerMasterData_Out.DT_ConsumerMasterData_Req dT_ConsumerMasterData_Req)
        {
            string fields = "";
            string values = "";
            string query = "";
            bool isRejected = false;
            string errorMessage = "";
            string consumerUpdateFields = "";
            ExceptionCISSync exceptionSync = new ExceptionCISSync();
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                if (dT_ConsumerMasterData_Req.Request.Consumer != null)
                {
                    foreach (var consumers in dT_ConsumerMasterData_Req.Request.Consumer)
                    {
                        #region Consumer

                        string utilityID = "";
                        int _consumerId = 0;
                        if (consumers.SyncMode == "Full")
                        {
                            if (consumers.ConsumerCreate != null)
                            {
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.ConsumerRefID))
                                {
                                    utilityID = consumers.ConsumerCreate.ConsumerRefID;
                                    fields += "Utility_ID,";
                                    values += "'" + utilityID + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.AltUtilityID))
                                {
                                    fields += "Alt_Utility_ID,";
                                    values += "'" + consumers.ConsumerCreate.AltUtilityID + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.ConsSeg))
                                {
                                    fields += "Cons_SEG,";
                                    values += "'" + consumers.ConsumerCreate.ConsSeg + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.VIP))
                                {
                                    fields += "VIP,";
                                    values += "'" + consumers.ConsumerCreate.VIP + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.EssentialService))
                                {
                                    fields += "Essential_Service,";
                                    values += "'" + consumers.ConsumerCreate.EssentialService + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.SALUTATION))
                                {
                                    fields += "SALUTATION,";
                                    values += "'" + consumers.ConsumerCreate.SALUTATION + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.FirstName))
                                {
                                    fields += "FIRST_NAME,";
                                    values += "'" + consumers.ConsumerCreate.FirstName + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.MiddleName))
                                {
                                    fields += "MIDDLE_NAME,";
                                    values += "'" + consumers.ConsumerCreate.MiddleName + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.LastName))
                                {
                                    fields += "LAST_NAME,";
                                    values += "'" + consumers.ConsumerCreate.LastName + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Address1))
                                {
                                    fields += "Address1,";
                                    values += "'" + consumers.ConsumerCreate.Address1 + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Address2))
                                {
                                    fields += "Address2,";
                                    values += "'" + consumers.ConsumerCreate.Address2 + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Pin))
                                {
                                    fields += "PIN,";
                                    values += "'" + consumers.ConsumerCreate.Pin + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.District))
                                {
                                    fields += "DISTRICT,";
                                    values += "'" + consumers.ConsumerCreate.District + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Region))
                                {
                                    fields += "REGION,";
                                    values += "'" + consumers.ConsumerCreate.Region + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Utility_Office_Id))
                                {
                                    fields += "Utility_Office_Id,";
                                    values += "'" + consumers.ConsumerCreate.Utility_Office_Id + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Status))
                                {
                                    fields += "Status,";
                                    values += "'" + consumers.ConsumerCreate.Status + "',";
                                }
                                else
                                {
                                    fields += "Status,";
                                    values += "'A',";
                                }
                                fields += "Created_by,";
                                values += "'Demo',";

                                fields += "Created_on,";
                                values += "'" + DateTime.Now.ToString() + "',";

                                fields = fields.TrimEnd(',');
                                values = values.TrimEnd(',');
                                Consumer consumer = new Consumer();
                                _consumerId = consumer.GetConsumerByConsumerID(utilityID);

                                if (_consumerId > 0)
                                {
                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        try
                                        {
                                            connection.Open();
                                            query = "UPDATE Consumer SET Alt_Utility_ID = '"
                                               + consumers.ConsumerCreate.AltUtilityID + "',Cons_SEG = '" + consumers.ConsumerCreate.ConsSeg + "',VIP = '"
                                               + consumers.ConsumerCreate.VIP + "',Essential_Service = '" + consumers.ConsumerCreate.EssentialService + "',SALUTATION = '"
                                               + consumers.ConsumerCreate.SALUTATION + "', FIRST_NAME = '" + consumers.ConsumerCreate.FirstName + "',MIDDLE_NAME = '"
                                               + consumers.ConsumerCreate.MiddleName + "', LAST_NAME = '" + consumers.ConsumerCreate.LastName + "',Address1 = '"
                                               + consumers.ConsumerCreate.Address1 + "',Address2 = '" + consumers.ConsumerCreate.Address2 + "', PIN = '"
                                               + consumers.ConsumerCreate.Pin + "',DISTRICT = '" + consumers.ConsumerCreate.District + "', REGION = '"
                                               + consumers.ConsumerCreate.Region + "', Utility_Office_Id = '" + consumers.ConsumerCreate.Utility_Office_Id
                                               + "', Status = '" + consumers.ConsumerCreate.Status + "',Changed_by ='Demo',Changed_On ='" + DateTime.Now.ToString()
                                               + "' where ID='" + _consumerId + "';";
                                            SqlCommand cmd = new SqlCommand(query, connection);
                                            cmd.ExecuteNonQuery();
                                            connection.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                }
                                else
                                {
                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        try
                                        {
                                            connection.Open();
                                            query = "insert into Consumer (" + fields + ") values(" + values + ")";
                                            SqlCommand cmd = new SqlCommand(query, connection);
                                            cmd.ExecuteNonQuery();
                                            connection.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                }

                            }
                        }
                        else if (consumers.SyncMode == "Alter")
                        {
                            if (consumers.ConsumerCreate != null)
                            {
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.ConsumerRefID))
                                {
                                    utilityID = consumers.ConsumerCreate.ConsumerRefID;
                                    //consumerUpdateFields += "ConsumerID =" + consumerID + "',";
                                }
                                else
                                {
                                    errorMessage = "ConsumerRefID not found in payload;";
                                    isRejected = true;
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.AltUtilityID))
                                {
                                    consumerUpdateFields += "Alt_Utility_ID = '" + consumers.ConsumerCreate.AltUtilityID + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.ConsSeg))
                                {
                                    consumerUpdateFields += "Cons_SEG = '" + consumers.ConsumerCreate.ConsSeg + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.VIP))
                                {
                                    consumerUpdateFields += "VIP = '" + consumers.ConsumerCreate.VIP + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.EssentialService))
                                {
                                    consumerUpdateFields += "Essential_Service = '" + consumers.ConsumerCreate.EssentialService + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.SALUTATION))
                                {
                                    consumerUpdateFields += "SALUTATION = '" + consumers.ConsumerCreate.SALUTATION + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.FirstName))
                                {
                                    consumerUpdateFields += "FIRST_NAME = '" + consumers.ConsumerCreate.FirstName + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.MiddleName))
                                {
                                    consumerUpdateFields += "MIDDLE_NAME = '" + consumers.ConsumerCreate.MiddleName + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.LastName))
                                {
                                    consumerUpdateFields += "LAST_NAME = '" + consumers.ConsumerCreate.LastName + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Address1))
                                {
                                    consumerUpdateFields += "Address1 = '" + consumers.ConsumerCreate.Address1 + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Address2))
                                {
                                    consumerUpdateFields += "Address2 = '" + consumers.ConsumerCreate.Address2 + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Pin))
                                {
                                    consumerUpdateFields += "PIN = '" + consumers.ConsumerCreate.Pin + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.District))
                                {
                                    consumerUpdateFields += "DISTRICT = '" + consumers.ConsumerCreate.District + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Region))
                                {
                                    consumerUpdateFields += "REGION = '" + consumers.ConsumerCreate.Region + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Utility_Office_Id))
                                {
                                    consumerUpdateFields += "Utility_Office_Id = '" + consumers.ConsumerCreate.Utility_Office_Id + "',";
                                }
                                if (!String.IsNullOrEmpty(consumers.ConsumerCreate.Status))
                                {
                                    consumerUpdateFields += "Status = '" + consumers.ConsumerCreate.Status + "',";
                                }
                                else
                                {
                                    consumerUpdateFields += "Status ='A',";
                                }

                                consumerUpdateFields += "Changed_by='Demo',";
                                consumerUpdateFields += "Changed_On= '" + DateTime.Now.ToString() + "',";

                                consumerUpdateFields = consumerUpdateFields.TrimEnd(',');

                                Consumer consumer = new Consumer();
                                _consumerId = consumer.GetConsumerByConsumerID(utilityID);

                                if (_consumerId > 0)
                                {
                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        try
                                        {
                                            connection.Open();
                                            query = "Update Consumer set " + consumerUpdateFields + " where Utility_ID=" + utilityID + "";
                                            SqlCommand cmd = new SqlCommand(query, connection);
                                            cmd.ExecuteNonQuery();
                                            connection.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                }
                                else
                                {
                                    isRejected = true;
                                    errorMessage = "Utility_ID not found in Consumer Table;";
                                }
                            }
                        }

                        #endregion

                        #region ConsumerDevLocLink

                        string consDevLocLnkFields = "";
                        string consDevLocLnkValues = "";
                        string updateconsDevLocLnkFields = "";
                        string ValidFromDate = "";
                        string ValidToDate = "";
                        //Consumer_DevLoc_link
                        if (consumers.Consumer_DevLoc_link != null)
                        {
                            foreach (var consumerDevLocLinks in consumers.Consumer_DevLoc_link)
                            {
                                int deviceLocationID = 0;
                                DeviceLocation deviceLocation = new DeviceLocation();
                                if (!String.IsNullOrEmpty(consumerDevLocLinks.DevLoc))
                                {
                                    var devLoc = consumerDevLocLinks.DevLoc;
                                    consDevLocLnkFields += "ConsumerID,";
                                    consDevLocLnkValues += "'" + _consumerId + "',";
                                    updateconsDevLocLnkFields += "ConsumerID = '" + _consumerId + "',";
                                    deviceLocationID = deviceLocation.GetDeviceLocIdByUtilityId(devLoc);
                                }
                                if (deviceLocationID > 0)
                                {
                                    consDevLocLnkFields += "DeviceLocationId,";
                                    consDevLocLnkValues += "'" + deviceLocationID + "',";
                                    updateconsDevLocLnkFields += "DeviceLocationId = '" + deviceLocationID + "',";
                                }
                                else
                                {
                                    isRejected = true;
                                    errorMessage = "Invalid Device Location ID detected;";
                                }

                                ConsumerDevlocLink consumerDevlocLink = new ConsumerDevlocLink();
                                List<string> existingConsumerIds = consumerDevlocLink.GetConsumerIDByDevLocId(deviceLocationID);

                                if (!String.IsNullOrEmpty(consumerDevLocLinks.StartDate))
                                {
                                    ValidFromDate = consumerDevLocLinks.StartDate;
                                    consDevLocLnkFields += "ValidFromDate,";
                                    consDevLocLnkValues += "'" + ValidFromDate + "',";
                                    updateconsDevLocLnkFields += "ValidFromDate = '" + ValidFromDate + "',";
                                }
                                else
                                {
                                    errorMessage = "StartDate missing in payload;";
                                    isRejected = true;
                                }
                                if (!String.IsNullOrEmpty(consumerDevLocLinks.EndDate))
                                {
                                    ValidToDate = consumerDevLocLinks.EndDate;
                                    consDevLocLnkFields += "ValidToDate,";
                                    consDevLocLnkValues += "'" + ValidToDate + "',";
                                    updateconsDevLocLnkFields += "ValidToDate = '" + ValidToDate + "',";
                                }

                                consDevLocLnkFields += "Created_by,";
                                consDevLocLnkValues += "'Demo',";

                                consDevLocLnkFields += "Created_on,";
                                consDevLocLnkValues += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                updateconsDevLocLnkFields += "Changed_by = 'Demo',Changed_on='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                consDevLocLnkFields = consDevLocLnkFields.TrimEnd(',');
                                consDevLocLnkValues = consDevLocLnkValues.TrimEnd(',');
                                updateconsDevLocLnkFields = updateconsDevLocLnkFields.TrimEnd(',');

                                bool isDifferentConsumer = false;
                                foreach (var existingConsumerId in existingConsumerIds)
                                {
                                    if (existingConsumerId != _consumerId.ToString())
                                    {
                                        isDifferentConsumer = true;
                                    }
                                }

                                int startDateCount = consumerDevlocLink.CheckValidFromDate(ValidFromDate);
                                if (startDateCount > 0 && isDifferentConsumer)
                                {
                                    isRejected = true;
                                    errorMessage += "DeviceLocationId not consistent row;";
                                }

                                int sameTimeSlice = consumerDevlocLink.CheckSameTimeSlice(ValidFromDate, ValidToDate);
                                if (sameTimeSlice > 0 && isDifferentConsumer)
                                {
                                    isRejected = true;
                                    errorMessage += "Same time slice row;";
                                }


                                if (!isRejected)
                                {
                                    //insert ConsumerDevLocLink
                                    using (SqlConnection connection = new SqlConnection(constr))
                                    {
                                        try
                                        {
                                            connection.Open();
                                            query = "insert into ConsumerDevLocLink (" + consDevLocLnkFields + ") values(" + consDevLocLnkValues + ")";
                                            SqlCommand cmd = new SqlCommand(query, connection);
                                            cmd.ExecuteNonQuery();
                                            connection.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                }


                            }
                        }

                        #endregion
                    }
                }

                if (dT_ConsumerMasterData_Req.Request.ObjectAttribute != null)
                {
                    AttributeNames attributeNames = new AttributeNames();
                    CheckValidTimeSlice checkValidTimeSlice = new CheckValidTimeSlice();
                    foreach (var objectAttributes in dT_ConsumerMasterData_Req.Request.ObjectAttribute)
                    {
                        if (objectAttributes.MasterObject != null)
                        {
                            string utilityId = "";
                            string updateFields = "";
                            int _tableId = 0;
                            int _attrTableId = 0;

                            if (objectAttributes.MasterObjectID.MasterObjectIDval != null)
                            {
                                utilityId = objectAttributes.MasterObjectID.MasterObjectIDval;
                            }

                            string masterTableName = "";
                            string attributeTableName = "";
                            string entityType = "";
                            string uniqueIdCol = "";

                            if (objectAttributes.MasterObject == Constants.Consumer)        // Consumer table
                            {
                                entityType = Constants.ConsumerTable;
                                masterTableName = Constants.ConsumerTable;
                                attributeTableName = Constants.ConsumerAttrTable;
                                uniqueIdCol = "Utility_ID";

                                Consumer consumer = new Consumer();
                                _tableId = consumer.GetConsumerByConsumerID(utilityId);

                                ConsumerAttribute consumerAttribute = new ConsumerAttribute();
                                _attrTableId = consumerAttribute.GetIDByUtilityID(utilityId);
                            }
                            else if (objectAttributes.MasterObject == Constants.Equipment)        // Equipment table
                            {
                                entityType = Constants.EquipmentTable;
                                masterTableName = Constants.EquipmentTable;
                                attributeTableName = Constants.EquipmentAttrTable;
                                uniqueIdCol = "Equipment_ID";

                                Equipment equipment = new Equipment();
                                _tableId = equipment.GetEquipmentIdByUtilityId(utilityId);

                                EquipmentAttribute equipmentAttribute = new EquipmentAttribute();
                                _attrTableId = equipmentAttribute.GetIDByUtilityID(utilityId);
                            }
                            else if (objectAttributes.MasterObject == Constants.Dev_Loc)        // Device_Location table
                            {
                                entityType = Constants.DevLocTable;
                                masterTableName = Constants.DevLocTable;
                                attributeTableName = Constants.DevLocAttrTable;
                                uniqueIdCol = "Utility_ID";

                                DeviceLocation deviceLocation = new DeviceLocation();
                                _tableId = deviceLocation.GetDeviceLocIdByUtilityId(utilityId);

                                DevLocAttribute devLocAttribute = new DevLocAttribute();
                                _attrTableId = devLocAttribute.GetIDByUtilityID(utilityId);
                            }

                            if (objectAttributes.MasterObjectID.MasterObjectIDReq != null)
                            {

                                foreach (var masterObjectIDReqs in objectAttributes.MasterObjectID.MasterObjectIDReq)
                                {
                                    updateFields += "" + masterObjectIDReqs.AttName + " ='" + masterObjectIDReqs.AttValue + "',";
                                }
                            }
                            updateFields = updateFields.TrimEnd(',');

                            if (_tableId > 0)
                            {
                                using (SqlConnection connection = new SqlConnection(constr))
                                {
                                    connection.Open();
                                    query = "update " + masterTableName + " set " + updateFields + " where ID=" + _tableId;
                                    SqlCommand cmd = new SqlCommand(query, connection);
                                    cmd.ExecuteNonQuery();
                                    connection.Close();
                                }
                            }

                            //Consumer Attribute
                            List<string> tbl_attr_Names = attributeNames.GeAttributeNamesIDByEntity_Type(entityType);
                            string validFromDate = "";
                            string validToDate = "";
                            string payloadAttributeName = "";
                            string payloadAttributeValue = "";

                            foreach (var attributeValues in objectAttributes.AttributeVal)
                            {
                                bool isRejectedAttr = false;
                                payloadAttributeName = attributeValues.AttName;
                                payloadAttributeValue = attributeValues.AttValue;

                                int index = tbl_attr_Names.FindIndex(s => s.Contains(payloadAttributeName));

                                if (index >= 0)
                                {
                                    if (attributeValues.StartDate != null)
                                    {
                                        validFromDate = attributeValues.StartDate;
                                    }
                                    else
                                    {
                                        isRejected = true;
                                        isRejectedAttr = true;
                                        errorMessage = "StartDate missing in AttributeVal;";
                                    }

                                    if (attributeValues.EndDate != null)
                                    {
                                        validToDate = attributeValues.EndDate;
                                    }
                                    else
                                    {
                                        isRejected = true;
                                        isRejectedAttr = true;
                                        errorMessage = "EndDate missing in AttributeVal;";
                                    }

                                    int startDateCount = checkValidTimeSlice.CheckValidFromDate(validFromDate, attributeTableName, payloadAttributeName);
                                    if (startDateCount > 0)
                                    {
                                        isRejected = true;
                                        isRejectedAttr = true;
                                        errorMessage += "Time slice in AttributeVal;";
                                    }

                                    int sameTimeSlice = checkValidTimeSlice.CheckSameTimeSlice(validFromDate, validToDate, attributeTableName, payloadAttributeName);
                                    if (sameTimeSlice > 0)
                                    {
                                        isRejected = true;
                                        isRejectedAttr = true;
                                        errorMessage += "Same time slice in AttributeVal;";
                                    }
                                    if (!isRejectedAttr)
                                    {
                                        //insert Consumer_Attrib
                                        using (SqlConnection connection = new SqlConnection(constr))
                                        {
                                            connection.Open();
                                            query = "insert into  " + attributeTableName
                                               + " (" + uniqueIdCol + ",Attrib_Name, Attrib_Value, Valid_From, Valid_To, Created_by, Created_on) values('"
                                               + utilityId + "','" + payloadAttributeName + "','" + payloadAttributeValue + "','" + validFromDate + "','"
                                               + validToDate + "','Demo','" + DateTime.Now.ToString() + "')";
                                            SqlCommand cmd = new SqlCommand(query, connection);
                                            cmd.ExecuteNonQuery();
                                            connection.Close();
                                        }

                                    }
                                }
                            }

                        }
                    }
                }

                if (!isRejected)
                {
                    //Write to File
                    var xmlString = Serialize<SI_AMI_ConsumerMasterData_Out.DT_ConsumerMasterData_Req>(dT_ConsumerMasterData_Req);
                    WriteToLog(xmlString, "masterdata", Constants.CisAdaptorLog_Path);
                }
                else
                {
                    var xmlStrings = Serialize<SI_AMI_ConsumerMasterData_Out.DT_ConsumerMasterData_Req>(dT_ConsumerMasterData_Req);
                    WriteToLog(xmlStrings, "masterdata_rejected", Constants.Rejected_Path);
                }

                errorMessage = errorMessage.TrimEnd(',');
                //Exception_CIS_Sync
                exceptionSync.UUID = "";
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = "Demo";
                exceptionSync.ServiceName = Constants.ConsumerMasterData_Sync_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :ConsumerMasterData_Sync --- UUID :demoUUID --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                var xmlStrings = Serialize<SI_AMI_ConsumerMasterData_Out.DT_ConsumerMasterData_Req>(dT_ConsumerMasterData_Req);
                WriteToLog(xmlStrings, Constants.ConsumerMasterData_Sync_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void DeviceSendTextMessage(UtilitiesDeviceERPSmartMeterTextBulkNotification_Out.UtilsDvceERPSmrtMtrTxtBulkNotifMsg UtilitiesDeviceERPSmartMeterTextBulkNotification)
        {
            string uuid = "";
            DateTime plannedProcessingTime = DateTime.MinValue;
            string query = "";
            string createdBy = "";
            bool isRejected = false;
            string parentUUID = "";
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                #region CIS_ADAPTOR_LOG
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (UtilitiesDeviceERPSmartMeterTextBulkNotification.MessageHeader != null)
                {
                    if (UtilitiesDeviceERPSmartMeterTextBulkNotification.MessageHeader.SenderBusinessSystemID != null)
                    {
                        createdBy = UtilitiesDeviceERPSmartMeterTextBulkNotification.MessageHeader.SenderBusinessSystemID;
                    }
                    if (UtilitiesDeviceERPSmartMeterTextBulkNotification.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = UtilitiesDeviceERPSmartMeterTextBulkNotification.MessageHeader.UUID.Value;
                        parentUUID = UtilitiesDeviceERPSmartMeterTextBulkNotification.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    cISAdaptorParent.LogDateTime = UtilitiesDeviceERPSmartMeterTextBulkNotification.MessageHeader.CreationDateTime;
                    plannedProcessingTime = UtilitiesDeviceERPSmartMeterTextBulkNotification.MessageHeader.CreationDateTime;
                }
                else
                {
                    parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Child_Count = UtilitiesDeviceERPSmartMeterTextBulkNotification.UtilitiesDeviceERPSmartMeterTextNotificationMessage.Length;
                cISAdaptorParent.Request_Type = "DeviceSendTextMessage";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);
                #endregion

                string textMsg = "";
                string errorMessage = "";
                //string referenceID = "";
                int amiDevControlId = 0;
                int dataOriginID = 0;
                string utilityID = "";
                AMIDeviceControl aMIDeviceControl = new AMIDeviceControl();
                Equipment equipment = new Equipment();
                CategoryMaster categoryMaster = new CategoryMaster();
                ExceptionCISSync exceptionSync = new ExceptionCISSync();
                DataSourceMaster dataSourceMaster = new DataSourceMaster();
                EquipmentModel equipmentModel = new EquipmentModel();
                //Validation

                if (UtilitiesDeviceERPSmartMeterTextBulkNotification.UtilitiesDeviceERPSmartMeterTextNotificationMessage != null)
                {
                    int i = 1;
                    foreach (var UtilitiesDeviceERPSmartMeterTextBulkNotifications in UtilitiesDeviceERPSmartMeterTextBulkNotification.UtilitiesDeviceERPSmartMeterTextNotificationMessage)
                    {
                        string fields = "";
                        string values = "";
                        string updatefields = "";

                        #region CIS_ADAPTOR_CHILD_LOG
                        CISAdaptorChild cISAdaptorChild = new CISAdaptorChild();

                        if (UtilitiesDeviceERPSmartMeterTextBulkNotifications.MessageHeader != null)
                        {
                            if (UtilitiesDeviceERPSmartMeterTextBulkNotifications.MessageHeader.UUID != null)
                            {
                                cISAdaptorChild.UUID = UtilitiesDeviceERPSmartMeterTextBulkNotifications.MessageHeader.UUID.Value;
                                uuid = UtilitiesDeviceERPSmartMeterTextBulkNotifications.MessageHeader.UUID.Value;
                            }
                            else
                            {
                                uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                            }
                            cISAdaptorChild.LogDateTime = UtilitiesDeviceERPSmartMeterTextBulkNotifications.MessageHeader.CreationDateTime;
                        }
                        else
                        {
                            uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                        }
                        cISAdaptorChild.Parent_UUID = parentUUID;
                        cISAdaptorChild.Request_Type = "DeviceSendTextMessage";
                        cisAdaptorLog.CISAdaptorChildLog(cISAdaptorChild);
                        #endregion

                        if (UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.ID != null &&
                            UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.ID.Value != null)
                        {
                            // referenceID = UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.ID.Value;
                            utilityID = UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.ID.Value;

                            fields += "Refrence_ID,";
                            values += "'" + parentUUID + "',";
                            updatefields += "Refrence_ID =" + "'" + parentUUID + "',";
                        }
                        if (UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.SmartMeter != null)
                        {
                            if (UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID != null &&
                                UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value != null)
                            {
                                string hes_cd = UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.SmartMeter.UtilitiesAdvancedMeteringSystemID.Value;
                                int hesID = hes.GetHESIdByHESCode(hes_cd);
                                fields += "HES_ID,";
                                values += "'" + hesID + "',";
                                updatefields += "HES_ID =" + "'" + hesID + "',";
                            }
                        }

                        if (UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.Text.TextPlainContent.Text != null &&
                            UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.Text.TextPlainContent.Text.Value != null)
                        {
                            textMsg = UtilitiesDeviceERPSmartMeterTextBulkNotifications.UtilitiesDevice.Text.TextPlainContent.Text.Value;
                            fields += "Text_Message,";
                            values += "'" + textMsg + "',";
                            updatefields += "Text_Message = '" + textMsg + "',";
                        }
                        if (createdBy != null)
                        {
                            fields += "Created_by,";
                            values += "'" + createdBy + "',";
                            updatefields += "Changed_by = '" + createdBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                            dataOriginID = dataSourceMaster.GetDataSourceMasterIDByName(createdBy);
                            if (dataOriginID > 0)
                            {
                                fields += "Data_Origin_Code,";
                                values += "'" + dataOriginID + "',";
                                updatefields += "Data_Origin_Code = '" + dataOriginID + "',";
                            }
                            else
                            {
                                isRejected = true;
                                errorMessage += "SenderBusinessSystemID not found in DataSourceMaster table;";
                            }
                        }
                        if (!String.IsNullOrEmpty(uuid))
                        {
                            fields += "UUID,";
                            values += "'" + uuid + "',";
                            updatefields += "UUID = '" + uuid + "',";
                        }
                        if (plannedProcessingTime != DateTime.MinValue)
                        {
                            plannedProcessingTime = dateTimeValidator.CheckDate(plannedProcessingTime);
                            fields += "Planned_Processing_DateTime,";
                            values += "'" + plannedProcessingTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                            updatefields += "Planned_Processing_DateTime = '" + plannedProcessingTime.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        }

                        fields += "Request_Type,";
                        values += "'DeviceSendTextMessage',";
                        updatefields += "Request_Type = 'DeviceSendTextMessage',";

                        fields += "Created_on,";
                        values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                        equipmentModel = equipment.GetEquipmentByUtilityId(utilityID);
                        if (equipmentModel.ID <= 0)
                        {
                            //isRejected = true;
                            errorMessage += "Equipment ID not found in equipment table for payload :" + i + ";";
                        }
                        else
                        {
                            fields += "Equipment_ID,";
                            values += "'" + equipmentModel.ID + "',";
                            updatefields += "Equipment_ID = '" + equipmentModel.ID + "',";
                        }

                        if (!isRejected)
                        {
                            fields += "Request_Status,";
                            values += "1,";
                            updatefields += "Request_Status = 1,";
                        }
                        else
                        {
                            fields += "Request_Status,";
                            values += "7,";
                            updatefields += "Request_Status = 7,";
                        }

                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');
                        updatefields = updatefields.TrimEnd(',');

                        amiDevControlId = aMIDeviceControl.GetAMIDeviceControlIDByReferenceID(parentUUID);

                        if (amiDevControlId > 0)
                        {
                            using (SqlConnection connection = new SqlConnection(constr))
                            {
                                connection.Open();
                                query = "update AMIDeviceControl set " + updatefields + "where ID = '" + amiDevControlId + "'";
                                SqlCommand cmd = new SqlCommand(query, connection);
                                cmd.ExecuteNonQuery();
                                connection.Close();
                            }
                        }
                        else
                        {
                            using (SqlConnection connection = new SqlConnection(constr))
                            {
                                connection.Open();
                                query = "insert into AMIDeviceControl (" + fields + ") values(" + values + ")";
                                SqlCommand cmd = new SqlCommand(query, connection);
                                cmd.ExecuteNonQuery();
                                connection.Close();
                            }
                            amiDevControlId = aMIDeviceControl.GetAMIDeviceControlIDByReferenceID(parentUUID);
                        }

                        if (!isRejected)
                        {
                            string[] strlist = textMsg.Split('#');

                            DisplayAttributes displayAttributes = new DisplayAttributes();
                            displayAttributes.Attributes = new Attribute[strlist.Length];
                            Attribute attribute;
                            int index = 0;
                            foreach (var text in strlist)
                            {
                                attribute = new Attribute();
                                string[] attrNameVal = text.Split('~');

                                attribute.AttribName = attrNameVal[0];
                                attribute.AttribValue = attrNameVal[1];

                                displayAttributes.Attributes[index] = attribute;

                                index++;
                            }

                            HesInput hesInput = new HesInput();
                            hesInput.Action = "BalanceUpdate";
                            hesInput.DeviceID = equipmentModel.MeterID;
                            hesInput.RefID = parentUUID;
                            hesInput.ReplyURL = ConfigurationManager.AppSettings["hes_reply_url"];
                            hesInput.DemandResetFlag = "true";
                            hesInput.MRStartDate = DateTime.Now.AddDays(-1).ToString("MM/dd/yyyy HH:mm:ss");
                            hesInput.MREndDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                            hesInput.ParamName = new string[] { "KWH", "CKWH" };
                            hesInput.DisplayAttribute = displayAttributes;

                            string responseStatus = "";
                            HesConnector hesConnector = new HesConnector();
                            HesResponse hesResponse = new HesResponse();

                            //Modify after HES COnnector code completion
                            //-------------------------------------------
                            string hes_connector_type = ConfigurationManager.AppSettings["hes_connector_type"];
                            if (hes_connector_type == "rest")
                                responseStatus = hesConnector.Hes_Rest_Connector(hesInput);
                            else
                                hesResponse = hesConnector.Hes_Connector(hesInput);

                            if (hesResponse.Code == 0)
                            {
                                responseStatus = "11";
                            }

                            //if (hesResponse != null)
                            //{
                            //    using (SqlConnection connection = new SqlConnection(constr))
                            //    {
                            //        connection.Open();
                            //        query = "update AMIDeviceControl set Response_Status='" + hesResponse.Code + "',Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where ID = '" + amiDevControlId + "'";
                            //        SqlCommand cmd = new SqlCommand(query, connection);
                            //        cmd.ExecuteNonQuery();
                            //        connection.Close();
                            //    }
                            //}
                            //-------------------------------------------


                            //responseStatus = "11"; //Hard-coded for now.

                            aMIDeviceControl.AMIDeviceControlRequestStatusUpdate(amiDevControlId, 2, responseStatus);
                        }
                        i++;
                    }
                }

                if (!isRejected)
                {
                    //Write to log   
                    var xmlString = Serialize<UtilitiesDeviceERPSmartMeterTextBulkNotification_Out.UtilsDvceERPSmrtMtrTxtBulkNotifMsg>(UtilitiesDeviceERPSmartMeterTextBulkNotification);
                    WriteToLog(xmlString, parentUUID, Constants.CisAdaptorLog_Path);
                }
                else
                {
                    var xmlStrings = Serialize<UtilitiesDeviceERPSmartMeterTextBulkNotification_Out.UtilsDvceERPSmrtMtrTxtBulkNotifMsg>(UtilitiesDeviceERPSmartMeterTextBulkNotification);
                    WriteToLog(xmlStrings, parentUUID, Constants.Rejected_Path);
                }

                errorMessage = errorMessage.TrimEnd(',');
                //Exception_CIS_Sync
                exceptionSync.UUID = parentUUID;
                exceptionSync.Status = isRejected ? "Failure" : "Success";
                exceptionSync.Message = errorMessage;
                exceptionSync.CreatedBy = createdBy;
                exceptionSync.ServiceName = Constants.DeviceSendTextMessage_MethodName;
                Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :DeviceSendTextMessage --- UUID :" + parentUUID + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                var xmlStrings = Serialize<UtilitiesDeviceERPSmartMeterTextBulkNotification_Out.UtilsDvceERPSmrtMtrTxtBulkNotifMsg>(UtilitiesDeviceERPSmartMeterTextBulkNotification);
                WriteToLog(xmlStrings, Constants.DeviceSendTextMessage_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void MeterReading_Response_Bulk_Confirmation(MeterReadingDocumentERPResultBulkCreateConfirmation_Out.MtrRdngDocERPRsltBulkCrteConfMsg mtrRdngDocERPRsltBulkCrteConfMsg)
        {
            //Seq-4 text file
            string parentUUID = "";
            string query = "";
            string docId = "";
            string createdBy = "";
            string uuid = "";
            string utilitiesDeviceID = "";
            string errorMessage = "";
            bool isRejected = false;
            DateTime creationDatetime = DateTime.MinValue;
            Equipment equipment = new Equipment();
            DataSourceMaster dataSourceMaster = new DataSourceMaster();
            ReadReqResponse readReqResponse = new ReadReqResponse();
            ExceptionCISSync exceptionSync = new ExceptionCISSync();
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                #region CIS_ADAPTOR_LOG
                CisAdaptorLog cisAdaptorLog = new CisAdaptorLog();
                CISAdaptorParent cISAdaptorParent = new CISAdaptorParent();
                if (mtrRdngDocERPRsltBulkCrteConfMsg.MessageHeader != null)
                {
                    if (mtrRdngDocERPRsltBulkCrteConfMsg.MessageHeader.SenderBusinessSystemID != null)
                    {
                        createdBy = mtrRdngDocERPRsltBulkCrteConfMsg.MessageHeader.SenderBusinessSystemID;
                    }
                    if (mtrRdngDocERPRsltBulkCrteConfMsg.MessageHeader.UUID != null)
                    {
                        cISAdaptorParent.UUID = mtrRdngDocERPRsltBulkCrteConfMsg.MessageHeader.UUID.Value;
                        parentUUID = mtrRdngDocERPRsltBulkCrteConfMsg.MessageHeader.UUID.Value;
                    }
                    else
                    {
                        parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                    }
                    cISAdaptorParent.LogDateTime = mtrRdngDocERPRsltBulkCrteConfMsg.MessageHeader.CreationDateTime;
                }
                else
                {
                    parentUUID = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                }
                cISAdaptorParent.Child_Count = mtrRdngDocERPRsltBulkCrteConfMsg.MeterReadingDocumentERPResultCreateConfirmationMessage.Length;
                cISAdaptorParent.Request_Type = "MeterReading_Response_Bulk_Confirmation";
                cisAdaptorLog.CISAdaptorLog(cISAdaptorParent);
                #endregion

                if (mtrRdngDocERPRsltBulkCrteConfMsg.MeterReadingDocumentERPResultCreateConfirmationMessage != null)
                {
                    foreach (var MeterReadingDocumentERPResultCreateConfirmationMessage in mtrRdngDocERPRsltBulkCrteConfMsg.MeterReadingDocumentERPResultCreateConfirmationMessage)
                    {
                        #region CIS_ADAPTOR_CHILD_LOG
                        CISAdaptorChild cISAdaptorChild = new CISAdaptorChild();
                        if (MeterReadingDocumentERPResultCreateConfirmationMessage.MessageHeader != null)
                        {
                            if (MeterReadingDocumentERPResultCreateConfirmationMessage.MessageHeader.UUID != null)
                            {
                                cISAdaptorChild.UUID = MeterReadingDocumentERPResultCreateConfirmationMessage.MessageHeader.UUID.Value;
                                uuid = MeterReadingDocumentERPResultCreateConfirmationMessage.MessageHeader.UUID.Value;
                            }
                            else
                            {
                                uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                            }
                            cISAdaptorChild.LogDateTime = MeterReadingDocumentERPResultCreateConfirmationMessage.MessageHeader.CreationDateTime;
                            creationDatetime = MeterReadingDocumentERPResultCreateConfirmationMessage.MessageHeader.CreationDateTime;
                        }
                        else
                        {
                            uuid = DateTime.Now.ToString("yyyyMMddHHmmss") + "_error";
                        }
                        cISAdaptorChild.Parent_UUID = parentUUID;
                        cISAdaptorChild.Request_Type = "MeterReading_Response_Bulk_Confirmation";
                        cisAdaptorLog.CISAdaptorChildLog(cISAdaptorChild);
                        #endregion

                        string readReqResUpdateValues = "";
                        if (MeterReadingDocumentERPResultCreateConfirmationMessage.MeterReadingDocument.ID != null &&
                            MeterReadingDocumentERPResultCreateConfirmationMessage.MeterReadingDocument.ID.Value != null)
                        {
                            docId = MeterReadingDocumentERPResultCreateConfirmationMessage.MeterReadingDocument.ID.Value;
                        }
                        if (MeterReadingDocumentERPResultCreateConfirmationMessage.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID != null
                            && MeterReadingDocumentERPResultCreateConfirmationMessage.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID.Value != null)
                        {
                            utilitiesDeviceID = MeterReadingDocumentERPResultCreateConfirmationMessage.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID.Value;
                        }
                        if (!String.IsNullOrEmpty(createdBy))
                        {
                            readReqResUpdateValues += "Changed_by = '" + createdBy + "', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        }
                        readReqResUpdateValues += "Status = 5,";

                        readReqResUpdateValues = readReqResUpdateValues.TrimEnd(',');

                        using (SqlConnection connection = new SqlConnection(constr))
                        {
                            connection.Open();
                            query = "update ReadRequestResponse set " + readReqResUpdateValues + " where Utility_ID = '" + utilitiesDeviceID + "' and Status=4 and Doc_ID='" + docId + "'";
                            SqlCommand cmd = new SqlCommand(query, connection);
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }

                    }

                    //Write to file
                    var xmlString = Serialize<MeterReadingDocumentERPResultBulkCreateConfirmation_Out.MtrRdngDocERPRsltBulkCrteConfMsg>(mtrRdngDocERPRsltBulkCrteConfMsg);
                    WriteToLog(xmlString, parentUUID, Constants.CisAdaptorLog_Path);

                    //Exception_CIS_Sync
                    exceptionSync.UUID = uuid;
                    exceptionSync.Status = isRejected ? "Failure" : "Success";
                    exceptionSync.Message = errorMessage;
                    exceptionSync.CreatedBy = createdBy;
                    exceptionSync.ServiceName = Constants.MeterReading_Response_Bulk_Confirmation_MethodName;
                    Exception_CIS_Sync exception_CIS_Sync = new Exception_CIS_Sync();
                    exception_CIS_Sync.Exception_CIS_Sync_Entry(exceptionSync);
                    ////Update Status=5 for all records
                    //if (mtrRdngDocERPRsltBulkCrteConfMsg.MeterReadingDocumentERPResultCreateConfirmationMessage != null)
                    //{
                    //    foreach (var MeterReadingDocumentERPResultCreateConfirmationMessage in mtrRdngDocERPRsltBulkCrteConfMsg.MeterReadingDocumentERPResultCreateConfirmationMessage)
                    //    {
                    //        if (MeterReadingDocumentERPResultCreateConfirmationMessage.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID != null
                    //        && MeterReadingDocumentERPResultCreateConfirmationMessage.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID.Value != null)
                    //        {
                    //            utilitiesDeviceID = MeterReadingDocumentERPResultCreateConfirmationMessage.MeterReadingDocument.UtiltiesMeasurementTask.UtiltiesDevice.UtilitiesDeviceID.Value;
                    //            //equipmentId = equipment.GetEquipmentIdByUtilityId(utilitiesDeviceID);
                    //        }

                    //        using (SqlConnection connection = new SqlConnection(constr))
                    //        {
                    //            connection.Open();
                    //            string query = "update ReadRequestResponse set Status=5 where Utility_ID = '" + utilitiesDeviceID + "' and Status=4";
                    //            SqlCommand cmd = new SqlCommand(query, connection);
                    //            cmd.ExecuteNonQuery();
                    //            connection.Close();
                    //        }

                    //    }
                    //}

                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :MeterReading_Response_Bulk_Confirmation --- UUID :" + uuid + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                var xmlString = Serialize<MeterReadingDocumentERPResultBulkCreateConfirmation_Out.MtrRdngDocERPRsltBulkCrteConfMsg>(mtrRdngDocERPRsltBulkCrteConfMsg);
                WriteToLog(xmlString, Constants.MeterReading_Response_Bulk_Confirmation_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        [SoapDocumentMethod(OneWay = true)]
        [WebMethod]
        public void ConsumerData_Sync(SI_AMI_ConsumerDataSync_Out.DT_ConsumerDataSync_Req dT_ConsumerDataSync_Req)
        {
            ConfigMasterDataObject configMasterDataObject = new ConfigMasterDataObject();
            ConfigMasterObjectTables configMasterObjectTables = new ConfigMasterObjectTables();
            EquipmentAttribute equipmentAttribute = new EquipmentAttribute();
            string refID = "";
            string query = "";
            bool isRejected = false;
            string errorMessage = "";
            SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                foreach (var dT_ConsumerData in dT_ConsumerDataSync_Req.Request)
                {
                    refID = dT_ConsumerData.RefID;
                    string configValue = configMasterDataObject.GetConfigValuByName();
                    if (configValue == "N")
                    {
                        if (!String.IsNullOrEmpty(dT_ConsumerData.MasterObject))
                        {
                            ConfigMasterObjectTablesModel configMasterObjectTablesModel = configMasterObjectTables.GetMasterObject(dT_ConsumerData.MasterObject);
                            if (!String.IsNullOrEmpty(configMasterObjectTablesModel.Master_Header_Table))
                            {
                                if (configMasterObjectTablesModel.Master_Header_Table == dT_ConsumerData.MasterObject)
                                {
                                    int id = 0;
                                    if (!String.IsNullOrEmpty(dT_ConsumerData.MasterObjectID))
                                    {
                                        id = equipmentAttribute.GetIdByUtilityId(configMasterObjectTablesModel.Master_Header_Table, dT_ConsumerData.MasterObjectID);

                                        List<string> fieldNames = equipmentAttribute.GetFieldNameByTableName(configMasterObjectTablesModel.Master_Header_Table);

                                        if (dT_ConsumerData.MasterObjectIDReq != null)
                                        {
                                            var notMatchingRecords = dT_ConsumerData.MasterObjectIDReq.Where(m => !fieldNames.Contains(m.AttName)).ToList();

                                            ExceptionCISMasterSyncModel exceptionCISMasterSyncModel = new ExceptionCISMasterSyncModel();
                                            ExceptionCISMasterSync exceptionCISMasterSync = new ExceptionCISMasterSync();
                                            if (notMatchingRecords.Count > 0)
                                            {
                                                foreach (var records in notMatchingRecords)
                                                {
                                                    exceptionCISMasterSyncModel.PayloadRefID = refID;
                                                    exceptionCISMasterSyncModel.MasterObject = dT_ConsumerData.MasterObject;
                                                    exceptionCISMasterSyncModel.AttributeName = records.AttName;
                                                    exceptionCISMasterSyncModel.AttributeValue = records.AttValue;
                                                    exceptionCISMasterSyncModel.ErrorDescription = "";
                                                    exceptionCISMasterSyncModel.Created_by = "";
                                                    exceptionCISMasterSyncModel.Changed_by = "";

                                                    exceptionCISMasterSync.ExceptionCISMaster_Sync(exceptionCISMasterSyncModel);
                                                }

                                            }
                                            else
                                            {
                                                string updateEquipmentFields = "";
                                                foreach (var MasterObjectIDReq in dT_ConsumerData.MasterObjectIDReq)
                                                {
                                                    updateEquipmentFields += ""+ MasterObjectIDReq.AttName + " =" + "'" + MasterObjectIDReq.AttValue + "',";
                                                }
                                                updateEquipmentFields += "Changed_by = 'Test', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                                                updateEquipmentFields = updateEquipmentFields.TrimEnd(',');

                                                using (SqlConnection connection = new SqlConnection(constr))
                                                {
                                                    connection.Open();
                                                    query = "update " + configMasterObjectTablesModel.Master_Header_Table + " set " + updateEquipmentFields + " where Utility_ID='" + dT_ConsumerData.MasterObjectID + "'";
                                                    SqlCommand cmd = new SqlCommand(query, connection);
                                                    cmd.ExecuteNonQuery();
                                                    connection.Close();
                                                }
                                            }
                                        }

                                        if (dT_ConsumerData.AttributeVal != null)
                                        {
                                            List<string> attribNames = equipmentAttribute.GetAttribNamesByEntityType(configMasterObjectTablesModel.Master_Header_Table);

                                            var matchingRecords = dT_ConsumerData.AttributeVal.Where(m => attribNames.Contains(m.AttName)).ToList();
                                            if (matchingRecords.Count > 0)
                                            {
                                                foreach (var records in matchingRecords)
                                                {
                                                    AttribTables attribTables = equipmentAttribute.GetValidFromByParam(records.AttName, id);

                                                    DateTime validTo = DateTime.MinValue;
                                                    DateTime validFrom = DateTime.MinValue;
                                                    if (Convert.ToDateTime(records.EndDate) != DateTime.MinValue)
                                                    {
                                                        if (Convert.ToDateTime(records.EndDate) > attribTables.Valid_From)
                                                        {
                                                            validTo = Convert.ToDateTime(records.EndDate).AddDays(-1);
                                                        }
                                                        else if ((Convert.ToDateTime(records.EndDate) < attribTables.Valid_From))
                                                        {
                                                            errorMessage = "Invalid Date";
                                                            isRejected = true;
                                                        }
                                                        else if (Convert.ToDateTime(attribTables.Valid_From) == DateTime.MinValue)
                                                        {
                                                            validFrom = Convert.ToDateTime(records.EndDate);
                                                        }
                                                    }
                                                    if(!isRejected)
                                                    {
                                                        //update records
                                                        using (SqlConnection connection = new SqlConnection(constr))
                                                        {
                                                            connection.Open();
                                                            query = "update " + configMasterObjectTablesModel.Master_Attrib_Table + " set Valid_To='" + validTo.ToString("MM/dd/yyyy HH:mm:ss") + "'  where Attrib_ID=" + attribTables.Attrib_ID + "";
                                                            SqlCommand cmd = new SqlCommand(query, connection);
                                                            cmd.ExecuteNonQuery();
                                                            connection.Close();
                                                        }

                                                        //Insert Records
                                                        using (SqlConnection connection = new SqlConnection(constr))
                                                        {
                                                            connection.Open();
                                                            query = "insert into " + configMasterObjectTablesModel.Master_Attrib_Table + "(Attrib_ID,Valid_From,Attrib_Value) values("+ attribTables.Attrib_ID + "," + validFrom + ","+ records.AttValue + ")"; ;
                                                            SqlCommand cmd = new SqlCommand(query, connection);
                                                            cmd.ExecuteNonQuery();
                                                            connection.Close();
                                                        }
                                                    }                                                   
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (configValue == "Y")
                    {
                        List<string> fieldNames = equipmentAttribute.GetFieldNameByTableName(dT_ConsumerData.MasterObject);

                        var notMatchingRecords = dT_ConsumerData.MasterObjectIDReq.Where(m => !fieldNames.Contains(m.AttName)).ToList();

                        if (notMatchingRecords.Count > 0)
                        {
                            string updateEquipmentFields = "";
                            foreach (var MasterObjectIDReq in dT_ConsumerData.MasterObjectIDReq)
                            {
                                updateEquipmentFields += "" + MasterObjectIDReq.AttName + " =" + "'" + MasterObjectIDReq.AttValue + "',";
                            }

                            updateEquipmentFields += "Changed_by = 'Test', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                            updateEquipmentFields = updateEquipmentFields.TrimEnd(',');

                            using (SqlConnection connection = new SqlConnection(constr))
                            {
                                connection.Open();
                                query = "update Device_Location set " + updateEquipmentFields + " where Utility_ID='" + dT_ConsumerData.MasterObjectID + "'";
                                SqlCommand cmd = new SqlCommand(query, connection);
                                cmd.ExecuteNonQuery();
                                connection.Close();
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(dT_ConsumerData.MasterObject))
                            {
                                ConfigMasterObjectTablesModel configMasterObjectTablesModel = configMasterObjectTables.GetMasterObject(dT_ConsumerData.MasterObject);
                                if (!String.IsNullOrEmpty(configMasterObjectTablesModel.Master_Header_Table))
                                {
                                    if (configMasterObjectTablesModel.Master_Header_Table == dT_ConsumerData.MasterObject)
                                    {
                                        int id = 0;
                                        if (!String.IsNullOrEmpty(dT_ConsumerData.MasterObjectID))
                                        {
                                            id = equipmentAttribute.GetIdByUtilityId(configMasterObjectTablesModel.Master_Header_Table, dT_ConsumerData.MasterObjectID);

                                            List<string> fieldNamesSec = equipmentAttribute.GetFieldNameByTableName(configMasterObjectTablesModel.Master_Header_Table);

                                            if (dT_ConsumerData.MasterObjectIDReq != null)
                                            {
                                                var notMatchingRecordsSec = dT_ConsumerData.MasterObjectIDReq.Where(m => !fieldNamesSec.Contains(m.AttName)).ToList();

                                                ExceptionCISMasterSyncModel exceptionCISMasterSyncModel = new ExceptionCISMasterSyncModel();
                                                ExceptionCISMasterSync exceptionCISMasterSync = new ExceptionCISMasterSync();
                                                if (notMatchingRecordsSec.Count > 0)
                                                {
                                                    foreach (var records in notMatchingRecordsSec)
                                                    {
                                                        exceptionCISMasterSyncModel.PayloadRefID = refID;
                                                        exceptionCISMasterSyncModel.MasterObject = dT_ConsumerData.MasterObject;
                                                        exceptionCISMasterSyncModel.AttributeName = records.AttName;
                                                        exceptionCISMasterSyncModel.AttributeValue = records.AttValue;
                                                        exceptionCISMasterSyncModel.ErrorDescription = "";
                                                        exceptionCISMasterSyncModel.Created_by = "";
                                                        exceptionCISMasterSyncModel.Changed_by = "";

                                                        exceptionCISMasterSync.ExceptionCISMaster_Sync(exceptionCISMasterSyncModel);
                                                    }

                                                }
                                                else
                                                {
                                                    string updateEquipmentFields = "";
                                                    foreach (var MasterObjectIDReq in dT_ConsumerData.MasterObjectIDReq)
                                                    {
                                                        updateEquipmentFields += "" + MasterObjectIDReq.AttName + " =" + "'" + MasterObjectIDReq.AttValue + "',";
                                                    }
                                                    updateEquipmentFields += "Changed_by = 'Test', Changed_on = '" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                                                    updateEquipmentFields = updateEquipmentFields.TrimEnd(',');

                                                    using (SqlConnection connection = new SqlConnection(constr))
                                                    {
                                                        connection.Open();
                                                        query = "update " + configMasterObjectTablesModel.Master_Header_Table + " set " + updateEquipmentFields + " where Utility_ID='" + dT_ConsumerData.MasterObjectID + "'";
                                                        SqlCommand cmd = new SqlCommand(query, connection);
                                                        cmd.ExecuteNonQuery();
                                                        connection.Close();
                                                    }
                                                }
                                            }

                                            if (dT_ConsumerData.AttributeVal != null)
                                            {
                                                List<string> attribNames = equipmentAttribute.GetAttribNamesByEntityType(configMasterObjectTablesModel.Master_Header_Table);

                                                var matchingRecords = dT_ConsumerData.AttributeVal.Where(m => attribNames.Contains(m.AttName)).ToList();

                                                if (matchingRecords.Count > 0)
                                                {
                                                    foreach (var records in matchingRecords)
                                                    {
                                                        AttribTables attribTables = equipmentAttribute.GetValidFromByParam(records.AttName, id);

                                                        DateTime validTo = DateTime.MinValue;
                                                        DateTime validFrom = DateTime.MinValue;
                                                        if (Convert.ToDateTime(records.EndDate) != DateTime.MinValue)
                                                        {
                                                            if (Convert.ToDateTime(records.EndDate) > attribTables.Valid_From)
                                                            {
                                                                validTo = Convert.ToDateTime(records.EndDate).AddDays(-1);
                                                            }
                                                            else if ((Convert.ToDateTime(records.EndDate) < attribTables.Valid_From))
                                                            {
                                                                errorMessage = "Invalid Date";
                                                                isRejected = true;
                                                            }
                                                            else if (Convert.ToDateTime(attribTables.Valid_From) == DateTime.MinValue)
                                                            {
                                                                validFrom = Convert.ToDateTime(records.EndDate);
                                                            }
                                                        }
                                                        if (!isRejected)
                                                        {
                                                            //update records
                                                            using (SqlConnection connection = new SqlConnection(constr))
                                                            {
                                                                connection.Open();
                                                                query = "update " + configMasterObjectTablesModel.Master_Attrib_Table + " set Valid_To='" + validTo.ToString("MM/dd/yyyy HH:mm:ss") + "'  where Attrib_ID=" + attribTables.Attrib_ID + "";
                                                                SqlCommand cmd = new SqlCommand(query, connection);
                                                                cmd.ExecuteNonQuery();
                                                                connection.Close();
                                                            }

                                                            //Insert Records
                                                            using (SqlConnection connection = new SqlConnection(constr))
                                                            {
                                                                connection.Open();
                                                                query = "insert into " + configMasterObjectTablesModel.Master_Attrib_Table + "(Attrib_ID,Valid_From,Attrib_Value) values(" + attribTables.Attrib_ID + "," + validFrom + "," + records.AttValue + ")"; ;
                                                                SqlCommand cmd = new SqlCommand(query, connection);
                                                                cmd.ExecuteNonQuery();
                                                                connection.Close();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                var xmlStrings = Serialize<SI_AMI_ConsumerDataSync_Out.DT_ConsumerDataSync_Req>(dT_ConsumerDataSync_Req);
                WriteToLog(xmlStrings, refID, Constants.Rejected_Path);
            }
            catch (Exception ex)
            {
                string errorLog = "Method :ConsumerData_Sync --- UUID :" + refID + " --- Version :" + version + " --- Exception :" + ex.Message + " --- Query:" + query + "";
                logger.Error(errorLog);
                errorMessage += "Exception :" + ex.Message;
                var xmlString = Serialize<SI_AMI_ConsumerDataSync_Out.DT_ConsumerDataSync_Req>(dT_ConsumerDataSync_Req);
                WriteToLog(xmlString, Constants.RegisterChange_MethodName + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), Constants.Excetion_Path);
            }
        }

        private static string Serialize<T>(T dataToSerialize)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void WriteToLog(string xmlStrings, string uuid, string path)
        {
            string fileNames = uuid + ".txt";
            string payloadPath = ConfigurationManager.AppSettings["payloadPath"];
            string roots = payloadPath + path;

            if (!Directory.Exists(roots))
            {
                Directory.CreateDirectory(roots);
            }
            string subdirs = roots + DateTime.Today.ToString("yyyy-MM-dd");
            if (!Directory.Exists(subdirs))
            {
                Directory.CreateDirectory(subdirs);
            }
            File.WriteAllText(subdirs + "/" + fileNames, xmlStrings);
        }

        private static void SetLogFileName(string name)
        {
            log4net.Repository.ILoggerRepository RootRep;
            RootRep = LogManager.GetRepository(Assembly.GetCallingAssembly());

            XmlElement section = ConfigurationManager.GetSection("log4net") as XmlElement;

            XPathNavigator navigator = section.CreateNavigator();
            XPathNodeIterator nodes = navigator.Select("appender/file");
            foreach (XPathNavigator appender in nodes)
            {
                appender.MoveToAttribute("value", string.Empty);
                appender.SetValue(string.Format(appender.Value, name + "+"));
            }

            IXmlRepositoryConfigurator xmlCon = RootRep as IXmlRepositoryConfigurator;
            xmlCon.Configure(section);
        }

    }
}

