﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class DeviceLocationInfo
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();
        public int GetDevLocInfoIdByDeviceLocationID(int deviceLocID)
        {
            string query = "";
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    query = "select ID from Device_Location_Info where Device_Location_ID=" + deviceLocID + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = reader["ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ID"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetDevLocInfoIdByDeviceLocationID --- Version :" + version + " --- Exception :" + ex.Message+" Query :"+query+"";
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}