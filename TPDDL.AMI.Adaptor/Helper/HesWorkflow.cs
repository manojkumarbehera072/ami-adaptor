﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class HesWorkflow
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();
        public void HesWorkFlowEntry(HesWorkflowModel hesWorkflowModel)
        {
            string fields = "";
            string values = "";
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    if (hesWorkflowModel != null)
                    {
                        //validations
                        if (!String.IsNullOrEmpty(hesWorkflowModel.WorkflowType))
                        {
                            fields += "Workflow_Type,";
                            values += "'" + hesWorkflowModel.WorkflowType + "',";
                        }
                        if (!String.IsNullOrEmpty(hesWorkflowModel.RefrenceObjectType))
                        {
                            fields += "Refrence_Object_Type,";
                            values += "'" + hesWorkflowModel.RefrenceObjectType + "',";
                        }
                        if (!String.IsNullOrEmpty(hesWorkflowModel.ReferenceObjId))
                        {
                            fields += "Reference_Obj_Id,";
                            values += "'" + hesWorkflowModel.ReferenceObjId + "',";
                        }
                        if (!String.IsNullOrEmpty(hesWorkflowModel.WorkflowStatus))
                        {
                            fields += "Workflow_Status,";
                            values += "'" + hesWorkflowModel.WorkflowStatus + "',";
                        }
                        if (!String.IsNullOrEmpty(hesWorkflowModel.WorkflowResponseText))
                        {
                            fields += "Workflow_Response_Text,";
                            values += "'" + hesWorkflowModel.WorkflowResponseText + "',";
                        }
                        if (!String.IsNullOrEmpty(hesWorkflowModel.RequestSentTime))
                        {
                            fields += "Request_Sent_Time,";
                            values += "'" + hesWorkflowModel.RequestSentTime + "',";
                        }
                        if (!String.IsNullOrEmpty(hesWorkflowModel.ResponseReceivedTime))
                        {
                            fields += "Response_Received_Time,";
                            values += "'" + hesWorkflowModel.ResponseReceivedTime + "',";
                        }
                        if (hesWorkflowModel.HESId != 0)
                        {
                            fields += "HES_Id,";
                            values += "'" + hesWorkflowModel.HESId + "',";
                        }
                        if (!String.IsNullOrEmpty(hesWorkflowModel.DataSource))
                        {
                            fields += "Data_Source,";
                            values += "'" + hesWorkflowModel.DataSource + "',";
                        }
                        //if (hesWorkflowModel.RetryCount != null)
                        //{
                        //    fields += "Retry_Count,";
                        //    values += "'" + hesWorkflowModel.RetryCount + "',";
                        //}
                        if (hesWorkflowModel.CreatedOn != DateTime.MinValue)
                        {
                            fields += "Created_On,";
                            values += "'" + hesWorkflowModel.CreatedOn.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        }
                        if (!String.IsNullOrEmpty(hesWorkflowModel.CreatedBy))
                        {
                            fields += "Created_By,";
                            values += "'" + hesWorkflowModel.CreatedBy + "',";
                        }
                        
                        fields += "Retry_Count,";
                        values += "'0',";
                        //Remove end Comma 
                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');

                        connection.Open();
                        string query = "insert into HES_WORKFLOW (" + fields + ") values(" + values + ")";
                        SqlCommand cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();

                        connection.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                string errorLog = "Method :HesWorkFlowCreate --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

    }
}