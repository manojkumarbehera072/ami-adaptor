﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class Equipment_Reg_Link
    {
        AdaptorLog adaptorLog = new AdaptorLog();
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        public EquipmentRegLink GetEquipmentRegLinkByLogRegNoAndEquipmentId(string LogRegNo,int EquipmentId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    EquipmentRegLink equipmentRegLink = new EquipmentRegLink();
                    connection.Open();
                    string query = "select * from Equipment_Reg_Link  where  Log_Reg_no='" + LogRegNo + "' and Equipment_Id='" + EquipmentId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();                   
                    while (reader.Read())
                    {
                        equipmentRegLink.EquipmentId = Convert.ToInt32(reader["Equipment_Id"].ToString());
                        equipmentRegLink.LogRegNo = reader["Log_Reg_no"].ToString();

                    }
                    connection.Close();
                    return equipmentRegLink;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetEquipmentRegLinkByLogRegNoAndEquipmentId --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public EquipmentRegLink GetEquipmentRegLinkByLogRegNoAndRegCodeAndEquipmentId(string LogRegNo,string RegCode)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    EquipmentRegLink equipmentRegLink = new EquipmentRegLink();
                    connection.Open();
                    string query = "select * from Equipment_Reg_Link  where  Log_Reg_no='" + LogRegNo + "' and UOBIS='" + RegCode + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        equipmentRegLink.EquipmentId = Convert.ToInt32(reader["Equipment_Id"].ToString());
                        equipmentRegLink.LogRegNo = reader["Log_Reg_no"].ToString();
                        equipmentRegLink.UOBIS = reader["UOBIS"].ToString();
                    }
                    connection.Close();
                    return equipmentRegLink;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetEquipmentRegLinkByLogRegNoAndRegCodeAndEquipmentId --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}