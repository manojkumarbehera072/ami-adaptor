﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class DeviceLocation
    {
        AdaptorLog adaptorLog = new AdaptorLog();
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        public int GetDeviceLocIdByUtilityId(string utilityId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select id from Device_Location  where Utility_ID='" + utilityId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["id"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetDeviceLocIdByUtilityId --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
        //public void DeleteDeviceLocationById(int devLocUtilId)
        //{
        //    try
        //    {
        //        using (SqlConnection connection = new SqlConnection(constr))
        //        {
        //            connection.Open();
        //            string query = "delete from Device_Location where ID = '" + devLocUtilId + "'";
        //            SqlCommand cmd = new SqlCommand(query, connection);
        //            cmd.ExecuteNonQuery();
        //            connection.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}