﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class Material_Master
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        SqlDataReader reader;
        AdaptorLog adaptorLog = new AdaptorLog();

        public Material GetMaterialByMaterialUtilityId(string utilityId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    Material materialMaster = new Material();
                    connection.Open();
                    string query = "select * from Material_Master  where  Utility_ID='" + utilityId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        materialMaster.ID = Convert.ToInt32(reader["ID"].ToString());
                        materialMaster.UtilityId = reader["Utility_ID"].ToString();
                        materialMaster.Description = reader["Description"].ToString();
                        materialMaster.DeviceType = reader["Device_Type"].ToString();
                        materialMaster.MaterClass = reader["Meter_Type"].ToString();
                        materialMaster.DataSource = reader["Data_Source"].ToString();
                        materialMaster.CompId = String.IsNullOrEmpty(reader["COMP_ID"].ToString()) ? 0 : Convert.ToInt32(reader["COMP_ID"].ToString());
                    }
                    connection.Close();
                    return materialMaster;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetMaterialByMaterialUtilityId --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}