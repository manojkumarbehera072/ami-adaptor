﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class Constants
    {
        public const string Equipment = "Equipment";
        public const string Consumer = "Consumer";
        public const string Dev_Loc = "DeviceLocation";

        public const string EquipmentTable = "Equipment";
        public const string ConsumerTable = "Consumer";
        public const string DevLocTable = "Device_Location";

        public const string EquipmentAttrTable = "Equipment_Attrib";
        public const string ConsumerAttrTable = "Consumer_Attrib";
        public const string DevLocAttrTable = "Device_Location_Attrib";

        public const string FailureSeverityCode = "FailureSeverityCode";
        public const string SuccessSeverityCode = "SuccessSeverityCode";

        public const string EquipmentCreateSingle_MethodName = "EquipmentCreate_Single_Confirmation";
        public const string EquipmentCreate_Bulk_MethodName = "EquipmentCreate_Bulk_Confirmation";
        public const string EquipmentChange_Single_MethodName = "EquipmentChange_Single_Confirmation";
        public const string RegisterCreate_MethodName = "RegisterCreate_Confirmation";
        public const string RegisterChange_MethodName = "RegisterChange_Confirmation";
        public const string ReplicationRequest_MethodName = "ReplicationRequest";
        public const string DeviceLocation_Notification_MethodName = "DeviceLocation_Notification";
        public const string MeterConnectDisconnect_Request_Single_MethodName = "MeterConnectDisconnect_Request_Single";
        public const string MeterReading_Request_Bulk_MethodName = "MeterReading_Request_Bulk_Confirmation";
        public const string ConsumerMasterData_Sync_MethodName = "ConsumerMasterData_Sync";
        public const string DeviceSendTextMessage_MethodName = "DeviceSendTextMessage";
        public const string MeterReading_Response_Bulk_Confirmation_MethodName = "MeterReading_Response_Bulk_Confirmation";


        public const string Excetion_Path = "/CISAdapterLog/Exception/";
        public const string Rejected_Path = "/CISAdapterLog/Rejected/";
        public const string CisAdaptorLog_Path = "/PayLoad/";
    }
}