﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class IntervalData
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();

        public List<IntervalDataModel> GetIntervalDataByParam(int equipmentID, int paramID, DateTime starDateTime, DateTime endDateTime)
        {
            List<IntervalDataModel> intervalDataModelsList = new List<IntervalDataModel>();
            IntervalDataModel intervalData = new IntervalDataModel();
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from INTERVAL_DATA where EQUIPMENT_ID='" + equipmentID + "' and PARAM_ID='" + paramID + "' and INTERVAL_END_TIME between '" + starDateTime.ToString("yyyy-MM-dd") + "' and '" + endDateTime.ToString("yyyy-MM-dd") + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        intervalData.EquipmentId = reader["EQUIPMENT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EQUIPMENT_ID"].ToString());
                        intervalData.DeviceCode = reader["DEVICE_CODE"] == DBNull.Value ? "null" : reader["DEVICE_CODE"].ToString();
                        intervalData.ParamId = reader["PARAM_ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["PARAM_ID"].ToString());
                        intervalData.IntervalEndTime = reader["INTERVAL_END_TIME"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["INTERVAL_END_TIME"].ToString());
                        intervalData.ParamValue = reader["PARAM_VALUE"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["PARAM_VALUE"].ToString());
                        intervalData.InsertedBy = reader["INSERTED_BY"] == DBNull.Value ? "null" : reader["INSERTED_BY"].ToString();
                        intervalData.InsertedDate = reader["INSERTED_DATE"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["INSERTED_DATE"].ToString());
                        intervalData.Status = reader["STATUS"] == DBNull.Value ? "null" : reader["STATUS"].ToString();
                        intervalData.Flag = reader["FLAG"] == DBNull.Value ? 0 : Convert.ToInt32(reader["FLAG"].ToString());
                        intervalData.DataSource = reader["DATA_SOURCE"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DATA_SOURCE"].ToString());
                        intervalData.ChangedBy = reader["CHANGED_BY"] == DBNull.Value ? "null" : reader["CHANGED_BY"].ToString();
                        intervalData.ChangedDate = reader["CHANGED_DATE"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["CHANGED_DATE"].ToString());
                        intervalDataModelsList.Add(intervalData);
                    }
                    connection.Close();
                    return intervalDataModelsList;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetIntervalDataByParam --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }


    }
}