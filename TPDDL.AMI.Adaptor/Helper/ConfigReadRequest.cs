﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class ConfigReadRequest
    {
        SqlDataReader reader;
        SqlDataAdapter Adp;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();
        public string GetConfigValueByType()
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string result = "";
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select CONFIG_VALUE from CONFIG_READ_REQUEST where config_type = 'DetermineFirstBill'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = reader["CONFIG_VALUE"].ToString();
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetConfigValueByType --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

    }
}