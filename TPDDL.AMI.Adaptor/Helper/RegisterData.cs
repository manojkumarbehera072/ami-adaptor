﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class RegisterData
    {
        AdaptorLog adaptorLog = new AdaptorLog();
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        public List<RegisterDataModel> GetRegisterDataByParam(int equipmentID, int paramID, DateTime starDateTime, DateTime endDateTime)
        {
            List<RegisterDataModel> registerDataList = new List<RegisterDataModel>();
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    RegisterDataModel registerData = new RegisterDataModel();

                    connection.Open();
                    string query = "select * from REGISTER_DATA where EQUIPMENT_ID='" + equipmentID + "' and PARAM_ID='" + paramID + "' and (END_TIME between '" + starDateTime.ToString("yyyy-MM-dd") + "' and '" + endDateTime.ToString("yyyy-MM-dd") + "')";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        registerData.EquipmentId = reader["EQUIPMENT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["EQUIPMENT_ID"].ToString());
                        registerData.DeviceCode = reader["DEVICE_CODE"] == DBNull.Value ? "null" : reader["DEVICE_CODE"].ToString();
                        registerData.ParamId = reader["PARAM_ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["PARAM_ID"].ToString());
                        registerData.EndTime = reader["END_TIME"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["END_TIME"].ToString());
                        registerData.ParamValue = reader["PARAM_VALUE"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["PARAM_VALUE"].ToString());
                        registerData.DemandPeakTiime = reader["DEMAND_PEAK_TIME"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["DEMAND_PEAK_TIME"].ToString());
                        registerData.InsertedBy = reader["INSERTED_BY"] == DBNull.Value ? "null" : reader["INSERTED_BY"].ToString();
                        registerData.InsertedDate = reader["INSERTED_DATE"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["INSERTED_DATE"].ToString());
                        registerData.Status = reader["STATUS"] == DBNull.Value ? "null" : reader["STATUS"].ToString();
                        registerData.Flag = reader["FLAG"] == DBNull.Value ? 0 : Convert.ToInt32(reader["FLAG"].ToString());
                        registerData.DataSource = reader["DATA_SOURCE"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DATA_SOURCE"].ToString());
                        registerData.UpdatedBy = reader["UPDATED_BY"] == DBNull.Value ? "null" : reader["UPDATED_BY"].ToString();
                        registerData.UpdatedOn = reader["UPDATED_ON"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["UPDATED_ON"].ToString());
                        registerDataList.Add(registerData);
                    }
                    connection.Close();
                    return registerDataList;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetRegisterDataByParam --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public void DeleteRegisterRecord()
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "delete from REGISTER_DATA where STATUS='EST'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :DeleteRegisterRecord --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }

        }
    }
}