﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class AMIDeviceControl
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];
        AdaptorLog adaptorLog = new AdaptorLog();
        public int GetAMIDeviceControlIDByReferenceID(string referenceId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select ID from AMIDeviceControl where Refrence_ID='" + referenceId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["ID"].ToString());
                    }
                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetAMIDeviceControlIDByReferenceID --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public AMIDeviceControlModel GetAMIDeviceControlByReferenceID(string referenceId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                AMIDeviceControlModel aMIDeviceControlModel = new AMIDeviceControlModel();
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from AMIDeviceControl where Refrence_ID='" + referenceId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        aMIDeviceControlModel.ID = Convert.ToInt32(reader["ID"].ToString());
                        aMIDeviceControlModel.UUID = reader["UUID"].ToString();
                        aMIDeviceControlModel.CategoryCode = reader["Category_Code"].ToString();
                        aMIDeviceControlModel.ProcessingDateTime = Convert.ToDateTime( reader["Planned_Processing_DateTime"].ToString());
                        aMIDeviceControlModel.UtilitiesDeviceID = reader["Equipment_ID"].ToString();
                    }
                    connection.Close();
                    return aMIDeviceControlModel;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetAMIDeviceControlByReferenceID --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public void AMIDeviceControlRequestStatusUpdate(int id, int requestStatus, string responseStatus)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    //string query = "update AMIDeviceControl set Request_Status=" + requestStatus + ",HES_Ack_Status='" + responseStatus + "',HES_Ack_DateTime='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where ID = '" + id + "'";
                    string query = "update AMIDeviceControl set HES_Ack_Status='" + responseStatus + "',HES_Ack_DateTime='" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "' where ID = '" + id + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :AMIDeviceControlRequestStatusUpdate --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}