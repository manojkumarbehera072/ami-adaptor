﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class ExceptionCISMasterSync
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();
        public void ExceptionCISMaster_Sync(ExceptionCISMasterSyncModel exceptionCISMasterSyncModel)
        {
            string fields = "";
            string values = "";
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    if (exceptionCISMasterSyncModel != null)
                    {
                        //validations
                        if (!String.IsNullOrEmpty(exceptionCISMasterSyncModel.PayloadRefID))
                        {
                            fields += "PayloadRefID,";
                            values += "'" + exceptionCISMasterSyncModel.PayloadRefID + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISMasterSyncModel.MasterObject))
                        {
                            fields += "MasterObject,";
                            values += "'" + exceptionCISMasterSyncModel.MasterObject + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISMasterSyncModel.AttributeName))
                        {
                            fields += "AttributeName,";
                            values += "'" + exceptionCISMasterSyncModel.AttributeName + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISMasterSyncModel.AttributeValue))
                        {
                            fields += "AttributeValue,";
                            values += "'" + exceptionCISMasterSyncModel.AttributeValue + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISMasterSyncModel.ErrorDescription))
                        {
                            fields += "ErrorDescription,";
                            values += "'" + exceptionCISMasterSyncModel.ErrorDescription + "',";
                        }
                        if (exceptionCISMasterSyncModel.Created_on != DateTime.MinValue)
                        {
                            fields += "Created_on,";
                            values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISMasterSyncModel.Created_by))
                        {
                            fields += "Created_by,";
                            values += "'" + exceptionCISMasterSyncModel.Created_by + "',";
                        }
                        if (exceptionCISMasterSyncModel.Changed_on != DateTime.MinValue)
                        {
                            fields += "Changed_on,";
                            values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISMasterSyncModel.Changed_by))
                        {
                            fields += "Changed_by,";
                            values += "'" + exceptionCISMasterSyncModel.Changed_by + "',";
                        }

                        //Remove end Comma 
                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');

                        connection.Open();
                        string query = "insert into Exception_CIS_Master_Sync (" + fields + ") values(" + values + ")";
                        SqlCommand cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                string errorLog = "Method :ExceptionCISMaster_Sync --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}