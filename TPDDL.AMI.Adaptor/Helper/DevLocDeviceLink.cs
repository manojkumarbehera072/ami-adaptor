﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class DevLocDeviceLink
    {
        AdaptorLog adaptorLog = new AdaptorLog();
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        public int GetDeviceLocDevLinkIdByParam(int equipmentId, int deviceLocationId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select id from DevLoc_Device_Link  where EquipmentId='" + equipmentId + "' and DeviceLocationId = '" + deviceLocationId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["id"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetDeviceLocDevLinkIdByParam --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public List<DevLocLinkModel> CheckValidFromDate(string validFromDate, int equipmentId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            List<DevLocLinkModel> devLocLinkList = new List<DevLocLinkModel>();

            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    DevLocLinkModel devLocLinkModel = new DevLocLinkModel();

                    int result = 0;
                    connection.Open();
                    string query = " select * from DevLoc_Device_Link where EquipmentId = "+ equipmentId + " and (ValidToDate is null or ValidToDate = '9999-12-31 00:00:00.000')";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        devLocLinkModel.ID = Convert.ToInt32(reader["ID"].ToString());
                        devLocLinkModel.EquipmentId = Convert.ToInt32(reader["EquipmentId"].ToString());
                        devLocLinkModel.DevicelocationId = Convert.ToInt32(reader["DeviceLocationId"].ToString());
                        devLocLinkModel.ValidFromDate = Convert.ToDateTime(reader["ValidFromDate"].ToString());
                        devLocLinkModel.ValidToDate = Convert.ToDateTime(reader["ValidToDate"].ToString());

                        //result = Convert.ToInt32(reader["count"].ToString());

                        devLocLinkList.Add(devLocLinkModel);
                    }

                    connection.Close();
                    return devLocLinkList;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetDeviceLocDevLinkIdByParam --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public List<DevLocLinkModel> CheckValidToDate(string validToDate, int equipmentId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            List<DevLocLinkModel> devLocLinkList = new List<DevLocLinkModel>();

            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    DevLocLinkModel devLocLinkModel = new DevLocLinkModel();

                    int result = 0;
                    connection.Open();
                    string query = " select * from DevLoc_Device_Link where EquipmentId = " + equipmentId + " and (ValidToDate is not null)";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        devLocLinkModel.ID = Convert.ToInt32(reader["ID"].ToString());
                        devLocLinkModel.EquipmentId = Convert.ToInt32(reader["EquipmentId"].ToString());
                        devLocLinkModel.DevicelocationId = Convert.ToInt32(reader["DeviceLocationId"].ToString());
                        devLocLinkModel.ValidFromDate = Convert.ToDateTime(reader["ValidFromDate"].ToString());
                        devLocLinkModel.ValidToDate = Convert.ToDateTime(reader["ValidToDate"].ToString());

                        devLocLinkList.Add(devLocLinkModel);
                    }

                    connection.Close();
                    return devLocLinkList;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :CheckValidToDate --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public int CheckEquipmentIdAndDeviceLocId(string validFromDate,string validToDate)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select count(1) as count from DevLoc_Device_Link where ValidFromDate between '"+ validFromDate + "' and '"+ validToDate + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["count"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :CheckEquipmentIdAndDeviceLocId --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public void DeleteRecord(int deviceLocId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "delete from DevLoc_Device_Link where Utility_ID = '" + deviceLocId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :DeleteRecord --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public DateTime GetValidFromDateByEquipmentID(int equipmentID)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            DateTime vaidFromDate = new DateTime();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select ValidFromDate from DevLoc_Device_Link where Equipmentid="+equipmentID+" and  (ValidToDate is null or ValidToDate='9999-12-31');";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        vaidFromDate = Convert.ToDateTime(reader["ValidFromDate"].ToString());
                    }
                    connection.Close();
                    return vaidFromDate;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetValidFromDateByEquipmentID --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}