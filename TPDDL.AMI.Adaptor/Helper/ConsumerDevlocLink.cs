﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class ConsumerDevlocLink
    {
        AdaptorLog adaptorLog = new AdaptorLog();
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        public List<string> GetConsumerIDByDevLocId(int devLocId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            List<string> consumerIds = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select ConsumerID from ConsumerDevLocLink where DeviceLocationId='" + devLocId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        //consumerDevLocLinkModel.ConsumerID = reader["Consumer_ID"].ToString();
                        consumerIds.Add(reader["ConsumerID"].ToString());
                    }

                    connection.Close();
                    return consumerIds;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetConsumerIDByDevLocId --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }

        }

        public int GetConsumerDevLocIDByParam(int devLocId, int consumerID)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            List<string> consumerIds = new List<string>();
            int ConsumerDevLocId = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select ID from ConsumerDevLocLink where DeviceLocationId='" + devLocId + "' and ConsumerID=" + consumerID;
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        //consumerDevLocLinkModel.ConsumerID = reader["Consumer_ID"].ToString();
                        //consumerIds.Add(reader["ID"].ToString());
                        ConsumerDevLocId = Convert.ToInt32(reader["ID"]);
                    }

                    connection.Close();
                    return ConsumerDevLocId;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetConsumerIDByParam --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }

        }

        public int CheckValidFromDate(string validFromDate)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = " select count(1) as count from ConsumerDevLocLink where ValidFromDate > '" + validFromDate + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["count"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :CheckValidFromDate --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public int CheckSameTimeSlice(string validFromDate, string validToDate)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select count(1) as count from ConsumerDevLocLink where ValidFromDate between '" + validFromDate + "' and '" + validToDate + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["count"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :CheckSameTimeSlice --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}