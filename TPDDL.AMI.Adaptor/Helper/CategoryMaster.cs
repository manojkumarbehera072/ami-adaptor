﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class CategoryMaster
    {
        AdaptorLog adaptorLog = new AdaptorLog();
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        public CategoryCodeMaster GetCategoryMasterByCategoryCode(string categoryCode)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                CategoryCodeMaster categoryCodeMaster = new CategoryCodeMaster(); 
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from CategoryCodeMaster where Category_Code='" + categoryCode + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        categoryCodeMaster.ID = Convert.ToInt32(reader["ID"].ToString());
                        categoryCodeMaster.Category_Name = reader["Category_Name"].ToString();
                    }

                    connection.Close();
                    return categoryCodeMaster;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetCategoryMasterByCategoryCode --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

    }
}