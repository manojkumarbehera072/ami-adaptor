﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class ConfigMasterData
    {
        SqlDataReader reader;
        SqlDataAdapter Adp;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();
        public string GetConfigValueByMDObjectAndConfigName()
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            string result = "";
            string query = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    query = "select CONFIG_VALUE  from CONFIG_MASTER_DATA_OBJECT where MD_OBJECT_NAME='DeviceLocation' and CONFIG_NAME='DevLocEqualConsumer'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = reader["CONFIG_VALUE"] == DBNull.Value ? "null" : reader["CONFIG_VALUE"].ToString();
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetConfigValueByMDObjectAndConfigName --- Version :" + version + " --- Exception :" + ex.Message + " Query :" + query + ""; 
                logger.Error(errorLog);
                throw ex;
            }
        }

    }
}