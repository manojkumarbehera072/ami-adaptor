﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class ConfigMasterObjectTables
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];
        AdaptorLog adaptorLog = new AdaptorLog();
        public ConfigMasterObjectTablesModel GetMasterObject(string masterObj)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            ConfigMasterObjectTablesModel configMasterObjectTablesModel = new ConfigMasterObjectTablesModel();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT Master_ojbect,Master_Attrib_Table FROM Config_Master_Object_Tables where Master_ojbect = '" + masterObj + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        configMasterObjectTablesModel.Master_Header_Table = reader["Master_ojbect"].ToString();
                        configMasterObjectTablesModel.Master_Attrib_Table = reader["Master_Attrib_Table"].ToString();
                    }
                    connection.Close();
                    return configMasterObjectTablesModel;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetMasterObject --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }

}