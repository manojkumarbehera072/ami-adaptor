﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class HESRequestLog
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();

        public void HESRequestLog_Entry(HesInput hesInput)
        {
            string fields = "";
            string values = "";
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    if (hesInput != null)
                    {
                        //validations
                        if (!String.IsNullOrEmpty(hesInput.RefID))
                        {
                            fields += "RefID,";
                            values += "'" + hesInput.RefID + "',";
                        }
                        if (!String.IsNullOrEmpty(hesInput.Action))
                        {
                            fields += "Action,";
                            values += "'" + hesInput.Action + "',";
                        }
                        if (!String.IsNullOrEmpty(hesInput.DeviceID))
                        {
                            fields += "DeviceID,";
                            values += "'" + hesInput.DeviceID + "',";
                        }
                        if (!String.IsNullOrEmpty(hesInput.ReplyURL))
                        {
                            fields += "ReplyURL,";
                            values += "'" + hesInput.ReplyURL + "',";
                        }
                        if (!String.IsNullOrEmpty(hesInput.MRStartDate))
                        {
                            fields += "MRStartDate,";
                            values += "'" + hesInput.MRStartDate + "',";
                        }
                        if (!String.IsNullOrEmpty(hesInput.MREndDate))
                        {
                            fields += "MREndDate,";
                            values += "'" + hesInput.MREndDate + "',";
                        }
                        if (hesInput.ParamName!=null)
                        {
                            string paramNames = "";
                            foreach(var paramName in hesInput.ParamName)
                            {
                                paramNames +=  paramName + ",";
                            }
                            paramNames = paramNames.TrimEnd(',');
                            fields += "ParamName,";
                            values += "'" + paramNames + "',";
                        }
                        if (!String.IsNullOrEmpty(hesInput.DemandResetFlag))
                        {
                            fields += "DemandResetFlag,";
                            values += "'" + hesInput.DemandResetFlag + "',";
                        }
                        if (hesInput.DisplayAttribute!=null)
                        {
                            string attrNames = "";
                            string attrValues = "";
                            foreach (var attributes in hesInput.DisplayAttribute.Attributes)
                            {
                                attrNames += attributes.AttribName + ",";
                                attrValues += attributes.AttribValue + ",";
                            }
                            fields += "DisplayAttributes,";
                            values += "'AttribName :"+ attrNames + "'--AttribVal :" + attrValues + "',";
                        }
                        fields += "Created_on,";
                        values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                        //Remove end Comma 
                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');

                        connection.Open();
                        string query = "insert into HESRequestLog (" + fields + ") values(" + values + ")";
                        SqlCommand cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();

                        connection.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                string errorLog = "Method :HESRequestLog_Entry --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}