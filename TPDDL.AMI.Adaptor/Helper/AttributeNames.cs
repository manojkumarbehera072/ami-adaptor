﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class AttributeNames
    {
        AdaptorLog adaptorLog = new AdaptorLog();
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        public List<string> GeAttributeNamesIDByEntity_Type(string entityType)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            List<string> attributeNames = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select Attrib_Name from Attrib_Names where Entity_Type='" + entityType + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        //consumerDevLocLinkModel.ConsumerID = reader["Consumer_ID"].ToString();
                        attributeNames.Add(reader["Attrib_Name"].ToString());
                    }
                    connection.Close();
                    return attributeNames;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GeAttributeNamesIDByEntity_Type --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }

        }

    }
}