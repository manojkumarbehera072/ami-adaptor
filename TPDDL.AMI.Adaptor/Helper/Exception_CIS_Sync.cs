﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class Exception_CIS_Sync
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();

        public void Exception_CIS_Sync_Entry(ExceptionCISSync exceptionCISSync)
        {
            string fields = "";
            string values = "";
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    if (exceptionCISSync != null)
                    {
                        //validations
                        if (!String.IsNullOrEmpty(exceptionCISSync.UUID))
                        {
                            fields += "UUID,";
                            values += "'" + exceptionCISSync.UUID + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISSync.Status))
                        {
                            fields += "Status,";
                            values += "'" + exceptionCISSync.Status + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISSync.Message))
                        {
                            fields += "Message,";
                            values += "'" + exceptionCISSync.Message + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISSync.CreatedBy))
                        {
                            fields += "Created_by,";
                            values += "'" + exceptionCISSync.CreatedBy + "',";
                        }
                        if (!String.IsNullOrEmpty(exceptionCISSync.ServiceName))
                        {
                            fields += "Service_Name,";
                            values += "'" + exceptionCISSync.ServiceName + "',";
                        }
                        fields += "Created_on,";
                        values += "'" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "',";

                        //Remove end Comma 
                        fields = fields.TrimEnd(',');
                        values = values.TrimEnd(',');

                        connection.Open();
                        string query = "insert into Exception_CIS_Sync (" + fields + ") values(" + values + ")";
                        SqlCommand cmd = new SqlCommand(query, connection);
                        cmd.ExecuteNonQuery();

                        connection.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                string errorLog = "Method :Exception_CIS_Sync_Entry --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}