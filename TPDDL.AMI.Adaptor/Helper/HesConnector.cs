﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Configuration;
using System.Xml;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class HesConnector
    {
        public static string hes_service_url = ConfigurationManager.AppSettings["hes_service_url"];
        public static string hes_rest_service_url = ConfigurationManager.AppSettings["hes_rest_service_url"];
        public static string hes_header_domain = ConfigurationManager.AppSettings["hes_header_domain"];
        string result = "";
        HESRequestLog hESRequestLog = new HESRequestLog();
        public HesResponse Hes_Connector(HesInput hesInput)
        {
            HesResponse hesResponse = new HesResponse();
            var client = new RestClient(hes_service_url);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Host", hes_header_domain);
            request.AddHeader("Content-Type", "text/xml; charset=utf-8");
            request.AddHeader("SOAPAction", "\"http://tpddl.org/" + hesInput.Action + "\"");
            var body = @"<?xml version=""1.0"" encoding=""utf-8""?>
            <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                <soap:Body>
                    <" + hesInput.Action + @" xmlns=""http://tpddl.org/"">
                        " + BuildInput(hesInput) + @"
                    </" + hesInput.Action + @">
                </soap:Body>
            </soap:Envelope>";
            request.AddParameter("text/xml; charset=utf-8", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            hESRequestLog.HESRequestLog_Entry(hesInput);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response.Content);
            string jsonText = JsonConvert.SerializeXmlNode(doc);
            var data = JsonConvert.DeserializeObject<Root>(jsonText);

            if (data.SoapEnvelope.SoapBody.ConnectResponse != null)
            {
                hesResponse.Status = data.SoapEnvelope.SoapBody.ConnectResponse.ConnectResult.Status;
                hesResponse.Code = data.SoapEnvelope.SoapBody.ConnectResponse.ConnectResult.Code;
            }
            else if (data.SoapEnvelope.SoapBody.DisconnectResponse != null)
            {
                hesResponse.Status = data.SoapEnvelope.SoapBody.DisconnectResponse.DisconnectResult.Status;
                hesResponse.Code = data.SoapEnvelope.SoapBody.DisconnectResponse.DisconnectResult.Code;
            }
            else if (data.SoapEnvelope.SoapBody.RelayStatusResponse != null)
            {
                hesResponse.Status = data.SoapEnvelope.SoapBody.RelayStatusResponse.RelayStatusResult.Status;
                hesResponse.Code = data.SoapEnvelope.SoapBody.RelayStatusResponse.RelayStatusResult.Code;
            }
            else if (data.SoapEnvelope.SoapBody.ODRResponse != null)
            {
                hesResponse.Status = data.SoapEnvelope.SoapBody.ODRResponse.ODRResult.Status;
                hesResponse.Code = data.SoapEnvelope.SoapBody.ODRResponse.ODRResult.Code;
            }
            else if (data.SoapEnvelope.SoapBody.PingResponse != null)
            {
                hesResponse.Status = data.SoapEnvelope.SoapBody.PingResponse.PingResult.Status;
                hesResponse.Code = data.SoapEnvelope.SoapBody.PingResponse.PingResult.Code;
            }
            else if (data.SoapEnvelope.SoapBody.DemandResetResponse != null)
            {
                hesResponse.Status = data.SoapEnvelope.SoapBody.DemandResetResponse.DemandResetResult.Status;
                hesResponse.Code = data.SoapEnvelope.SoapBody.DemandResetResponse.DemandResetResult.Code;
            }
            else if (data.SoapEnvelope.SoapBody.BalanceUpdateResponse != null)
            {
                hesResponse.Status = data.SoapEnvelope.SoapBody.BalanceUpdateResponse.BalanceUpdateResult.Status;
                hesResponse.Code = data.SoapEnvelope.SoapBody.BalanceUpdateResponse.BalanceUpdateResult.Code;
            }
            return hesResponse;
        }

        public string Hes_Rest_Connector(HesInput hesInput)
        {
            var client = new RestClient(hes_rest_service_url + hesInput.Action);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            
            var body = JsonConvert.SerializeObject(hesInput);
            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (hesInput.Action == "Connect")
            {
                var res = JsonConvert.DeserializeObject<ConnectResult>(response.Content);
                result = res.Status;
            }
            if (hesInput.Action == "Disconnect")
            {
                var res = JsonConvert.DeserializeObject<DisconnectResult>(response.Content);
                result = res.Status;
            }
            if (hesInput.Action == "RelayStatus")
            {
                var res = JsonConvert.DeserializeObject<RelayStatusResult>(response.Content);
                result = res.Status;
            }
            if (hesInput.Action == "ODR")
            {
                var res = JsonConvert.DeserializeObject<ODRResult>(response.Content);
                result = res.Status;
            }
            if (hesInput.Action == "Ping")
            {
                var res = JsonConvert.DeserializeObject<PingResult>(response.Content);
                result = res.Status;
            }
            if (hesInput.Action == "DemandReset")
            {
                var res = JsonConvert.DeserializeObject<DemandResetResult>(response.Content);
                result = res.Status;
            }
            if (hesInput.Action == "BalanceUpdate")
            {
                var res = JsonConvert.DeserializeObject<BalanceUpdateResult>(response.Content);
                result = res.Status;
            }

            return result;
        }

        private string BuildInput(HesInput hesInput)
        {
            string inputBuilt = "";

            switch (hesInput.Action)
            {
                case "Disconnect":

                    inputBuilt = @"<hesInput>
                                <RefID>" + hesInput.RefID + @"</RefID>
                                <Action>" + hesInput.Action + @"</Action>
                                <DeviceID>" + hesInput.DeviceID + @"</DeviceID>
                                <ReplyURL>" + hesInput.ReplyURL + @"</ReplyURL>
                            </hesInput>";
                    break;
                case "Connect":
                    
                    inputBuilt = @"<hesInput>
                                <RefID>" + hesInput.RefID + @"</RefID>
                                <Action>" + hesInput.Action + @"</Action>
                                <DeviceID>" + hesInput.DeviceID + @"</DeviceID>
                                <ReplyURL>" + hesInput.ReplyURL + @"</ReplyURL>
                            </hesInput>";
                    break;
                case "Ping":
                    
                    inputBuilt = @"<hesInput>
                                <RefID>" + hesInput.RefID + @"</RefID>
                                <Action>" + hesInput.Action + @"</Action>
                                <DeviceID>" + hesInput.DeviceID + @"</DeviceID>
                                <ReplyURL>" + hesInput.ReplyURL + @"</ReplyURL>
                            </hesInput>";
                    break;
                case "RelayStatus":
                    
                    inputBuilt = @"<hesInput>
                                <RefID>" + hesInput.RefID + @"</RefID>
                                <Action>" + hesInput.Action + @"</Action>
                                <DeviceID>" + hesInput.DeviceID + @"</DeviceID>
                                <ReplyURL>" + hesInput.ReplyURL + @"</ReplyURL>
                            </hesInput>";
                    break;
                case "ODR":
                    
                    inputBuilt = @"<odrInput>
                                <RefID>" + hesInput.RefID + @"</RefID>
                                <Action>" + hesInput.Action + @"</Action>
                                <DeviceID>" + hesInput.DeviceID + @"</DeviceID>
                                <ReplyURL>" + hesInput.ReplyURL + @"</ReplyURL>
                                <MRStartDate>" + hesInput.MRStartDate + @"</MRStartDate>
                                <MREndDate>" + hesInput.MREndDate + @"</MREndDate>
                                <ParamName>" + hesInput.ParamName[0] + @"</ParamName>
                            </odrInput>";
                    break;
                case "DemandReset":
                    
                    inputBuilt = @"<demandResetInput>
                                <RefID>" + hesInput.RefID + @"</RefID>
                                <Action>" + hesInput.Action + @"</Action>
                                <DeviceID>" + hesInput.DeviceID + @"</DeviceID>
                                <ReplyURL>" + hesInput.ReplyURL + @"</ReplyURL>
                                <DemandResetFlag>" + hesInput.DemandResetFlag + @"</DemandResetFlag>
                            </demandResetInput>";
                    break;
                case "BalanceUpdate":

                    inputBuilt = @"<balanceUpdateInput>
                                <RefID>" + hesInput.RefID + @"</RefID>
                                <Action>" + hesInput.Action + @"</Action>
                                <DeviceID>" + hesInput.DeviceID + @"</DeviceID>
                                <ReplyURL>" + hesInput.ReplyURL + @"</ReplyURL>
                                <DisplayAttributes><Attributes>" +

                                BalanceUpdateAttributesArray(hesInput.DisplayAttribute)
                                
                                + @"</Attributes></DisplayAttributes>
                            </balanceUpdateInput>";
                    break;
                default:
                    inputBuilt = "Error";
                    break;
            }

            return inputBuilt;
        }

        private string BalanceUpdateAttributesArray(DisplayAttributes displayAttributes)
        {
            string text = "";

            foreach(var attr in displayAttributes.Attributes)
            {
                text += @"<Attrib>
                            <AttribName>" + attr.AttribName + @"</AttribName>
                            <AttribValue>" + attr.AttribValue + @"</AttribValue>
                        </Attrib>";
            }

            return text;
        }

    }
}