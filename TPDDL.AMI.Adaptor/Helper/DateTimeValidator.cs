﻿using log4net;
using System;
using System.Configuration;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class DateTimeValidator
    {
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();
        public DateTime CheckDate(DateTime dateTime)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                DateTime newDateTime = DateTime.MinValue;
                if (dateTime.Kind.ToString() == "Utc")
                {
                    newDateTime = dateTime.AddHours(5).AddMinutes(30);
                }
                else
                {
                    newDateTime = dateTime;
                }
                return newDateTime;
            }
            catch (Exception ex)
            {
                string errorLog = "Method :CheckDate --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }           
        }
    }
}