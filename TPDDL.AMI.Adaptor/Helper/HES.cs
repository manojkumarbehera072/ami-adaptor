﻿using log4net;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Helper;

namespace TPDDL.AMI.Adaptor
{
    public class HES
    {
        
        SqlDataReader reader;
        SqlDataAdapter Adp;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        AdaptorLog adaptorLog = new AdaptorLog();

        public int GetHESIdByHESCode(string hesName)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select id from hes_master where hes_cd='" + hesName + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["id"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetHESIdByHESCode --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public DataTable GetSAPAMIUrl(string servicename)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select ServiceUrl,UserId,Password from InterfaceResponseUrls where upper(ServiceName)=upper('" + servicename + "') and Active='Y'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    DataTable dturl = new DataTable();
                    dturl.Clear();
                    Adp = new SqlDataAdapter();
                    Adp.SelectCommand = cmd;
                    Adp.Fill(dturl);
                                     
                    connection.Close();
                    return dturl;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetSAPAMIUrl --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public string GetConifigValueByParam(string serviceName, string configName)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    string result = "";
                    connection.Open();
                    string query = "select CONFIG_VALUE from CONFIG_CIS_SYNC where ServiceName='"+ serviceName + "' and CONFIG_NAME='" + configName + "' AND ACTIVE='Y'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = reader["CONFIG_VALUE"].ToString();
                    }
                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetConifigValueByParam --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

    }
}