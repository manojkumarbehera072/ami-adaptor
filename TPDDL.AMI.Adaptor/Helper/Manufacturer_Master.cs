﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class Manufacturer_Master
    {
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        SqlDataReader reader;
        AdaptorLog adaptorLog = new AdaptorLog();

        public Manufacturer GetManufacturerById(string manufacturerID)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    Manufacturer materialMaster = new Manufacturer();
                    connection.Open();
                    string query = "select * from Manufacturer_Master  where Manufacturer_ID = '" + manufacturerID + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        materialMaster.ID = Convert.ToInt32(reader["ID"].ToString());
                        materialMaster.ManufacturerId = reader["Manufacturer_ID"].ToString();
                        materialMaster.Name = reader["Name"].ToString();
                        materialMaster.Description = reader["Description"].ToString();
                        materialMaster.DataSource = reader["Data_Source"].ToString();
                        materialMaster.CompCD = reader["Comp_Cd"].ToString();
                    }
                    connection.Close();
                    return materialMaster;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetManufacturerById --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}