﻿using log4net;
using System;
using System.Configuration;
using System.Data.SqlClient;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class ParameterMaster
    {
        AdaptorLog adaptorLog = new AdaptorLog();
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        public ParameterMasterModel GetIdByDataTypeTag(string UoMCategory,string UoMSubCategory,string ParamName, string ProfileName)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            ParameterMasterModel parameterMasterModel = new ParameterMasterModel();
            try
            {
                constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select ID,Param_Type from Parameter_Master where UOM_CATEGORY='" + UoMCategory + "' and UOM_SubCategory='" + UoMSubCategory + "' and PARAM_NAME='" + ParamName + "' /*and ProfileName='" + ProfileName + "'*/";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        parameterMasterModel.ID = reader["ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ID"].ToString());
                        parameterMasterModel.ParamType = reader["Param_Type"].ToString();
                    }
                    connection.Close();
                    return parameterMasterModel;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetIdByDataTypeTag --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }
    }
}