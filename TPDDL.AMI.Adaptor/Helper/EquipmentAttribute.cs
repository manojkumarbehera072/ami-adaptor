﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.AMI.Adaptor.Models;

namespace TPDDL.AMI.Adaptor.Helper
{
    public class EquipmentAttribute
    {
        AdaptorLog adaptorLog = new AdaptorLog();
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public static string version = ConfigurationManager.AppSettings["version"];

        public int GetIDByUtilityID(string ConsumerID)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select ID from Equipment_Attrib where Equipment_ID ='" + ConsumerID + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["ID"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetIDByUtilityID --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }

        }

        public List<string> GetFieldNameByTableName(string tableName)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    List<string> columnNames = new List<string>();
                    connection.Open();
                    string query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string columnName = reader["COLUMN_NAME"].ToString();
                        columnNames.Add(columnName);
                    }
                    connection.Close();
                    return columnNames;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetFieldNameByTableName --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }

        }

        public int GetIdByUtilityId(string tablename, string utilityId)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    int result = 0;
                    connection.Open();
                    string query = "select ID from " + tablename + " where Utility_ID='" + utilityId + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result = Convert.ToInt32(reader["ID"].ToString());
                    }

                    connection.Close();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetEquipmentIdByUtilityId --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }
        }

        public List<string> GetAttribNamesByEntityType(string entityType)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            List<string> attribTablesList = new List<string>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    List<string> columnNames = new List<string>();
                    connection.Open();
                    string query = "SELECT AE.Attrib_Name FROM [Equipment_Attrib]  EA join  [Attrib_Names] AE on ea.Attrib_ID=ae.ID where ae.Entity_Type= '" + entityType + "'";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string result = reader["Attrib_Name"].ToString();
                        attribTablesList.Add(result);
                    }
                    connection.Close();
                    return attribTablesList;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetAttribNamesByEntityType --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }

        }

        public AttribTables GetValidFromByParam(string attribName,int id)
        {
            adaptorLog.SetLogFileName("Ami_Adaptor_Log");
            log4net.Config.XmlConfigurator.Configure();
            ILog logger = log4net.LogManager.GetLogger("ErrorLog");
            AttribTables attribTables = new AttribTables();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT Valid_From,Attrib_ID FROM [Equipment_Attrib]  EA join  [Attrib_Names] AE on ea.Attrib_ID=ae.ID where ae.Attrib_Name='"+ attribName + "' and Ea.Equipment_ID="+id+"";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        attribTables.Attrib_ID= reader["Attrib_ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Attrib_ID"].ToString());
                        attribTables.Valid_From = reader["Valid_From"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["Valid_From"].ToString());
                    }
                    connection.Close();
                    return attribTables;
                }
            }
            catch (Exception ex)
            {
                string errorLog = "Method :GetValidFromByParam --- Version :" + version + " --- Exception :" + ex.Message;
                logger.Error(errorLog);
                throw ex;
            }

        }

    }
}