USE [CDR_MDM_TEST]
GO

/****** Object:  Table [dbo].[HESRequestLog]    Script Date: 16-02-2022 15:53:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HESRequestLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RefID] [varchar](25) NULL,
	[Action] [varchar](25) NULL,
	[DeviceID] [varchar](25) NULL,
	[ReplyURL] [varchar](100) NULL,
	[MRStartDate] [varchar](25) NULL,
	[MREndDate] [varchar](25) NULL,
	[ParamName] [varchar](100) NULL,
	[DemandResetFlag] [varchar](25) NULL,
	[DisplayAttributes] [varchar](100) NULL,
	[Created_on] [datetime] NULL,
 CONSTRAINT [PK_HESRequestLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

