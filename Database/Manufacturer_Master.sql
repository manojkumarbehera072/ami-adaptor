Create table [dbo].[Manufacturer_Master]
(
ID int identity(1,1) primary key,
Manufacturer_ID varchar(20),
Name varchar(30),
Description varchar(30),
Data_Source varchar(30),
Comp_Cd varchar(20),
Created_by varchar(20),
Created_on datetime,
Changed_by varchar(20),
Changed_on datetime
)