USE [tatapower]
GO

/****** Object:  Table [dbo].[Consumer]    Script Date: 08-12-2021 18:24:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Consumer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TYPE] [varchar](25) NULL,
	[Consumer_ID] [varchar](25) NULL,
	[Alt_Utility_ID] [varchar](25) NULL,
	[Cons_SEG] [varchar](25) NULL,
	[VIP] [varchar](25) NULL,
	[Essential_Service] [varchar](25) NULL,
	[SALUTATION] [varchar](25) NULL,
	[FIRST_NAME] [varchar](60) NULL,
	[MIDDLE_NAME] [varchar](60) NULL,
	[LAST_NAME] [varchar](60) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[PIN] [int] NULL,
	[DISTRICT] [varchar](25) NULL,
	[REGION] [varchar](25) NULL,
	[Utility_Office_Id] [varchar](25) NULL,
	[Status] [varchar](10) NULL,
	[Data_Source] [int] NULL,
	[Comp_Cd] [int] NULL,
	[Created_by] [varchar](25) NULL,
	[Changed_by] [varchar](25) NULL,
	[Created_on] [datetime] NULL,
	[Changed_On] [datetime] NULL
) ON [PRIMARY]
GO

