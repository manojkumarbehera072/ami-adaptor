USE [tatapower]
GO

/****** Object:  Table [dbo].[Consumer_Attrib]    Script Date: 08-12-2021 18:21:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Consumer_Attrib](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Utility_ID] [varchar](25) NULL,
	[Attrib_Name] [varchar](30) NULL,
	[Attrib_Value] [varchar](30) NULL,
	[Valid_From] [datetime] NULL,
	[Valid_To] [datetime] NULL,
	[Data_Source] [int] NULL,
	[Comp_Cd] [int] NULL,
	[Created_by] [varchar](25) NULL,
	[Changed_by] [varchar](25) NULL,
	[Created_on] [datetime] NULL,
	[Changed_On] [datetime] NULL
) ON [PRIMARY]
GO

