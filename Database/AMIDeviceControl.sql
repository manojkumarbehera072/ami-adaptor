CREATE TABLE [dbo].[AMIDeviceControl](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UUID] [varchar](50) NULL,
	[Request_Type] [varchar](25) NULL,
	[Equipment_ID] [int] NULL,
	[Planned_Processing_DateTime] [datetime] NULL,
	[Category_Code] [int] NULL,
	[Refrence_ID] [varchar](50) NULL,
	[Request_Status] [int] NULL,
	[Immediate_Status_Change_Indicator] [bit] NULL,
	[Retry_Count] [int] NULL,
	[Inclusion_Exclusion_Code] [varchar](25) NULL,
	[Interval_Boundary_Type_Code] [varchar](25) NULL,
	[Lower_Boundary_Priority_Code] [varchar](25) NULL,
	[Data_Origin_Code] [varchar](25) NULL,
	[Reason_Code] [varchar](25) NULL,
	[Response_Status] [bit] NULL,
	[Response_Code] [varchar](25) NULL,
	[Response_Text] [varchar](25) NULL,
	[HES_ID] [int] NULL,
	[HES_Ack_DateTime] [datetime] NULL,
	[HES_Ack_Status] [varchar](20) NULL,
	[AMI_Response_DateTime] [datetime] NULL,
	[HES_Response_DateTime] [datetime] NULL,
	[HES_Response_Text] [varchar](100) NULL,
	[Text_Message] [varchar](200) NULL,
	[Created_by] [varchar](25) NULL,
	[Created_on] [datetime] NULL,
	[Changed_by] [varchar](25) NULL,
	[Changed_on] [datetime] NULL,
 CONSTRAINT [PK_AMIDeviceControl] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AMIDeviceControl] ADD  CONSTRAINT [DF_AMIDeviceControl_CREATED_ON]  DEFAULT (getdate()) FOR [Created_on]
GO


