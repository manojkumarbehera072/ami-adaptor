
Create table [dbo].[Exception_CIS_Sync]
(
ID int identity(1,1) primary key,
UUID varchar(50),
Status varchar(20),
Message varchar(200),
Created_by varchar(20),
Created_on datetime,
Service_Name varchar(50)
)