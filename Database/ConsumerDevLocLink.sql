USE [tatapower]
GO

/****** Object:  Table [dbo].[ConsumerDevLocLink]    Script Date: 08-12-2021 18:21:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ConsumerDevLocLink](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ConsumerID] [varchar](25) NULL,
	[DeviceLocationId] [int] NULL,
	[ValidFromDate] [datetime] NULL,
	[ValidToDate] [datetime] NULL,
	[Created_by] [varchar](25) NULL,
	[Created_on] [datetime] NULL,
	[Changed_by] [varchar](25) NULL,
	[Changed_on] [datetime] NULL
) ON [PRIMARY]
GO

