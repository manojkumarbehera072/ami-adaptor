
CREATE TABLE [dbo].[CategoryCodeMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category_Code] [int] NULL,
	[Category_Name] [varchar](25) NULL,
	[Created_by] [varchar](25) NULL,
	[Created_on] [datetime] NULL,
	[Changed_by] [varchar](25) NULL,
	[Changed_on] [datetime] NULL,
 CONSTRAINT [PK_CategoryCodeMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO