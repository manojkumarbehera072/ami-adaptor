USE [tatapower]
GO

/****** Object:  Table [dbo].[Attrib_Names]    Script Date: 08-12-2021 18:20:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Attrib_Names](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Entity_Type] [varchar](25) NULL,
	[Attrib_Name] [varchar](25) NULL,
	[Active] [varchar](25) NULL,
	[Data_Source] [int] NULL,
	[Comp_Cd] [int] NULL,
	[Created_by] [varchar](25) NULL,
	[Changed_by] [varchar](25) NULL,
	[Created_on] [datetime] NULL,
	[Changed_On] [datetime] NULL
) ON [PRIMARY]
GO

