ALTER TABLE [dbo].[Equipment] DROP CONSTRAINT [DF_Equipment_Created_on]
GO

/****** Object:  Table [dbo].[Equipment]    Script Date: 10/28/2021 3:27:46 PM ******/
DROP TABLE [dbo].[Equipment]
GO

/****** Object:  Table [dbo].[Equipment]    Script Date: 10/28/2021 3:27:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Equipment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Utility_ID] [varchar](20) NULL,
	[Meter_Class] [varchar](20) NULL,
	[Manufacturer_ID] [varchar](20) NULL,
	[Status] [varchar](20) NULL,
	[Meter_ID] [varchar](20) NULL,
	[Device_Type] [varchar](20) NULL,
	[Model_ID] [varchar](20) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[Material_ID] [varchar](20) NULL,
	[Name_Plate_ID] [varchar](20) NULL,
	[HES_ID] [int] NULL,
	[Data_Source] [varchar](20) NULL,
	[Org_ID] [int] NULL,
	[Created_by] [varchar](20) NULL,
	[Created_on] [smalldatetime] NULL,
	[changed_by] [varchar](20) NULL,
	[changed_on] [smalldatetime] NULL,
 CONSTRAINT [PK_Equipment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Equipment] ADD  CONSTRAINT [DF_Equipment_Created_on]  DEFAULT (getdate()) FOR [Created_on]
GO



---------------------------------------------
---------------------------------------------


/****** Object:  Table [dbo].[Equipment_Reg_Link]    Script Date: 10/28/2021 3:28:46 PM ******/
DROP TABLE [dbo].[Equipment_Reg_Link]
GO

/****** Object:  Table [dbo].[Equipment_Reg_Link]    Script Date: 10/28/2021 3:28:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Equipment_Reg_Link](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Equipment_Id] [varchar](20) NULL,
	[ValidFrom] [datetime] NULL,
	[ValidTo] [datetime] NULL,
	[Log_Reg_no] [varchar](20) NULL,
	[UOBIS] [varchar](20) NULL,
	[Cat_Code] [varchar](20) NULL,
	[Div_code] [varchar](20) NULL,
	[TimeZone] [varchar](20) NULL,
	[MeasCode] [varchar](20) NULL,
	[No_Digits] [int] NULL,
	[Dec_Digits] [int] NULL,
	[Factor] [int] NULL,
	[Created_by] [varchar](20) NULL,
	[Created_on] [datetime] NULL,
	[Changed_by] [varchar](20) NULL,
	[Changed_on] [datetime] NULL,
 CONSTRAINT [PK_Equipment_Reg_Link] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


---------------------------------------------
---------------------------------------------


ALTER TABLE [dbo].[CIS_ADAPTOR_LOG] DROP CONSTRAINT [DF_CIS_ADAPTOR_LOG_Insertedon]
GO

/****** Object:  Table [dbo].[CIS_ADAPTOR_LOG]    Script Date: 10/28/2021 3:29:37 PM ******/
DROP TABLE [dbo].[CIS_ADAPTOR_LOG]
GO

/****** Object:  Table [dbo].[CIS_ADAPTOR_LOG]    Script Date: 10/28/2021 3:29:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CIS_ADAPTOR_LOG](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UUID] [varchar](50) NULL,
	[Log_DateTime] [datetime] NULL,
	[Request_Type] [varchar](100) NULL,
	[Child_Count] [int] NULL,
	[Insertedon] [datetime] NOT NULL,
 CONSTRAINT [PK_CIS_ADAPTOR_LOG] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CIS_ADAPTOR_LOG] ADD  CONSTRAINT [DF_CIS_ADAPTOR_LOG_Insertedon]  DEFAULT (getdate()) FOR [Insertedon]
GO



---------------------------------------------
---------------------------------------------


ALTER TABLE [dbo].[CIS_ADAPTOR_CHILD_LOG] DROP CONSTRAINT [DF_CIS_ADAPTOR_CHILD_LOG_Insertedon]
GO

/****** Object:  Table [dbo].[CIS_ADAPTOR_CHILD_LOG]    Script Date: 10/28/2021 3:30:13 PM ******/
DROP TABLE [dbo].[CIS_ADAPTOR_CHILD_LOG]
GO

/****** Object:  Table [dbo].[CIS_ADAPTOR_CHILD_LOG]    Script Date: 10/28/2021 3:30:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CIS_ADAPTOR_CHILD_LOG](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Parent_UUID] [varchar](50) NULL,
	[UUID] [varchar](50) NULL,
	[Log_DateTime] [datetime] NULL,
	[Request_Type] [varchar](100) NULL,
	[Insertedon] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CIS_ADAPTOR_CHILD_LOG] ADD  CONSTRAINT [DF_CIS_ADAPTOR_CHILD_LOG_Insertedon]  DEFAULT (getdate()) FOR [Insertedon]
GO


---------------------------------------------
---------------------------------------------


DROP TABLE [dbo].[Device_Location]
GO

/****** Object:  Table [dbo].[Device_Location]    Script Date: 10/28/2021 3:30:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Device_Location](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Installation_Type] [varchar](30) NULL,
	[Utility_ID] [varchar](30) NULL,
	[Alt_Utility_ID] [varchar](30) NULL,
	[Description] [varchar](50) NULL,
	[Premise] [numeric](25, 0) NULL,
	[GPS_LAT] [numeric](22, 8) NULL,
	[GPS_LONG] [numeric](22, 8) NULL,
	[Utility_Office_ID] [varchar](50) NULL,
	[Virtual_Flg] [char](1) NULL,
	[GIS_ID] [varchar](30) NULL,
	[STATUS_CD] [char](1) NULL,
	[Multi_Device] [char](1) NULL,
	[Comp_Cd] [numeric](12, 0) NULL,
	[Created_by] [varchar](30) NULL,
	[Changed_by] [varchar](30) NULL,
	[Created_on] [datetime] NULL,
	[Changed_on] [datetime] NULL,
 CONSTRAINT [PK_Device_Location] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


---------------------------------------------
---------------------------------------------


DROP TABLE [dbo].[Device_Location_Info]
GO

/****** Object:  Table [dbo].[Device_Location_Info]    Script Date: 10/28/2021 3:31:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Device_Location_Info](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Device_Location_ID] [numeric](15, 0) NULL,
	[HouseID] [varchar](25) NULL,
	[StreetPostalCode] [varchar](25) NULL,
	[CityName] [varchar](25) NULL,
	[StreetName] [varchar](25) NULL,
	[Data_Source] [numeric](12, 0) NULL,
	[CountryCode] [varchar](25) NULL,
	[TimeZoneCode] [varchar](25) NULL,
	[ParentInstallationPointID] [varchar](25) NULL,
	[InstallationDate] [datetime] NULL,
	[Created_by] [varchar](25) NULL,
	[Changed_by] [varchar](25) NULL,
	[Created_on] [datetime] NULL,
	[Changed_on] [datetime] NULL,
 CONSTRAINT [PK_Device_Location_Info] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



---------------------------------------------
---------------------------------------------


ALTER TABLE [dbo].[DevLoc_Device_Link] DROP CONSTRAINT [DF_DevLoc_Device_Link_Created_on]
GO

/****** Object:  Table [dbo].[DevLoc_Device_Link]    Script Date: 10/28/2021 3:32:09 PM ******/
DROP TABLE [dbo].[DevLoc_Device_Link]
GO

/****** Object:  Table [dbo].[DevLoc_Device_Link]    Script Date: 10/28/2021 3:32:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DevLoc_Device_Link](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EquipmentId] [int] NOT NULL,
	[DeviceLocationId] [int] NOT NULL,
	[ValidFromDate] [datetime] NOT NULL,
	[ValidToDate] [datetime] NULL,
	[Created_by] [varchar](25) NULL,
	[Created_on] [datetime] NULL,
	[Changed_by] [varchar](25) NULL,
	[Changed_on] [datetime] NULL,
 CONSTRAINT [PK_DevLoc_Device_Link] PRIMARY KEY CLUSTERED 
(
	[EquipmentId] ASC,
	[DeviceLocationId] ASC,
	[ValidFromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DevLoc_Device_Link] ADD  CONSTRAINT [DF_DevLoc_Device_Link_Created_on]  DEFAULT (getdate()) FOR [Created_on]
GO


---------------------------------------------
---------------------------------------------


DROP TABLE [dbo].[HES_MASTER]
GO

/****** Object:  Table [dbo].[HES_MASTER]    Script Date: 10/28/2021 3:32:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HES_MASTER](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HES_CODE] [varchar](20) NULL,
	[COMP_ID] [int] NULL,
	[HES_DESCRIPTION] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



---------------------------------------------
---------------------------------------------
DROP TABLE [dbo].[ReadRequestResponse]
GO
/****** Object:  Table [dbo].[ReadRequestResponse]    Script Date: 02-11-2021 15:07:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ReadRequestResponse](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Equipment_ID] [int] NULL,
	[Doc_ID] [varchar](25) NULL,
	[MRReasonCode] [varchar](25) NULL,
	[MeterReadStartDate] [datetime] NULL,
	[MeterReadEndDate] [datetime] NULL,
	[ReadSource] [varchar](25) NULL,
	[LogRegno] [varchar](25) NULL,
	[RegCode] [varchar](25) NULL,
	[Status] [int] NULL,
	[ResponseStatus] [int] NULL,
	[MRResult] [varchar](25) NULL,
	[HeaderUUID] [varchar](50) NULL,
	[ChildUUID] [varchar](50) NULL,
	[CreationDatetime] [datetime] NULL,
	[Created_by] [varchar](25) NULL,
	[Created_on] [datetime] NULL,
	[Changed_by] [varchar](25) NULL,
	[Changed_on] [datetime] NULL,
 CONSTRAINT [PK_ReadRequestResponse] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO