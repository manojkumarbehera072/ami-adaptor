USE [CDR_MDM_TEST]
GO

/****** Object:  Table [dbo].[Material_Master]    Script Date: 11/29/2021 10:50:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Material_Master](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Utility_ID] [varchar](24) NULL,
	[Description] [varchar](30) NULL,
	[Device_Type] [varchar](20) NULL,
	[Meter_Class] [varchar](20) NULL,
	[Data_Source] [varchar](20) NULL,
	[COMP_ID] [int] NULL,
	[Inserted_By] [varchar](20) NULL,
	[Inserted_DATE] [smalldatetime] NULL,
	[Changed_By] [varchar](20) NULL,
	[Changed_Date] [smalldatetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


