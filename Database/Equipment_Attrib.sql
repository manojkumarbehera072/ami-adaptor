USE [tatapower]
GO

/****** Object:  Table [dbo].[Equipment_Attrib]    Script Date: 08-12-2021 18:22:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Equipment_Attrib](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Equipment_ID] [varchar](25) NULL,
	[Attrib_Name] [varchar](25) NULL,
	[Attrib_Value] [varchar](25) NULL,
	[Valid_From] [datetime] NULL,
	[Valid_To] [datetime] NULL,
	[Data_Source] [int] NULL,
	[Virtual_Flg] [varchar](25) NULL,
	[Comp_Cd] [int] NULL,
	[Created_by] [varchar](30) NULL,
	[Changed_by] [varchar](30) NULL,
	[Created_on] [datetime] NULL,
	[Changed_On] [datetime] NULL
) ON [PRIMARY]
GO

